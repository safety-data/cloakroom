package com.safetydata.cloakroom.admin;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CloakroomRoleTestUtil {
    public static Set<Role> roles(String... name) {
        return Stream.of(name).map(Role::new).collect(Collectors.toSet());
    }

    public static Role role(String name) {
        return new Role(name);
    }
}
