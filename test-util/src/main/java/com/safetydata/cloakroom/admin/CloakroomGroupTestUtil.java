package com.safetydata.cloakroom.admin;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CloakroomGroupTestUtil {
    public static Set<Group> groups(String... name) {
        return Stream.of(name).map(CloakroomGroupTestUtil::group).collect(Collectors.toSet());
    }

    public static Group group(String name) {
        return Group.builder().name(name).build();
    }

    public static List<GroupId> ids(String... id) {
        return Stream.of(id).map(GroupId::new).collect(Collectors.toUnmodifiableList());
    }

    public static GroupId id(String id) {
        return new GroupId(id);
    }
}
