package com.safetydata.cloakroom.admin;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CloakroomUserIdTestUtil {

    public static List<UserId> ids(String... id) {
        return Stream.of(id).map(UserId::new).collect(Collectors.toUnmodifiableList());
    }

    public static UserId id(String id) {
        return new UserId(id);
    }
}
