package com.safetydata.cloakroom.admin;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class CloakroomUserIdTestUtilTest {

  @Test
  public void idShouldProperlyConstructAUserId() {
    assertEquals(CloakroomUserIdTestUtil.id("yoloId"), new UserId("yoloId"));
  }

  @Test
  public void idsShouldProperlyConstructAUserIdList() {
    assertEquals(CloakroomUserIdTestUtil.ids("chewbacca", "yoda"), List.of(
            new UserId("chewbacca"),
            new UserId("yoda")
    ));
  }
}
