package com.safetydata.cloakroom.admin;

import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.assertEquals;

public class CloakroomRoleTestUtilTest {

  @Test
  public void roleShouldProperlyConstructACloakroomRole() {
    assertEquals(CloakroomRoleTestUtil.role("yolo"), new Role("yolo"));
  }

  @Test
  public void rolesShouldProperlyConstructACloakroomRoleList() {
    assertEquals(CloakroomRoleTestUtil.roles("chewbacca", "yoda"), Set.of(
            new Role("chewbacca"),
            new Role("yoda")
    ));
  }
}
