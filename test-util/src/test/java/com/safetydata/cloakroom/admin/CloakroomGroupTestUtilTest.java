package com.safetydata.cloakroom.admin;

import org.junit.Test;

import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class CloakroomGroupTestUtilTest {

  @Test
  public void groupsShouldProperlyConstructACloakroomGroup() {
    assertEquals(CloakroomGroupTestUtil.group("yolo"), Group.builder().name("yolo").build());
  }

  @Test
  public void groupsShouldProperlyConstructACloakroomGroupList() {
    assertEquals(CloakroomGroupTestUtil.groups("chewbacca", "yoda"), Set.of(
        Group.builder().name("chewbacca").build(),
        Group.builder().name("yoda").build()
    ));
  }

  @Test
  public void idShouldProperlyConstructAGroupId() {
    assertEquals(CloakroomGroupTestUtil.id("yoloId"), new GroupId("yoloId"));
  }

  @Test
  public void idsShouldProperlyConstructAGroupIdList() {
    assertEquals(CloakroomGroupTestUtil.ids("chewbacca", "yoda"), List.of(
        new GroupId("chewbacca"),
        new GroupId("yoda")
    ));
  }
}
