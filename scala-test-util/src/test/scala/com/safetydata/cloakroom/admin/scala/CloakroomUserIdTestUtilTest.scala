package com.safetydata.cloakroom.admin.scala

import org.scalatest.flatspec.AnyFlatSpec
import com.safetydata.cloakroom.{admin => javaAdmin}
import org.scalatest.matchers.should.Matchers

class CloakroomUserIdTestUtilTest extends AnyFlatSpec with Matchers {
  "id" should "correctly construct an UserId" in {
    val id: javaAdmin.UserId = CloakroomUserIdTestUtil.id("an id")
    id.id() shouldEqual "an id"
  }

  "ids" should "correctly construct a list of UserId" in {
    val id: List[javaAdmin.UserId] = CloakroomUserIdTestUtil.ids("an id", "another id")
    id.map(_.id()) shouldEqual List("an id", "another id")
  }
}
