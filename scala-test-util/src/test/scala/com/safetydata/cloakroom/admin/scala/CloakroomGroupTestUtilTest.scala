package com.safetydata.cloakroom.admin.scala

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import com.safetydata.cloakroom.{admin => javaAdmin}

class CloakroomGroupTestUtilTest extends AnyFlatSpec with Matchers {
  "group" should "properly construct group" in {
    val result: Group = CloakroomGroupTestUtil.group("yolo")
    result.name shouldEqual "yolo"
  }

  "groups" should "properly construct set of groups" in {
    val result: Set[Group] = CloakroomGroupTestUtil.groups("yoda", "chewbacca")
    result.map(_.name) shouldEqual Set("yoda", "chewbacca")
  }

  "id" should "correctly construct an GroupId" in {
    val id: javaAdmin.GroupId = CloakroomGroupTestUtil.id("an id")
    id.id() shouldEqual "an id"
  }

  "ids" should "correctly construct a list of GroupId" in {
    val id: List[javaAdmin.GroupId] = CloakroomGroupTestUtil.ids("an id", "another id")
    id.map(_.id()) shouldEqual List("an id", "another id")
  }
}
