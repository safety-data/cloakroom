package com.safetydata.cloakroom.admin.scala

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CloakroomRoleTestUtilTest extends AnyFlatSpec with Matchers {
  "role" should "properly construct role" in {
    val result: Role = CloakroomRoleTestUtil.role("yolo")
    result.name shouldEqual "yolo"
  }

  "roles" should "properly construct set of roles" in {
    val result: Set[Role] = CloakroomRoleTestUtil.roles("yoda", "chewbacca")
    result.map(_.name) shouldEqual Set("yoda", "chewbacca")
  }
}
