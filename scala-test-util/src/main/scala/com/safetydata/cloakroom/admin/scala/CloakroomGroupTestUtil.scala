package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.{admin => javaAdmin}

import scala.jdk.CollectionConverters._

object CloakroomGroupTestUtil {
  def groups(groupName: String*):  Set[Group] = javaAdmin
    .CloakroomGroupTestUtil
    .groups(groupName:_*)
    .asScala
    .map(Group.fromJava(_))
    .toSet

  def group(groupName: String): Group = Group.fromJava(javaAdmin.CloakroomGroupTestUtil.group(groupName))

  def ids(ids: String*):  List[javaAdmin.GroupId] = javaAdmin.CloakroomGroupTestUtil
    .ids(ids:_*)
    .asScala
    .toList

  def id(id: String): javaAdmin.GroupId = javaAdmin.CloakroomGroupTestUtil.id(id)
}
