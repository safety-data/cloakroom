package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.{admin => javaAdmin}
import scala.jdk.CollectionConverters._

object CloakroomUserIdTestUtil {
  def ids(ids: String*):  List[javaAdmin.UserId] = javaAdmin.CloakroomUserIdTestUtil
    .ids(ids:_*)
    .asScala
    .toList

  def id(id: String): javaAdmin.UserId = javaAdmin.CloakroomUserIdTestUtil.id(id)
}
