package com.safetydata.cloakroom.scala

import com.safetydata.cloakroom.TokenTestUtil
import com.typesafe.config.ConfigFactory
import org.scalatest.flatspec.AsyncFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.jdk.CollectionConverters._

class TokenVerifierTest extends AsyncFlatSpec with Matchers {
  val realm = "test-realm"

  "TokenVerifier" should "correctly parse valid token" in {
    val token = TokenTestUtil.getTokenFromKeycloak(realm)
    val tokenVerifier = TokenVerifier(realm)
    tokenVerifier(token).map(token => {
      token.isRight shouldBe (true)
      val verifiedToken = token.getOrElse(throw new Exception("Not expected"))
      verifiedToken.roles shouldBe (ConfigFactory.load().getStringList("cloakroom.test.roles").asScala.toSet)
    })
  }

  "TokenVerifier" should "return a left either when token is not validated" in {
    val token = TokenTestUtil.generateInvalidToken().token;

    val tokenVerifier = TokenVerifier(realm)
    tokenVerifier(token).map(token => {
      token should matchPattern {
        case Left(_: BadPublicKeys) =>
      }
    })
  }
}
