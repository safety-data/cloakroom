package com.safetydata.cloakroom.scala

import com.safetydata.cloakroom.VerificationResultTestUtil
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

import scala.collection.JavaConverters._
import scala.compat.java8.OptionConverters._

class VerifiedTokenTest extends AnyFlatSpecLike with Matchers {
  "VerifiedToken" should "not wrap invalid token" in {
    assertThrows[IllegalArgumentException] {
      VerifiedToken(VerificationResultTestUtil.badVerificationResult())
    }
  }

  "VerifiedToken" should "wrap valid token" in {
    val verificationResult = VerificationResultTestUtil.goodVerificationResult();
    val verifiedToken = VerifiedToken(verificationResult)
    verificationResult.token().get() shouldBe (verifiedToken.accessToken)
    verificationResult.roles().get() shouldBe (verifiedToken.roles.asJava)
    verificationResult.username().asScala shouldBe (verifiedToken.username)
    verificationResult.email().asScala shouldBe (verifiedToken.email)
  }
}
