package com.safetydata.cloakroom.scala

import com.safetydata.{cloakroom => javaCloakroom}
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers

import java.util.Optional

class VerificationErrorTest extends AnyFlatSpecLike with Matchers {
  "VerificationError" should "correctly wrap BadPublicKeys" in {
    val javaError = new javaCloakroom.BadPublicKeys()

    VerificationError(javaError) shouldBe BadPublicKeys(javaError.reason)
  }

  it should "correctly wrap CouldNotParseToken" in {
    val underlyingError = new RuntimeException("Your token is BAD!")
    val javaError = new javaCloakroom.CouldNotParseToken(underlyingError)

    VerificationError(javaError) shouldBe CouldNotParseToken(Some(underlyingError), javaError.reason)
  }

  it should "correctly wrap CouldNotParseToken without underlying error" in {
    val javaError = new javaCloakroom.CouldNotParseToken(null)

    VerificationError(javaError) shouldBe CouldNotParseToken(None, javaError.reason)
  }

  it should "correctly wrap InvalidToken" in {
    val underlyingError = new RuntimeException("Your token is BAD!")
    val javaError = new javaCloakroom.InvalidToken(underlyingError)

    VerificationError(javaError) shouldBe InvalidToken(Some(underlyingError), javaError.reason)
  }

  it should "correctly wrap InvalidToken without underlying error" in {
    val javaError = new javaCloakroom.InvalidToken(null)

    VerificationError(javaError) shouldBe InvalidToken(None, javaError.reason)
  }

  it should "fail if a new subclass of VerificationError happens to exist" in {
    assertThrows[IllegalArgumentException] {
      VerificationError(new javaCloakroom.VerificationError {
        override def reason(): String = ""
        override def underlyingError(): Optional[Throwable] = Optional.empty()
      })
    }
  }
}
