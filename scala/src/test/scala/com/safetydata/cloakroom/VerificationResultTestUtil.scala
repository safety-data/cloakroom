package com.safetydata.cloakroom

import org.keycloak.representations.AccessToken

object VerificationResultTestUtil {
  def goodVerificationResult(): VerificationResult = VerificationResult.ofToken("clientId", {
    val accessToken = new AccessToken()
    val access = new AccessToken.Access()
    access.addRole("test")
    accessToken.setPreferredUsername("test")
    accessToken.setEmail("test@test.com")
    accessToken.setResourceAccess(java.util.Map.of("clientId", access))
    accessToken
  })

  // just because VerificationResult.ofToken is not visible outside this package
  def badVerificationResult(): VerificationResult = VerificationResult.ofError(new BadPublicKeys())
}
