package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.{admin => javaAdmin}
import org.scalatest.concurrent.ScalaFutures.convertScalaFuture

import scala.jdk.CollectionConverters._

object GroupTestUtil {
  def groups(groupName: String*):  Set[Group] = javaAdmin
    .GroupTestUtil
    .groups(groupName:_*)
    .asScala
    .map(Group.fromJava(_))
    .toSet

  def group(groupName: String): Group = Group.fromJava(javaAdmin.GroupTestUtil.group(groupName))

  def group(groupName: String, roles: String*): Group = Group.fromJava(javaAdmin.GroupTestUtil.group(groupName, roles:_*))

  def clean(groupAdministration: GroupAdministration): Unit = {
    groupAdministration.listGroups(0, Int.MaxValue).futureValue.groups.map(_._1)
      .foreach { groupId =>
        groupAdministration.deleteGroup(groupId).futureValue
      }
  }

  def createGroups(groups: List[Group], groupAdministration: GroupAdministration): Unit = {
    groups.foreach(groupAdministration.addGroup(_).futureValue.getOrElse(throw new Exception("Not expected")))
  }
}
