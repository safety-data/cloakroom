package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.scala.GroupTestUtil.group
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}

import java.time.{ZoneOffset, ZonedDateTime}
import scala.concurrent.ExecutionContext.Implicits.global

class GroupAdministrationTest extends AnyFlatSpecLike with Matchers with BeforeAndAfterEach with ScalaFutures with BeforeAndAfterAll {
  private var groupAdministration: GroupAdministration = _
  private var userAdministration: UserAdministration = _
  private val realm = "test-realm"

  private val testGroupName = "test"
  private val totoGroupName = "toto"
  
  implicit val defaultPatience: PatienceConfig = {
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(5, Millis))
  }

  override def beforeAll(): Unit = {
    super.beforeAll()
    val keycloakAdministration = KeycloakAdministration(realm)
    groupAdministration = keycloakAdministration.groupAdministration
    userAdministration = keycloakAdministration.userAdministration
  }

  override def beforeEach(): Unit = {
    GroupTestUtil.clean(groupAdministration)
    super.beforeEach()
    val groups = List(
      group("group1b", "group1_role"),
      group("group1", "group1_role"),
      group("group2", "son_b"),
      group("group3")
    )
    GroupTestUtil.createGroups(groups, groupAdministration)
  }

  "groupCount" should "work" in {
    groupAdministration.groupCount().futureValue shouldEqual 4
  }
  
  "getGroupByName" should "return existing group" in {
    groupAdministration.getGroupByName("group1").futureValue.map(_._2.group.name) shouldBe Some("group1")
  }
  
  it should "return Nothing if group does not exist" in {
    groupAdministration.getGroupByName("ze_group_that_does_not_exist").futureValue shouldBe None
  }

  "getGroupById" should "return existing group" in {
    val (groupId, group1) = groupAdministration.getGroupByName("group1").futureValue.get
    groupAdministration.getGroupById(groupId).futureValue shouldEqual group1
  }

  "addModifyDeleteGroup" should "work" in {
    groupAdministration.getGroupByName(testGroupName).futureValue shouldEqual None

    val attributes = Map("description" -> Seq("Hello I'm toto"))
    val initialGroup = Group(testGroupName).withRoles(Set.empty).withAttributes(attributes)
    
    val groupId = groupAdministration.addGroup(initialGroup).futureValue.getOrElse(throw new Exception("Not expected"))

    groupAdministration.getGroupById(groupId).futureValue.group shouldEqual initialGroup

    groupAdministration.getGroupByName(testGroupName).futureValue.map(_._2.group) shouldEqual Some(initialGroup)

    val groupModifier = GroupModifier.forGroup(initialGroup).withName(totoGroupName)

    groupAdministration.modifyGroup(groupId, groupModifier).futureValue
    
    val updatedGroup = Group(totoGroupName).withRoles(Set.empty).withAttributes(attributes)

    groupAdministration.getGroupByName(testGroupName).futureValue shouldEqual None
    groupAdministration.getGroupByName(totoGroupName).futureValue.map(_._2.group) shouldEqual Some(updatedGroup)

    groupAdministration.deleteGroup(groupId).futureValue

    groupAdministration.getGroupByName(totoGroupName).futureValue shouldEqual None
  }
  
  "modifyGroup" should "set roles" in {
    val initialGroup = Group(testGroupName).withRoles(RoleTestUtil.roles("son_a", "son_b"))
    val groupId = groupAdministration.addGroup(initialGroup).futureValue.getOrElse(throw new Exception("Not expected"))
    val groupModifier = GroupModifier.forGroup(initialGroup).setRoles(RoleTestUtil.roles("son_b", "son_c"))
    
    groupAdministration.modifyGroup(groupId, groupModifier).futureValue
    val fetchedGroup = groupAdministration.getGroupById(groupId).futureValue.group
    
    fetchedGroup shouldEqual initialGroup.withRoles(RoleTestUtil.roles("son_b", "son_c"))
  }

  it should "add roles" in {
    val initialGroup = Group(testGroupName).withRoles(RoleTestUtil.roles("son_a", "son_b"))
    val groupId = groupAdministration.addGroup(initialGroup).futureValue.getOrElse(throw new Exception("Not expected"))
    val groupModifier = GroupModifier.forGroup(initialGroup).addRoles(RoleTestUtil.roles("son_b", "son_c"))

    groupAdministration.modifyGroup(groupId, groupModifier).futureValue
    val fetchedGroup = groupAdministration.getGroupById(groupId).futureValue.group

    fetchedGroup shouldEqual initialGroup.withRoles(RoleTestUtil.roles("son_a", "son_b", "son_c"))
  }

  it should "remove roles" in {
    val initialGroup = Group(testGroupName).withRoles(RoleTestUtil.roles("son_a", "son_b"))
    val groupId = groupAdministration.addGroup(initialGroup).futureValue.getOrElse(throw new Exception("Not expected"))
    val groupModifier = GroupModifier.forGroup(initialGroup).removeRoles(RoleTestUtil.roles("son_b", "son_c"))

    groupAdministration.modifyGroup(groupId, groupModifier).futureValue
    val fetchedGroup = groupAdministration.getGroupById(groupId).futureValue.group

    fetchedGroup shouldEqual initialGroup.withRoles(RoleTestUtil.roles("son_a"))
  }

  it should "set attributes" in {
    val initialGroup = Group(testGroupName).withRoles(RoleTestUtil.roles("son_a", "son_b")).withAttributes(Map("description" -> Seq("Hello I'm toto")))
    val groupId = groupAdministration.addGroup(initialGroup).futureValue.getOrElse(throw new Exception("Not expected"))

    val newAttributes = Map("description" -> Seq("Hello I'm titi"))
    val groupModifier = GroupModifier.forGroup(initialGroup).withAttributes(newAttributes)

    groupAdministration.modifyGroup(groupId, groupModifier).futureValue
    val fetchedGroup = groupAdministration.getGroupById(groupId).futureValue.group

    fetchedGroup shouldEqual initialGroup.withAttributes(newAttributes)
  }

  "groupSize and groupMembers" should "count group members correctly" in {
    val (userId1, _) = userAdministration.getUserByUsername("another.usertest").futureValue.get
    val groups1 = GroupTestUtil.groups("group1", "group2")
    val userModifier1 = UserModifier.setGroups(groups1)
    userAdministration.modifyUser(userId1, userModifier1)

    val (userId2, _) = userAdministration.getUserByUsername("usertest").futureValue.get
    val groups2 = GroupTestUtil.groups("group2", "group3")
    val userModifier2 = UserModifier.setGroups(groups2)
    userAdministration.modifyUser(userId2, userModifier2)

    val (groupId1, _) = groupAdministration.getGroupByName("group1").futureValue.get
    val (groupId2, _) = groupAdministration.getGroupByName("group2").futureValue.get
    val (groupId3, _) = groupAdministration.getGroupByName("group3").futureValue.get
    groupAdministration.groupSize(groupId1).futureValue shouldEqual 1
    groupAdministration.groupSize(groupId2).futureValue shouldEqual 2
    groupAdministration.groupSize(groupId3).futureValue shouldEqual 1

    groupAdministration.groupMembers(groupId1, 0, 100).futureValue.users.map(_._2.username) shouldEqual Seq("another.usertest")
    groupAdministration.groupMembers(groupId2, 0, 100).futureValue.users.map(_._2.username) shouldEqual Seq("another.usertest", "usertest")
    groupAdministration.groupMembers(groupId2, 1, 2).futureValue.users.map(_._2.username) shouldEqual Seq("usertest")
    groupAdministration.groupMembers(groupId1, 0, 1).futureValue.users.map(_._2.username) shouldEqual Seq("another.usertest")
    groupAdministration.groupMembers(groupId2, 0, 100, filter = Some(UserAdministration.Filter(UserAdministration.FilterField.Username, "another.*".r))).futureValue.users.map(_._2.username) shouldEqual Seq("another.usertest")
    groupAdministration.groupMembers(groupId2, 0, 100, requiredRole = Some(Role("another_role"))).futureValue.users.map(_._2.username) shouldEqual Seq("another.usertest")
  }

  "listGroups" should "find groups correctly by name" in {
    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByName(true)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group1",
      "group1b",
      "group2",
      "group3"
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByName(false)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group3",
      "group2",
      "group1b",
      "group1"
    )
  }

  it should "paginate correctly" in {
    val results = groupAdministration.listGroups(1, 2, GroupSort.ByName(true)).futureValue

    results.groups.map(_._2.name) shouldEqual Seq(
      "group1b",
      "group2",
    )

    results.totalCount shouldEqual 4
  }

  it should "sort by name with case" in {
    GroupTestUtil.clean(groupAdministration)
    val group1 = group("agroup1", "another_role")
    val group2 = group("Agroup2", "another_role")
    val group3 = group("bgroup3", "another_role")
    val group4 = group("Bgroup4")
    GroupTestUtil.createGroups(List(group1, group2, group3, group4), groupAdministration)

    val resultsWithCaseInsensitive = groupAdministration.listGroups(0, 4, GroupSort.ByName(ascending = true)).futureValue.groups.map(_._2.name)
    val expectedWithCaseInsensitive = Seq(group1.name, group2.name, group3.name, group4.name)
    resultsWithCaseInsensitive shouldBe expectedWithCaseInsensitive

    val resultsWithCaseSensitive = groupAdministration.listGroups(0, 4, GroupSort.ByName(ascending = true, caseInsensitive = false)).futureValue.groups.map(_._2.name)
    val expectedWithCaseSensitive = Seq(group2.name, group4.name, group1.name, group3.name)
    resultsWithCaseSensitive shouldBe expectedWithCaseSensitive

    val resultsByReverseNameWithCaseInsensitive = groupAdministration.listGroups(0, 4, GroupSort.ByName(ascending = false)).futureValue.groups.map(_._2.name)
    val expectedByReverseNameWithCaseInsensitive = Seq(group4.name, group3.name, group2.name, group1.name)
    resultsByReverseNameWithCaseInsensitive shouldBe expectedByReverseNameWithCaseInsensitive

    val resultsByReverseNamWithCaseInsensitive = groupAdministration.listGroups(0, 4, GroupSort.ByName(ascending = false, caseInsensitive = false)).futureValue.groups.map(_._2.name)
    val expectedByReverseNamWithCaseInsensitive = Seq(group3.name, group1.name, group4.name, group2.name)
    resultsByReverseNamWithCaseInsensitive shouldBe expectedByReverseNamWithCaseInsensitive
  }

  it should "apply regex when requested" in {
    val results = groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByName(true), Some(".*1.*".r)).futureValue

    results.groups.map(_._2.name) shouldEqual Seq(
      "group1",
      "group1b",
    )
    results.totalCount shouldEqual 2

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByName(false), Some(".*1.*".r)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group1b",
      "group1"
    )
  }

  it should "sort correctly by size" in {
    // list groups initially to load cache and prove it's refreshing
    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.BySize(true)).futureValue

    val (userId1, _) = userAdministration.getUserByUsername("another.usertest").futureValue.get
    val groups1 = GroupTestUtil.groups("group1", "group2")
    val userModifier1 = UserModifier.setGroups(groups1)
    userAdministration.modifyUser(userId1, userModifier1)

    val (userId2, _) = userAdministration.getUserByUsername("usertest").futureValue.get
    val groups2 = GroupTestUtil.groups("group2", "group3")
    val userModifier2 = UserModifier.setGroups(groups2)
    userAdministration.modifyUser(userId2, userModifier2)

    Thread.sleep(200)
    groupAdministration.getGroupByName("group1").futureValue.get._2.size shouldEqual 1
    groupAdministration.getGroupByName("group2").futureValue.get._2.size shouldEqual 2
    groupAdministration.getGroupByName("group3").futureValue.get._2.size shouldEqual 1

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.BySize(true)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group1b",
      "group1",
      "group3",
      "group2",
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.BySize(false)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group2",
      "group3",
      "group1",
      "group1b",
    )
  }

  it should "sort correctly by attributes" in {
    val (group1Id, group1) = groupAdministration.getGroupByName("group1").futureValue.get
    val group1Attributes = Map("fruit" -> Seq("banana"), "count" -> Seq("42"), "date" -> Seq(ZonedDateTime.of(2000, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC).toString))
    val group1Modifier = GroupModifier.forGroup(group1).withAttributes(group1Attributes)
    groupAdministration.modifyGroup(group1Id, group1Modifier).futureValue

    val (group2Id, group2) = groupAdministration.getGroupByName("group2").futureValue.get
    val group2Attributes = Map("fruit" -> Seq("apple"), "count" -> Seq("43"), "date" -> Seq(ZonedDateTime.of(1998, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC).toString))
    val group2Modifier = GroupModifier.forGroup(group2).withAttributes(group2Attributes)
    groupAdministration.modifyGroup(group2Id, group2Modifier).futureValue

    val (group3Id, group3) = groupAdministration.getGroupByName("group3").futureValue.get
    val group3Attributes = Map("fruit" -> Seq("orange"), "count" -> Seq("41"), "date" -> Seq(ZonedDateTime.of(1999, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC).toString))
    val group3Modifier = GroupModifier.forGroup(group3).withAttributes(group3Attributes)
    groupAdministration.modifyGroup(group3Id, group3Modifier).futureValue

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByAttribute("fruit", SortUtils.AttributeType.String, true)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group1b",
      "group2",
      "group1",
      "group3",
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByAttribute("fruit", SortUtils.AttributeType.String, false)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group3",
      "group1",
      "group2",
      "group1b",
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByAttribute("fruit", SortUtils.AttributeType.String, true, false)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group2",
      "group1",
      "group3",
      "group1b",
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByAttribute("fruit", SortUtils.AttributeType.String, false, false)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group1b",
      "group3",
      "group1",
      "group2",
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByAttribute("count", SortUtils.AttributeType.Integer, true)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group1b",
      "group3",
      "group1",
      "group2",
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByAttribute("count", SortUtils.AttributeType.Integer, false)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group2",
      "group1",
      "group3",
      "group1b",
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByAttribute("count", SortUtils.AttributeType.Integer, true, false)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group3",
      "group1",
      "group2",
      "group1b",
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByAttribute("count", SortUtils.AttributeType.Integer, false, false)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group1b",
      "group2",
      "group1",
      "group3",
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByAttribute("date", SortUtils.AttributeType.DateTime, true)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group1b",
      "group2",
      "group3",
      "group1",
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByAttribute("date", SortUtils.AttributeType.DateTime, false)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group1",
      "group3",
      "group2",
      "group1b",
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByAttribute("date", SortUtils.AttributeType.DateTime, true, false)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group2",
      "group3",
      "group1",
      "group1b",
    )

    groupAdministration.listGroups(0, Int.MaxValue, GroupSort.ByAttribute("date", SortUtils.AttributeType.DateTime, false, false)).futureValue.groups.map(_._2.name) shouldEqual Seq(
      "group1b",
      "group1",
      "group3",
      "group2",
    )
  }
}
