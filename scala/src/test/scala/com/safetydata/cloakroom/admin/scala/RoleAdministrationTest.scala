package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.scala.GroupTestUtil.group
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}

import scala.concurrent.ExecutionContext.Implicits.global

class RoleAdministrationTest extends AnyFlatSpecLike with Matchers with BeforeAndAfterEach with ScalaFutures with BeforeAndAfterAll {
  private var groupAdministration: GroupAdministration = _
  private var roleAdministration: RoleAdministration = _
  private val realm = "test-realm"

  private val testRoleName = "test"
  private val totoRoleName = "toto"
  
  implicit val defaultPatience: PatienceConfig = {
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(5, Millis))
  }

  private def cleanStuff() =  {
    Seq(testRoleName, totoRoleName).foreach(roleAdministration.deleteRole(_))
    GroupTestUtil.clean(groupAdministration)
  }

  override def beforeAll(): Unit = {
    super.beforeAll()
    val keycloakAdministration = KeycloakAdministration(realm)
    groupAdministration = keycloakAdministration.groupAdministration
    roleAdministration = keycloakAdministration.roleAdministration

  }

  override def beforeEach(): Unit = {
    cleanStuff()
    super.beforeEach()
    val groups = List(
      group("group1b", "group1_role"),
      group("group1", "group1_role")
    )
    GroupTestUtil.createGroups(groups, groupAdministration)
  }

  "listAvailableRoles" should "work" in {
    roleAdministration.listAvailableRoles().futureValue shouldEqual RoleTestUtil.roles(
      "nobody_as_it",
      "son_a",
      "son_b",
      "son_c",
      "another_role",
      "group1_role",
      "composite")
  }

  "getRoleByName" should "return Nothing if role does not exist" in {
    roleAdministration.getRoleByName("ze_role_that_does_not_exist").futureValue shouldBe None
  }

  it should "return existing roles" in {
    roleAdministration.getRoleByName("son_a").futureValue.map(_.name) shouldBe Some("son_a")
  }

  "listUsersWithRole" should "work" in {
    roleAdministration.listUsersWithRole(Role("nobody_as_it"), 0, 100, UserSort.ByUsername(true)).futureValue.users shouldEqual Seq()
    roleAdministration.listUsersWithRole(Role("composite"), 0, 100, UserSort.ByUsername(true)).futureValue.users.map(_._2.username).toSet shouldEqual Set(
      "usertest", "another.usertest"
    )

    roleAdministration.listUsersWithRole(Role("composite"), 0, 100, UserSort.ByUsername(true), filter = Some(UserAdministration.Filter(UserAdministration.FilterField.Username, "another.*".r))).futureValue.users.map(_._2.username).toSet shouldEqual Set(
      "another.usertest"
    )
    roleAdministration.listUsersWithRole(Role("composite"), 0, 100, UserSort.ByUsername(true), requiredRole = Some(Role("another_role"))).futureValue.users.map(_._2.username).toSet shouldEqual Set(
      "another.usertest"
    )
  }

  "listGroupsWithRole" should "work" in {
    roleAdministration.listGroupsWithRole(Role("nobody_as_it"), 0, 100, GroupSort.ByName(true)).futureValue.groups shouldEqual Seq()
    roleAdministration.listGroupsWithRole(Role("group1_role"), 0, 100, GroupSort.ByName(true)).futureValue.groups.map(_._2.name).toSet shouldEqual Set(
      "group1", "group1b"
    )
  }
  
  "addUpdateDeleteRole" should "work" in {
    roleAdministration.getRoleByName(testRoleName).futureValue shouldEqual None
    
    val initialRole = Role(testRoleName)
    roleAdministration.addRole(initialRole).futureValue
    
    roleAdministration.getRoleByName(testRoleName).futureValue shouldEqual Some(initialRole)
    
    val updatedRole = initialRole.copy(name = totoRoleName)
    
    roleAdministration.modifyRole(testRoleName, updatedRole).futureValue

    roleAdministration.getRoleByName(testRoleName).futureValue shouldEqual None
    roleAdministration.getRoleByName(totoRoleName).futureValue shouldEqual Some(updatedRole)
    
    roleAdministration.deleteRole(totoRoleName).futureValue

    roleAdministration.getRoleByName(totoRoleName).futureValue shouldEqual None
  }
}
