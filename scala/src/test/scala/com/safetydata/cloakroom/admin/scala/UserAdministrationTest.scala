package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.exception.UserIdNotFoundException
import com.safetydata.cloakroom.admin.scala.GroupTestUtil.group
import com.safetydata.cloakroom.admin.scala.KeycloakModificationError.RoleNotFound
import com.safetydata.cloakroom.mail.FakeSMTPChecker
import org.scalatest.Inside.inside
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpecLike
import org.scalatest.matchers.should.Matchers
import org.scalatest.time.{Millis, Seconds, Span}
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach}

import java.time.ZonedDateTime
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}


class UserAdministrationTest extends AnyFlatSpecLike with Matchers with BeforeAndAfterEach with ScalaFutures with BeforeAndAfterAll {
  private var userAdministration: UserAdministration = _
  private val testUsername = "cloakroom_test_user"
  private val user  = User(
    username = testUsername,
    email = Some(testUsername + "@panda.com"),
    firstName = Some("Jup"),
    lastName = Some("Yter"),
    enable = false,
    attributes = Map("mood" -> Seq("happy", "lucky")),
    roles = RoleTestUtil.roles("son_a", "son_b"),
    groups = Set.empty,
  )
  private val totoUsername = "toto"
  private val titiUsername = "titi"
  private val invisibleUser = "notvisible1"
  private val realm = "test-realm"
  private val smtpChecker: FakeSMTPChecker = FakeSMTPChecker.fromTypesafeConf()
  
  private var groupAdministration: GroupAdministration = _

  implicit val defaultPatience: PatienceConfig = {
    PatienceConfig(timeout = Span(5, Seconds), interval = Span(5, Millis))
  }

  private def cleanStuff() =  {
    smtpChecker.clearAllMail()
    Seq(
      testUsername,
      totoUsername,
      titiUsername,
      invisibleUser
    ).flatten(userAdministration.getUserByUsername(_).futureValue)
      .foreach{case (userId, _) =>
        userAdministration.deleteUser(userId).futureValue
      }
  }

  override def beforeAll(): Unit = {
    super.beforeAll()
    val keycloakAdministration = KeycloakAdministration(realm, Map("son_a" -> 2))
    groupAdministration = keycloakAdministration.groupAdministration
    userAdministration = keycloakAdministration.userAdministration
  }

  override def beforeEach(): Unit = {
    cleanStuff()
    GroupTestUtil.clean(groupAdministration)
    super.beforeEach()
    val groups = List(
      group("group1", "group1_role"),
      group("group2", "son_b"),
      group("group3")
    )
    GroupTestUtil.createGroups(groups, groupAdministration)
  }

  private def removeCreateDate(user: User): User = user.copy(createDate = None)

  "addUser and getUserById" should "work" in {
    val now = ZonedDateTime.now()
    val userId = userAdministration.addUser(user).futureValue.getOrElse(throw new Exception("Expected UserId"))
    val createdUser = userAdministration.getUserById(userId).futureValue
    removeCreateDate(createdUser) shouldEqual user
    createdUser.createDate.get should be > now
  }

  "addUser" should "not let you create more users for this role than the defined limit" in {
    userAdministration.addUser(user).futureValue.isRight shouldBe true
    userAdministration.addUser(
      user.copy(username = totoUsername, email = Some("username1@yop.com"))
    ).futureValue.isRight shouldBe true

    val problematicUser = user.copy(username = titiUsername, email = Some("username2@yop.com"))
    userAdministration.addUser(problematicUser).futureValue shouldEqual Left(
      KeycloakModificationError.LimitOfUserPerRolesReached(problematicUser, RoleTestUtil.roles("son_a"))
    )
  }

  it should "not let you create a user with an already used username" in {
    userAdministration.addUser(user).futureValue.isRight shouldBe true
    userAdministration.addUser(User(username = user.username, enable = false).withRoles(Set())).futureValue shouldEqual Left(
      KeycloakModificationError.UserAlreadyExists(user.username)
    )
  }

  it should "not let you create a user with an already used email" in {
    userAdministration.addUser(user).futureValue.isRight shouldBe true
    userAdministration.addUser(User(username = titiUsername, enable = false, email = user.email).withRoles(Set())).futureValue shouldEqual Left(
      KeycloakModificationError.EmailAlreadyUsed(user.email.get)
    )
  }
  
  it should "add groups when requested" in {
    val userWithGroups = user.withGroups(GroupTestUtil.groups("group1", "group3"))
    val userId = userAdministration.addUser(userWithGroups).futureValue.getOrElse(throw new Exception("Not expected"))
    val createdUser = userAdministration.getUserById(userId).futureValue
    removeCreateDate(createdUser) shouldEqual userWithGroups
  }

  "modify" should "not let you create more user for this role than the defined limit" in {
    userAdministration.addUser(user).futureValue.isRight shouldBe true
    userAdministration.addUser(
      user.copy(username = totoUsername, email = Some("username1@yop.com"))
    ).futureValue.isRight shouldBe true

    val problematicUser = user.copy(username = titiUsername, email = Some("username2@yop.com"), roles = Set.empty, attributes = Map.empty)
    val problematicUserId = userAdministration.addUser(problematicUser).futureValue.getOrElse(throw new Exception("Not expected"))
    inside(userAdministration.modifyUser(problematicUserId, UserModifier.addRoles(RoleTestUtil.roles("son_a"))).futureValue) {
      case Left(KeycloakModificationError.LimitOfUserPerRolesReached(user, roles)) =>
        removeCreateDate(user) shouldEqual problematicUser
        roles shouldEqual RoleTestUtil.roles("son_a")
    }
  }

  it should "not let you create a user with an already used email" in {
    userAdministration.addUser(user).futureValue.isRight shouldBe true
    val problematicUserId = userAdministration.addUser(User(username = titiUsername, enable = false).withRoles(Set())).futureValue.getOrElse(throw new Exception("Not expected"))
    userAdministration.modifyUser(problematicUserId, UserModifier.email(user.email.get)).futureValue shouldEqual Left(
      KeycloakModificationError.EmailAlreadyUsed(user.email.get)
    )
  }

  "listUsers" should "sort users in forward and reverse order" in {
    val expected = Seq("another.usertest", "cloakroom.admin", "usertest", "userwithoutanyrole")
    userAdministration
      .listUsers(0, 9, UserSort.ByUsername(true))
      .futureValue
      .users
      .map(_._2.username) shouldEqual expected

    userAdministration
      .listUsers(0, 9, UserSort.ByUsername(false))
      .futureValue
      .users
      .map(_._2.username) shouldEqual expected.reverse
  }

  it should "work with pagination" in {
    val results = userAdministration
      .listUsers(1, 2)
      .futureValue

    results.users.map(_._2.username) shouldEqual Seq("cloakroom.admin", "usertest")
    results.totalCount shouldEqual 4

    val results2 = userAdministration
      .listUsers(3, 1)
      .futureValue

    results2.users.map(_._2.username) shouldEqual Seq("userwithoutanyrole")
    results2.totalCount shouldEqual 4
  }

  it should "sort by email" in {
    // usertest: test@safety-data.com
    // another.usertest: another.test@safety-data.com
    // userwithoutanyrole: userwithoutanyrole@safety-data.com
    // cloakroom.admin: admin@safety-data.com
    val expected = Seq("cloakroom.admin", "another.usertest", "usertest", "userwithoutanyrole")
    userAdministration
      .listUsers(0, 9, UserSort.ByEmail(true))
      .futureValue
      .users
      .map(_._2.username) shouldEqual expected

    userAdministration
      .listUsers(0, 9, UserSort.ByEmail(false))
      .futureValue
      .users
      .map(_._2.username) shouldEqual expected.reverse
  }

  it should "sort by create date" in {
    val usersByCreateDate = userAdministration
      .listUsers(0, 9, UserSort.ByCreateDate(true))
      .futureValue
      .users

    val sortedUsers = usersByCreateDate.sortBy(_._2.createDate)

    usersByCreateDate shouldEqual sortedUsers

    userAdministration
      .listUsers(0, 9, UserSort.ByCreateDate(false))
      .futureValue
      .users shouldEqual sortedUsers.reverse
  }

  it should "sort by attribute" in {
    //        "username": "usertest",
    //        "attributes" : {
    //            "fruit" : [ "banana" ],
    //            "date": [ "2000-01-01T00:00Z" ],
    //            "number": [ "42" ]
    //        }
    //        "username": "another.usertest",
    //        "attributes" : {
    //            "fruit" : [ "apple" ],
    //            "date": [ "2003-01-01T00:00Z" ],
    //            "number": [ "43" ]
    //        }
    //        "username": "userwithoutanyrole",
    //        "clientRoles": {},
    //        "attributes" : {
    //            "fruit" : [ "orange" ],
    //            "date": [ "1999-01-01T00:00Z" ],
    //            "number": [ "44" ]
    //        }

    val fruitExpected = Seq("cloakroom.admin", "another.usertest", "usertest", "userwithoutanyrole")
    userAdministration
      .listUsers(0, 9, UserSort.ByAttribute("fruit", SortUtils.AttributeType.String, true))
      .futureValue
      .users
      .map(_._2.username) shouldEqual fruitExpected

    userAdministration
      .listUsers(0, 9, UserSort.ByAttribute("fruit", SortUtils.AttributeType.String, false))
      .futureValue
      .users
      .map(_._2.username) shouldEqual fruitExpected.reverse

    val fruitExpectedMissingLast = Seq("another.usertest", "usertest", "userwithoutanyrole", "cloakroom.admin")
    userAdministration
      .listUsers(0, 9, UserSort.ByAttribute("fruit", SortUtils.AttributeType.String, true, false))
      .futureValue
      .users
      .map(_._2.username) shouldEqual fruitExpectedMissingLast

    userAdministration
      .listUsers(0, 9, UserSort.ByAttribute("fruit", SortUtils.AttributeType.String, false, false))
      .futureValue
      .users
      .map(_._2.username) shouldEqual fruitExpectedMissingLast.reverse

    val dateExpected = Seq("cloakroom.admin", "userwithoutanyrole", "usertest", "another.usertest")
    userAdministration
      .listUsers(0, 9, UserSort.ByAttribute("date", SortUtils.AttributeType.DateTime, true))
      .futureValue
      .users
      .map(_._2.username) shouldEqual dateExpected

    userAdministration
      .listUsers(0, 9, UserSort.ByAttribute("date", SortUtils.AttributeType.DateTime, false))
      .futureValue
      .users
      .map(_._2.username) shouldEqual dateExpected.reverse

    val dateExpectedMissingLast = Seq("userwithoutanyrole", "usertest", "another.usertest", "cloakroom.admin")
    userAdministration
      .listUsers(0, 9, UserSort.ByAttribute("date", SortUtils.AttributeType.DateTime, true, false))
      .futureValue
      .users
      .map(_._2.username) shouldEqual dateExpectedMissingLast

    userAdministration
      .listUsers(0, 9, UserSort.ByAttribute("date", SortUtils.AttributeType.DateTime, false, false))
      .futureValue
      .users
      .map(_._2.username) shouldEqual dateExpectedMissingLast.reverse

    val intExpected = Seq("cloakroom.admin", "usertest", "another.usertest", "userwithoutanyrole")
    userAdministration
      .listUsers(0, 9, UserSort.ByAttribute("number", SortUtils.AttributeType.Integer, true))
      .futureValue
      .users
      .map(_._2.username) shouldEqual intExpected

    userAdministration
      .listUsers(0, 9, UserSort.ByAttribute("number", SortUtils.AttributeType.Integer, false))
      .futureValue
      .users
      .map(_._2.username) shouldEqual intExpected.reverse

    val intExpectedMissingLast = Seq("usertest", "another.usertest", "userwithoutanyrole", "cloakroom.admin")
    userAdministration
      .listUsers(0, 9, UserSort.ByAttribute("number", SortUtils.AttributeType.Integer, true, false))
      .futureValue
      .users
      .map(_._2.username) shouldEqual intExpectedMissingLast

    userAdministration
      .listUsers(0, 9, UserSort.ByAttribute("number", SortUtils.AttributeType.Integer, false, false))
      .futureValue
      .users
      .map(_._2.username) shouldEqual intExpectedMissingLast.reverse
  }

  it should "apply a filter on username" in {
    val expected = Seq("another.usertest", "usertest")

    val actual = userAdministration
      .listUsers(0, 9, UserSort.ByUsername(true), Some(UserAdministration.Filter(UserAdministration.FilterField.Username, ".*usertest.*".r)))
      .futureValue

    actual.users.map(_._2.username) shouldEqual expected
    actual.totalCount shouldEqual 2
  }

  it should "apply a filter by email" in {
    // usertest: test@safety-data.com
    // another.usertest: another.test@safety-data.com
    // userwithoutanyrole: userwithoutanyrole@safety-data.com
    // cloakroom.admin: admin@safety-data.com
    val expected = Seq("another.usertest", "usertest")

    val actual = userAdministration
      .listUsers(0, 9, UserSort.ByUsername(true), Some(UserAdministration.Filter(UserAdministration.FilterField.Email, ".*test@safety-data.com".r)))
      .futureValue

    actual.users.map(_._2.username) shouldEqual expected
    actual.totalCount shouldEqual 2
  }

  it should "apply a filter by role" in {
    val expected = Seq("another.usertest", "usertest")

    val actual = userAdministration
      .listUsers(0, 9, UserSort.ByUsername(true), None, Some(Role("composite")))
      .futureValue

    actual.users.map(_._2.username) shouldEqual expected
    actual.totalCount shouldEqual 2
  }

  "getUserByUsername" should "correctly work" in {
    val userId = userAdministration.addUser(user).futureValue.getOrElse(throw new Exception("Not expected"))
    val (retrievedId, retrievedUser) = userAdministration.getUserByUsername(user.username).futureValue.get
    retrievedId shouldEqual userId
    removeCreateDate(retrievedUser) shouldEqual user
  }

  "modifyUser" should "allow to disable or enable user" in {
    val userId = userAdministration.addUser(user).futureValue.getOrElse(throw new Exception("Not expected"))
    val activatedUser = userAdministration.modifyUser(userId, UserModifier.activate).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(activatedUser) shouldEqual user.copy(enable = true)
    activatedUser shouldEqual userAdministration.getUserById(userId).futureValue

    val disabledUser = userAdministration.modifyUser(userId, UserModifier.disable).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(disabledUser) shouldEqual user
    disabledUser shouldEqual userAdministration.getUserById(userId).futureValue
  }

  it should "allow to change firstName" in {
    val newFirstName = "vadlimir"
    val userId = userAdministration.addUser(user).futureValue.getOrElse(throw new Exception("Not expected"))
    val newUser = userAdministration.modifyUser(userId, UserModifier.firstName(newFirstName)).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(newUser) shouldEqual user.copy(firstName = Some(newFirstName))
    newUser shouldEqual userAdministration.getUserById(userId).futureValue
  }

  it should "allow to change lastName" in {
    val newLastName = "Poutine"
    val userId = userAdministration.addUser(user).futureValue.getOrElse(throw new Exception("Not expected"))
    val newUser = userAdministration.modifyUser(userId, UserModifier.lastName(newLastName)).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(newUser) shouldEqual user.copy(lastName = Some(newLastName))
    newUser shouldEqual userAdministration.getUserById(userId).futureValue
  }

  it should "allow to set required actions" in {
    val newRequiredAction: Set[UserAction] = Set(UserAction.ConfigureTotp, UserAction.UpdateProfile, UserAction.UpdatePassword, UserAction.VerifyEmail)
    val userId = userAdministration.addUser(user).futureValue.getOrElse(throw new Exception("Not expected"))
    val newUser = userAdministration.modifyUser(userId, UserModifier.requiredActions(newRequiredAction)).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(newUser) shouldEqual user.copy(requiredActions = newRequiredAction)
    newUser shouldEqual userAdministration.getUserById(userId).futureValue
  }

  it should "allow to set required actions (1)" in {
    val newRequiredAction: Set[UserAction] = Set(UserAction.ConfigureTotp, UserAction.UpdateProfile, UserAction.UpdatePassword, UserAction.VerifyEmail)
    val userId = userAdministration.addUser(user).futureValue.getOrElse(throw new Exception("Not expected"))
    val newUser = userAdministration.modifyUser(userId, UserModifier.requiredActions(newRequiredAction)).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(newUser) shouldEqual user.copy(requiredActions = newRequiredAction)
    newUser shouldEqual userAdministration.getUserById(userId).futureValue
  }

  it should "allow to set required actions (2)" in {
    val userId = userAdministration
      .addUser(user.copy(
        requiredActions = Set(UserAction.ConfigureTotp,
          UserAction.UpdateProfile,
          UserAction.UpdatePassword,
          UserAction.VerifyEmail)))
      .futureValue.getOrElse(throw new Exception("Not expected"))
    val newRequiredAction: Set[UserAction] = Set(UserAction.ConfigureTotp)
    val newUser = userAdministration.modifyUser(userId, UserModifier.requiredActions(newRequiredAction)).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(newUser) shouldEqual user.copy(requiredActions = newRequiredAction)
    newUser shouldEqual userAdministration.getUserById(userId).futureValue
  }

  it should "allow to add roles" in {
    val userId = userAdministration.addUser(user.copy(roles = RoleTestUtil.roles("son_a"))).futureValue.getOrElse(throw new Exception("Not expected"))
    val newUser = userAdministration.modifyUser(userId, UserModifier.addRoles(RoleTestUtil.roles("son_b"))).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(newUser) shouldEqual user.copy(roles = RoleTestUtil.roles("son_a", "son_b"))
    newUser shouldEqual userAdministration.getUserById(userId).futureValue
  }

  it should "not allow to add a non-existent role" in {
    val userId = userAdministration.addUser(user.copy(roles = RoleTestUtil.roles("son_a"))).futureValue.getOrElse(throw new Exception("Not expected"))
    userAdministration.modifyUser(userId, UserModifier.addRoles(RoleTestUtil.roles("yolo"))).futureValue shouldEqual Left(
      RoleNotFound("yolo")
    )
  }

  it should "allow to remove roles" in {
    val userId = userAdministration.addUser(user).futureValue.getOrElse(throw new Exception("Not expected"))
    val newUser = userAdministration.modifyUser(userId, UserModifier.addRoles(RoleTestUtil.roles("son_b"))).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(newUser) shouldEqual user.copy(roles = RoleTestUtil.roles("son_a", "son_b"))
    newUser shouldEqual userAdministration.getUserById(userId).futureValue
  }

  it should "allow to set roles" in {
    val userId = userAdministration.addUser(user.copy(roles = RoleTestUtil.roles("son_a"))).futureValue.getOrElse(throw new Exception("Not expected"))
    val newUser = userAdministration.modifyUser(userId, UserModifier.setRoles(RoleTestUtil.roles("son_b"))).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(newUser) shouldEqual user.copy(roles = RoleTestUtil.roles("son_b"))
    newUser shouldEqual userAdministration.getUserById(userId).futureValue
  }

  it should "set groups when requested" in {
    val userWithGroups = user.withGroups(GroupTestUtil.groups("group1", "group3"))
    val userId = userAdministration.addUser(userWithGroups).futureValue.getOrElse(throw new Exception("Not expected"))
    val userModifier = UserModifier(groupModifier = Some(UserGroupModifier.Set(GroupTestUtil.groups("group2", "group3"))))

    val updatedUser = userAdministration.modifyUser(userId, userModifier).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(updatedUser) shouldEqual user.withGroups(GroupTestUtil.groups("group2", "group3"))
  }
  
  it should "add groups when requested" in {
    val userWithGroups = user.withGroups(GroupTestUtil.groups("group1", "group3"))
    val userId = userAdministration.addUser(userWithGroups).futureValue.getOrElse(throw new Exception("Not expected"))
    val userModifier = UserModifier(groupModifier = Some(UserGroupModifier.Add(GroupTestUtil.groups("group2", "group3"))))

    val updatedUser = userAdministration.modifyUser(userId, userModifier).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(updatedUser) shouldEqual user.withGroups(GroupTestUtil.groups("group1", "group2", "group3"))
  }

  it should "remove groups when requested" in {
    val userWithGroups = user.withGroups(GroupTestUtil.groups("group1", "group3"))
    val userId = userAdministration.addUser(userWithGroups).futureValue.getOrElse(throw new Exception("Not expected"))
    val userModifier = UserModifier(groupModifier = Some(UserGroupModifier.Remove(GroupTestUtil.groups("group2", "group3"))))

    val updatedUser = userAdministration.modifyUser(userId, userModifier).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(updatedUser) shouldEqual user.withGroups(GroupTestUtil.groups("group1"))
  }

  it should "set attributes when requested" in {
    val userId = userAdministration.addUser(user).futureValue.getOrElse(throw new Exception("Not expected"))
    val newUser = userAdministration.modifyUser(userId, UserModifier.attributes(Map("nationality" -> Seq("French", "Nambian")))).futureValue.getOrElse(throw new Exception("Not expected"))
    removeCreateDate(newUser) shouldEqual user.copy(attributes = Map("nationality" -> Seq("French", "Nambian")))
    newUser shouldEqual userAdministration.getUserById(userId).futureValue
  }

  "deleteUser" should "work" in {
    val userId = userAdministration.addUser(user).futureValue.getOrElse(throw new Exception("Not expected"))
    userAdministration.deleteUser(userId)
    Thread.sleep(200)
    userAdministration.getUserById(userId).transform(Success(_)).futureValue should matchPattern {
      case Failure(exc: UserIdNotFoundException) =>
    }
  }

  "userRoles" should "work" in {
    val userId = userAdministration.addUser(user).futureValue.getOrElse(throw new Exception("Not expected"))
    userAdministration.userRoles(userId).futureValue shouldEqual user.roles
  }

  "effectiveUserRoles" should "work" in {
    val userId = userAdministration.addUser(user.copy(roles = RoleTestUtil.roles("composite"))).futureValue.getOrElse(throw new Exception("Not expected"))
    userAdministration.effectiveUserRoles(userId).futureValue shouldEqual RoleTestUtil.roles("composite", "son_a", "son_b")
  }

  "userCount" should "work" in {
    userAdministration.userCount().futureValue shouldEqual 4
  }
}
