package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.{admin => javaAdmin}

import scala.jdk.CollectionConverters._

object RoleTestUtil {
  def roles(roleName: String*):  Set[Role] = javaAdmin
    .RoleTestUtil
    .roles(roleName:_*)
    .asScala
    .map(Role.fromJava(_))
    .toSet

  def role(roleName: String): Role = Role.fromJava(javaAdmin.RoleTestUtil.role(roleName))
}
