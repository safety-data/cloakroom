package com.safetydata.cloakroom.scala

import com.safetydata.cloakroom.CloakroomConfig
import com.safetydata.cloakroom.publickeys.HttpGet
import com.safetydata.{cloakroom => javaCloakroom}

import scala.compat.java8.FutureConverters
import scala.concurrent.{ExecutionContext, Future}

object TokenVerifier {
  /**
   * Will use TypeSafe config at cloakroom.keycloak
   */
  def apply(realm: String): TokenVerifier =
    apply(javaCloakroom.TokenVerifier.fromTypeSafeConf(realm))

  private def apply(javaTokenVerifier: javaCloakroom.TokenVerifier): TokenVerifier = new TokenVerifierImpl(javaTokenVerifier)

  /**
   * Will use TypeSafe config at cloakroom.keycloak
   */
  def apply(httpGet: HttpGet, realm: String): TokenVerifier =
    apply(javaCloakroom.TokenVerifier.fromTypeSafeConf(httpGet, realm))

  def apply(config: CloakroomConfig, httpGet: HttpGet = null): TokenVerifier =
    if (httpGet != null)
      apply(javaCloakroom.TokenVerifier.fromConfig(config, httpGet))
    else
      apply(javaCloakroom.TokenVerifier.fromConfig(config))

  def fakeVerifier(result: Either[VerificationError, VerifiedToken]) = new TokenVerifier {
    override def apply(token: String)(implicit executionContext: ExecutionContext): Future[Either[VerificationError, VerifiedToken]] = Future.successful(result)
  }
}

trait TokenVerifier {
  def apply(token: String)(implicit executionContext: ExecutionContext): Future[Either[VerificationError, VerifiedToken]]
}

class TokenVerifierImpl private[scala](val javaTokenVerifier: javaCloakroom.TokenVerifier) extends TokenVerifier {
  def apply(token: String)(implicit executionContext: ExecutionContext): Future[Either[VerificationError, VerifiedToken]] = {
    FutureConverters.toScala(
      javaTokenVerifier.verify(token)).
      map(convertVerificationResultToEither)
  }

  private def convertVerificationResultToEither(verificationResult: javaCloakroom.VerificationResult): Either[VerificationError, VerifiedToken] = {
    if (verificationResult.isValid) {
      Right(VerifiedToken(verificationResult))
    } else {
      Left(VerificationError(verificationResult.error().get()))
    }
  }
}
