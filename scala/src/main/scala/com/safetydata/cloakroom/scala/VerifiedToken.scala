package com.safetydata.cloakroom.scala

import com.safetydata.cloakroom.VerificationResult
import org.keycloak.representations.AccessToken

import scala.collection.JavaConverters._
import scala.compat.java8.OptionConverters._

case class VerifiedToken private[scala](username: Option[String], email: Option[String], roles: Set[String], accessToken: AccessToken)

object VerifiedToken {
  def apply(verificationResult: VerificationResult): VerifiedToken = {
    if (!verificationResult.isValid) {
      throw new IllegalArgumentException("Should not construct token over invalid Verification result")
    }
    VerifiedToken(
      accessToken=verificationResult.token().get(),
      roles = verificationResult.roles().get().asScala.toSet,
      username = verificationResult.username().asScala,
      email = verificationResult.email().asScala
    )
  }

  def fake(username: Option[String], email: Option[String], roles: Set[String], forClient: String) = {
    val accessToken = new AccessToken();
    username.map(accessToken.setPreferredUsername(_))
    email.map(accessToken.setEmail(_))

    val resourceAccess = new AccessToken.Access();
    roles.foreach(resourceAccess.addRole(_))
    accessToken.setResourceAccess(java.util.Map.of(forClient, resourceAccess))

    VerifiedToken(
      username=username,
      email=email,
      roles=roles,
      accessToken=accessToken,
    )
  }
}
