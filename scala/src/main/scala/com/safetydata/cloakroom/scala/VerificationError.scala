package com.safetydata.cloakroom.scala

import com.safetydata.{cloakroom => javaCloakroom}

import scala.compat.java8.OptionConverters._

sealed trait VerificationError {
  val reason: String
}

case class InvalidToken(underlyingError: Option[Throwable], reason: String) extends VerificationError

case class CouldNotParseToken(underlyingError: Option[Throwable], reason: String) extends VerificationError

case class BadPublicKeys(reason: String) extends VerificationError

object VerificationError {
  private[cloakroom] def apply(javaVerificationError: javaCloakroom.VerificationError): VerificationError = {
    javaVerificationError match {
      case error: javaCloakroom.BadPublicKeys => BadPublicKeys(error.reason())
      case error: javaCloakroom.CouldNotParseToken => CouldNotParseToken(error.underlyingError().asScala, error.reason())
      case error: javaCloakroom.InvalidToken => InvalidToken(error.underlyingError().asScala, error.reason())
      case _ => throw new IllegalArgumentException(f"Unknown VerificationError: ${javaVerificationError.getClass}")
    }
  }
}
