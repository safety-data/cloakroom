package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.UserId

case class PaginatedUsers(users: Seq[(UserId, User)], totalCount: Int)
