package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.scala.UserAdministration.Filter
import com.safetydata.cloakroom.{admin => javaAdmin}

import java.util.Optional
import scala.compat.java8.OptionConverters.RichOptionalGeneric
import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.CollectionConverters._
import scala.jdk.OptionConverters.RichOption
import scala.util.matching.Regex

object RoleAdministration {
  private[scala] def apply(javaRoleAdministration: javaAdmin.RoleAdministration)(implicit exc: ExecutionContext): RoleAdministration = new RoleAdministrationImpl(javaRoleAdministration)
}

trait RoleAdministration {
  def listAvailableRoles(): Future[Set[Role]]
  def getRoleByName(roleName: String): Future[Option[Role]]
  def addRole(role: Role): Future[Unit]
  def deleteRole(roleName: String): Future[Unit]
  def modifyRole(roleName: String, role: Role): Future[Unit]

  def listUsersWithRole(role: Role, firstResult: Int, maxResults: Int, sort: UserSort, filter: Option[Filter] = None, requiredRole: Option[Role] = None): Future[PaginatedUsers]
  def listGroupsWithRole(role: Role, firstResult: Int, maxResults: Int, sort: GroupSort, nameFilter: Option[Regex] = None): Future[PaginatedGroups]
}

class RoleAdministrationImpl private[scala](val javaRoleAdministration: javaAdmin.RoleAdministration)(implicit exc: ExecutionContext) extends RoleAdministration {
  override def listAvailableRoles(): Future[Set[Role]] = Future {
    javaRoleAdministration.listAvailableRoles().asScala.map(Role.fromJava(_)).toSet
  }

  override def getRoleByName(roleName: String): Future[Option[Role]] = Future {
    javaRoleAdministration.getRoleByName(roleName).asScala.map(Role.fromJava(_))
  }

  override def addRole(role: Role): Future[Unit] = Future {
    javaRoleAdministration.addRole(role.toJaveRole)
  }

  override def deleteRole(roleName: String): Future[Unit] = Future {
    javaRoleAdministration.deleteRole(roleName)
  }

  override def modifyRole(roleName: String, role: Role): Future[Unit] = Future {
    javaRoleAdministration.modifyRole(roleName, role.toJaveRole);
  }

  override def listGroupsWithRole(role: Role, firstResult: Int, maxResults: Int, sort: GroupSort, nameFilter: Option[Regex]): Future[PaginatedGroups] = Future {
    val javaResult = javaRoleAdministration.listGroupsWithRole(role.toJaveRole, firstResult, maxResults, sort.toJava, Optional.ofNullable(nameFilter.map(_.pattern).orNull))
    PaginatedGroups(javaResult.groups().asScala.map(GroupWithSize.fromJava).toSeq, javaResult.totalCount())
  }
  
  override def listUsersWithRole(role: Role, firstResult: Int, maxResults: Int, sort: UserSort, filter: Option[Filter], requiredRole: Option[Role]): Future[PaginatedUsers] = Future {
    val javaResult = javaRoleAdministration.listUsersWithRole(role.toJaveRole, firstResult, maxResults, sort.toJava, filter.map(_.toJava).toJava, requiredRole.map(_.toJaveRole).toJava)
    PaginatedUsers(javaResult.users().asScala.map(User.fromJava).toSeq, javaResult.totalCount())
  }
}