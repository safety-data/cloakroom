package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.UserGroupModifierPackageAccess
import com.safetydata.cloakroom.{admin => javaAdmin}

sealed trait UserGroupModifier {
  def toJavaUserGroupModifier: javaAdmin.UserGroupModifier
}

object UserGroupModifier {
  case class Add(groups: collection.immutable.Set[Group]) extends UserGroupModifier {
    override def toJavaUserGroupModifier: javaAdmin.UserGroupModifier = UserGroupModifierPackageAccess.add(groups)
  }

  case class Remove(groups: collection.immutable.Set[Group]) extends UserGroupModifier {
    override def toJavaUserGroupModifier: javaAdmin.UserGroupModifier = UserGroupModifierPackageAccess.remove(groups)
  }

  case class Set(groups: collection.immutable.Set[Group]) extends UserGroupModifier {
    override def toJavaUserGroupModifier: javaAdmin.UserGroupModifier = UserGroupModifierPackageAccess.set(groups)
  }
}