package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.UserId
import com.safetydata.cloakroom.admin.scala.UserAdministration.Filter
import com.safetydata.cloakroom.{admin => javaAdmin}

import scala.compat.java8.OptionConverters.RichOptionalGeneric
import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.CollectionConverters._
import scala.jdk.OptionConverters.RichOption
import scala.util.matching.Regex

object UserAdministration {
  sealed trait FilterField {
    private[scala] def toJava: javaAdmin.User.FilterField
  }

  object FilterField {
    case object Username extends FilterField {
      private[scala] override def toJava: javaAdmin.User.FilterField = javaAdmin.User.FilterField.username
    }
    case object Email extends FilterField {
      private[scala] override def toJava: javaAdmin.User.FilterField = javaAdmin.User.FilterField.email
    }
  }

  case class Filter(field: FilterField, filter: Regex) {
    private[scala] def toJava: javaAdmin.User.Filter = new javaAdmin.User.Filter(field.toJava, filter.pattern)
  }

  private[scala] def apply(javaUserAdministration: javaAdmin.UserAdministration)(implicit exc: ExecutionContext): UserAdministration = new UserAdministrationImpl(javaUserAdministration)
}

trait UserAdministration {
  def addUser(user: User, sendEmail: Boolean = true): Future[Either[KeycloakModificationError, UserId]]
  def modifyUser(userId: UserId, modifier: UserModifier, sendEmail: Boolean = true): Future[Either[KeycloakModificationError, User]]
  def listUsers(firstResult: Int, maxResults: Int, sort: UserSort = UserSort.ByUsername(true), filter: Option[Filter] = None, requiredRole: Option[Role] = None): Future[PaginatedUsers]
  def deleteUser(userId: UserId): Future[Unit]
  def userRoles(userId: UserId): Future[Set[Role]]
  def effectiveUserRoles(userId: UserId): Future[Set[Role]]
  def userCount(): Future[Int]
  def getUserByUsername(username: String): Future[Option[(UserId, User)]]
  def getUserById(userId: UserId): Future[User]
  def refreshCache(): Future[Unit]
}

class UserAdministrationImpl private[scala](val javaUserAdministration: javaAdmin.UserAdministration)(implicit exc: ExecutionContext)
  extends UserAdministration
  with KeycloakErrorHandling
{

  override def addUser(user: User, sendEmail: Boolean): Future[Either[KeycloakModificationError, UserId]] = Future {
    handleCreationAndModifyError {
      javaUserAdministration.addUser(user.toJava, sendEmail).id()
    }
  }

  override def modifyUser(userId: UserId, modifier: UserModifier, sendEmail: Boolean): Future[Either[KeycloakModificationError, User]] = Future {
    handleCreationAndModifyError {
      User.fromJava(javaUserAdministration.modifyUser(userId, modifier.toJavaUserModifier, sendEmail).dropId())
    }
  }

  override def listUsers(firstResult: Int, maxResults: Int, sort: UserSort, filter: Option[Filter], requiredRole: Option[Role]): Future[PaginatedUsers] = Future {
    val javaResult = javaUserAdministration.listUsers(firstResult, maxResults, sort.toJava, filter.map(_.toJava).toJava, requiredRole.map(_.toJaveRole).toJava)
    PaginatedUsers(javaResult.users().asScala.map(User.fromJava).toSeq, javaResult.totalCount())
  }

  override def deleteUser(userId: UserId): Future[Unit] = Future {
    javaUserAdministration.deleteUser(userId)
  }

  override def userRoles(userId: UserId): Future[Set[Role]] = Future {
    javaUserAdministration.userRoles(userId).asScala.map(Role.fromJava(_)).toSet
  }

  override def effectiveUserRoles(userId: UserId): Future[Set[Role]] = Future {
    javaUserAdministration.effectiveUserRoles(userId).asScala.map(Role.fromJava(_)).toSet
  }

  override def userCount(): Future[Int] = Future {
    javaUserAdministration.userCount()
  }

  override def getUserByUsername(username: String): Future[Option[(UserId, User)]] = Future {
    javaUserAdministration.getUserByUsername(username).asScala.map(User.fromJava)
  }

  override def getUserById(userId: UserId): Future[User] = Future {
    User.fromJava(
      javaUserAdministration.getUserById(userId)
    )
  }

  override def refreshCache(): Future[Unit] = Future {
    javaUserAdministration.refreshCache()
  }
}