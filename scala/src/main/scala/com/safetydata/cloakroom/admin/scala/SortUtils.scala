package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.{admin => javaAdmin}

object SortUtils {
  sealed trait AttributeType {
    def toJava: javaAdmin.SortUtils.AttributeType
  }
  object AttributeType {
    case object String extends AttributeType {
      override def toJava: javaAdmin.SortUtils.AttributeType = javaAdmin.SortUtils.AttributeType.string
    }

    /**
     * Assumes the attribute is stored as a String parsable as an Integer, otherwise treats it as 0.
     */
    case object Integer extends AttributeType {
      override def toJava: javaAdmin.SortUtils.AttributeType = javaAdmin.SortUtils.AttributeType.integer
    }

    /**
     * Assumes the attribute is a ZonedDateTime in ISO-8601 format, otherwise treats it as the beginning of time.
     */
    case object DateTime extends AttributeType {
      override def toJava: javaAdmin.SortUtils.AttributeType = javaAdmin.SortUtils.AttributeType.dateTime
    }
  }
}
