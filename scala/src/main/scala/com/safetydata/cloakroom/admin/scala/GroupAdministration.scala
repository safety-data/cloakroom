package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.GroupId
import com.safetydata.cloakroom.admin.scala.UserAdministration.Filter
import com.safetydata.cloakroom.{admin => javaAdmin}

import java.util.Optional
import scala.compat.java8.OptionConverters.RichOptionalGeneric
import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.CollectionConverters._
import scala.jdk.OptionConverters.RichOption
import scala.util.matching.Regex

object GroupAdministration {
  private[scala] def apply(javaGroupAdministration: javaAdmin.GroupAdministration)(implicit exc: ExecutionContext): GroupAdministration = new GroupAdministrationImpl(javaGroupAdministration)
}

trait GroupAdministration {
  def addGroup(enrichedGroup: Group): Future[Either[KeycloakModificationError, GroupId]]
  def modifyGroup(groupId: GroupId, modifier: GroupModifier): Future[Either[KeycloakModificationError, GroupWithSize]]
  def deleteGroup(groupId: GroupId): Future[Unit]
  def getGroupByName(groupName: String): Future[Option[(GroupId, GroupWithSize)]]
  def getGroupById(groupId: GroupId): Future[GroupWithSize]
  def listGroups(firstResult: Int, maxResults: Int, sort: GroupSort = GroupSort.ByName(true), nameFilter: Option[Regex] = None): Future[PaginatedGroups]
  def groupCount(): Future[Long]
  def groupSize(groupId: GroupId): Future[Long]
  def groupMembers(groupId: GroupId, firstResult: Int, maxResults: Int, sort: UserSort = UserSort.ByUsername(true), filter: Option[Filter] = None, requiredRole: Option[Role] = None): Future[PaginatedUsers]
  def refreshCache(): Future[Unit]
}

class GroupAdministrationImpl private[scala](val javaGroupAdministration: javaAdmin.GroupAdministration)(implicit exc: ExecutionContext)
  extends GroupAdministration
  with KeycloakErrorHandling
{
  override def addGroup(enrichedGroup: Group): Future[Either[KeycloakModificationError, GroupId]] = Future {
    handleCreationAndModifyError {
      javaGroupAdministration.addGroup(enrichedGroup.toJava).id()
    }
  }

  override def getGroupByName(groupName: String): Future[Option[(GroupId, GroupWithSize)]] = Future {
    javaGroupAdministration.getGroupByName(groupName).asScala
      .map(GroupWithSize.fromJava(_))
  }

  override def getGroupById(groupId: GroupId): Future[GroupWithSize] = Future {
    GroupWithSize.fromJava(javaGroupAdministration.getGroupById(groupId))._2
  }

  override def groupCount(): Future[Long] = Future {
    javaGroupAdministration.groupCount()
  }

  override def modifyGroup(groupId: GroupId, modifier: GroupModifier): Future[Either[KeycloakModificationError, GroupWithSize]] = Future {
    handleCreationAndModifyError{
      GroupWithSize.fromJava(javaGroupAdministration.modifyGroup(groupId, modifier.toJavaGroupModifier))._2
    }
  }

  override def deleteGroup(groupId: GroupId): Future[Unit] = Future {
    javaGroupAdministration.deleteGroup(groupId)
  }

  override def groupSize(groupId: GroupId): Future[Long] = Future {
    javaGroupAdministration.groupSize(groupId)
  }

  override def groupMembers(groupId: GroupId, firstResult: Int, maxResults: Int, sort: UserSort, filter: Option[Filter], requiredRole: Option[Role]): Future[PaginatedUsers] = Future {
    val javaResult = javaGroupAdministration.groupMembers(groupId, firstResult, maxResults, sort.toJava, filter.map(_.toJava).toJava, requiredRole.map(_.toJaveRole).toJava)
    PaginatedUsers(javaResult.users().asScala.map(User.fromJava(_)).toSeq, javaResult.totalCount());
  }

  override def listGroups(firstResult: Int, maxResults: Int, sort: GroupSort = GroupSort.ByName(true), nameFilter: Option[Regex] = None): Future[PaginatedGroups] = Future {
    val javaResult = javaGroupAdministration.listGroups(firstResult, maxResults, sort.toJava, Optional.ofNullable(nameFilter.map(_.pattern).orNull))
    PaginatedGroups(javaResult.groups().asScala.map(GroupWithSize.fromJava(_)).toSeq, javaResult.totalCount())
  }

  override def refreshCache(): Future[Unit] = Future {
    javaGroupAdministration.refreshCache()
  }
}