package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.{admin => javaAdmin}
import enumeratum.{Enum, EnumEntry}

sealed trait UserAction extends EnumEntry {
  private [admin] def toJavaUserAction: javaAdmin.UserAction
}

object UserAction extends Enum[UserAction] {
  val values = findValues

  case object VerifyEmail extends UserAction {
    private [admin] override def toJavaUserAction: javaAdmin.UserAction = javaAdmin.UserAction.VERIFY_EMAIL
  }

  case object UpdateProfile extends UserAction {
    private [admin] override def toJavaUserAction: javaAdmin.UserAction = javaAdmin.UserAction.UPDATE_PROFILE
  }

  case object ConfigureTotp extends UserAction {
    private [admin] override def toJavaUserAction: javaAdmin.UserAction = javaAdmin.UserAction.CONFIGURE_TOTP
  }

  case object UpdatePassword extends UserAction {
    private [admin] override def toJavaUserAction: javaAdmin.UserAction = javaAdmin.UserAction.UPDATE_PASSWORD
  }

  def fromJava(action: javaAdmin.UserAction): UserAction = {
    action match {
      case javaAdmin.UserAction.VERIFY_EMAIL =>
        VerifyEmail
      case javaAdmin.UserAction.UPDATE_PROFILE =>
        UpdateProfile
      case javaAdmin.UserAction.CONFIGURE_TOTP =>
        ConfigureTotp
      case javaAdmin.UserAction.UPDATE_PASSWORD =>
        UpdatePassword
    }
  }
}
