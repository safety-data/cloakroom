package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.scala.SortUtils.AttributeType
import com.safetydata.cloakroom.{admin => javaAdmin}

sealed trait UserSort {
  private[scala] def toJava: javaAdmin.User.Sort
}

object UserSort {
  case class ByUsername(ascending: Boolean, caseInsensitive: Boolean = false) extends UserSort {
    private[scala] override def toJava: javaAdmin.User.Sort = new javaAdmin.User.SortByUsername(ascending)
  }
  case class ByEmail(ascending: Boolean, missingValuesFirst: Boolean = true, caseInsensitive: Boolean = false) extends UserSort {
    private[scala] override def toJava: javaAdmin.User.Sort = new javaAdmin.User.SortByEmail(ascending, missingValuesFirst)
  }
  case class ByCreateDate(ascending: Boolean, missingValuesFirst: Boolean = true) extends UserSort {
    private[scala] override def toJava: javaAdmin.User.Sort = new javaAdmin.User.SortByCreateDate(ascending, missingValuesFirst)
  }
  case class ByAttribute(name: String, attributeType: AttributeType, ascending: Boolean, missingValuesFirst: Boolean = true) extends UserSort {
    private[scala] override def toJava: javaAdmin.User.Sort = new javaAdmin.User.SortByAttribute(name, attributeType.toJava, ascending, missingValuesFirst)
  }
}