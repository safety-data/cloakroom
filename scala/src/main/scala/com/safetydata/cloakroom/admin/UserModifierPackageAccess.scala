package com.safetydata.cloakroom.admin

import compat.java8.OptionConverters.RichOptionForJava8
import collection.JavaConverters._

import com.safetydata.cloakroom.{admin => javaAdmin}

/**
 * This is only useful to expose package private java stuff
 * to subscala scala package without making it visible to API user
 */
private[admin] object UserModifierPackageAccess {
  private[admin] def constructJavaUserModifier(
    email: Option[String] = None,
    firstName: Option[String] = None,
    lastName: Option[String] = None,
    requiredActions: Option[Set[UserAction]] = None,
    enable: Option[Boolean] = None,
    attributes: Option[Map[String, Seq[String]]] = None,
    roleModifier: Option[RoleModifier] = None,
    groupModifier: Option[UserGroupModifier] = None,
  ): javaAdmin.UserModifier =
    new javaAdmin.UserModifier(
      email.asJava,
      firstName.asJava,
      lastName.asJava,
      requiredActions.map(_.asJava).asJava,
      enable.map(java.lang.Boolean.valueOf(_)).asJava,
      attributes.map(_.map(entry => entry._1 -> entry._2.asJava).asJava).asJava,
      roleModifier.getOrElse(RoleModifier.empty()),
      groupModifier.getOrElse(UserGroupModifier.empty())
    )
}
