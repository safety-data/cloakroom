package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.{admin => javaAdmin}

import java.time.ZonedDateTime
import scala.collection.JavaConverters._
import scala.compat.java8.OptionConverters.RichOptionalGeneric

case class User(
  username: String,
  enable: Boolean,
  email: Option[String] = None,
  firstName: Option[String] = None,
  lastName: Option[String] = None,
  requiredActions: Set[UserAction] = Set.empty,
  createDate: Option[ZonedDateTime] = None,
  attributes: Map[String, Seq[String]] = Map.empty,
  roles: Set[Role] = Set.empty,
  groups: Set[Group] = Set.empty,
) {

  def toJava: javaAdmin.User = {
    val builder = javaAdmin.User.builder()
      .setEnable(enable)
      .username(username)
      .attributes(attributes.map(entry => entry._1 -> entry._2.asJava).asJava)
      .roles(roles.map(_.toJaveRole).asJava)
      .groups(groups.map(_.toJava).asJava)

    email.foreach(builder.email)
    firstName.foreach(builder.firstName)
    lastName.foreach(builder.lastName)
    builder.requiredActions(requiredActions.map(_.toJavaUserAction).asJava)
    createDate.foreach(builder.createDate)

    builder.build()
  }

  def withRoles(roles: Set[Role]): User =
    this.copy(roles = roles)
  
  def withGroups(groups: Set[Group]): User =
    this.copy(groups = groups)

  def withAttributes(attributes: Map[String, Seq[String]]): User =
    this.copy(attributes = attributes)
}

object User {
  def fromJava(javaUser: javaAdmin.User): User = User(
    javaUser.username(),
    javaUser.isEnabled,
    javaUser.email().asScala,
    javaUser.firstName().asScala,
    javaUser.lastName().asScala,
    javaUser.requiredActions().asScala.toSet.map(UserAction.fromJava(_)),
    javaUser.createDate().asScala,
    javaUser.attributes().asScala.toMap.map(entry => entry._1 -> entry._2.asScala.toSeq),
    javaUser.roles().asScala.map(Role.fromJava(_)).toSet,
    javaUser.groups().asScala.map(Group.fromJava(_)).toSet,
  )

  def fromJava(javaUserWithId: javaAdmin.UserWithId): (javaAdmin.UserId, User) = javaUserWithId.id() -> fromJava(javaUserWithId.dropId())
}
