package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.{admin => javaAdmin}

import scala.jdk.CollectionConverters._

case class Group(name: String, attributes: Map[String, Seq[String]] = Map.empty, roles: Set[Role] = Set.empty) extends IGroup {
  def toJava: javaAdmin.Group =
    javaAdmin.Group.builder()
      .name(name)
      .attributes(attributes.map(entry => entry._1 -> entry._2.asJava).asJava)
      .roles(roles.map(_.toJaveRole).asJava)
      .build()

  def withRoles(roles: Set[Role]): Group =
    this.copy(roles = roles)

  def withAttributes(attributes: Map[String, Seq[String]]): Group =
    this.copy(attributes = attributes)
}

object Group {
  def fromJava(javaEnrichedGroup: javaAdmin.Group): Group = Group(
    javaEnrichedGroup.name(),
    javaEnrichedGroup.attributes().asScala.toMap.map(entry => entry._1 -> entry._2.asScala.toSeq),
    javaEnrichedGroup.roles().asScala.map(Role.fromJava(_)).toSet
  )

  def fromJava(javaGroupWithRolesAndId: javaAdmin.GroupWithId): (javaAdmin.GroupId, Group) =
    javaGroupWithRolesAndId.id() -> fromJava(javaGroupWithRolesAndId.dropId())
}
