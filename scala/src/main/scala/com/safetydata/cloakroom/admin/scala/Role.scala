package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.{admin => javaAdmin}

case class Role(name: String) {
  def toJaveRole: javaAdmin.Role = new javaAdmin.Role(name)
}

object Role {
  def fromJava(javaRole: javaAdmin.Role): Role = Role(
    javaRole.name()
  )
}