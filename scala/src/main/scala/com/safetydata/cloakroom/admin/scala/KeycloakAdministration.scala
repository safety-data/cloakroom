package com.safetydata.cloakroom.admin.scala

import scala.concurrent.ExecutionContext
import com.safetydata.cloakroom.{admin => javaAdmin}
import scala.jdk.CollectionConverters._

object KeycloakAdministration {
  def apply(realm: String, rolesWithMaxUsersLimitations: Map[String, Int] = Map.empty)(implicit exc: ExecutionContext): KeycloakAdministration =
    new KeycloakAdministrationImpl(realm, rolesWithMaxUsersLimitations)
}

trait KeycloakAdministration {
  def userAdministration: UserAdministration
  def groupAdministration: GroupAdministration
  def roleAdministration: RoleAdministration
}

class KeycloakAdministrationImpl private[scala](realm: String, rolesWithMaxUsersLimitations: Map[String, Int] = Map.empty)(implicit exc: ExecutionContext)
  extends KeycloakAdministration {
  val javaKeyloakAdministration: javaAdmin.KeycloakAdministration = javaAdmin.KeycloakAdministration.fromTypeSafeConf(
    realm,
    rolesWithMaxUsersLimitations.map{case (key, value) =>
      key -> java.lang.Integer.valueOf(value.intValue())
    }.asJava)

  override val userAdministration: UserAdministration = UserAdministration(javaKeyloakAdministration.getUserAdministration)

  override val groupAdministration: GroupAdministration = GroupAdministration(javaKeyloakAdministration.getGroupAdministration)

  override val roleAdministration: RoleAdministration = RoleAdministration(javaKeyloakAdministration.getRoleAdministration)
}