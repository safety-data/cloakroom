package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.GroupId

case class PaginatedGroups(groups: Seq[(GroupId, GroupWithSize)], totalCount: Int)
