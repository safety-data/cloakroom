package com.safetydata.cloakroom.admin.scala

sealed trait KeycloakModificationError

object KeycloakModificationError {
  case class UserAlreadyExists(username: String) extends KeycloakModificationError
  case class EmailAlreadyUsed(email: String) extends KeycloakModificationError
  case class LimitOfUserPerRolesReached(user: User, rolesExceedMax: Set[Role]) extends KeycloakModificationError
  case class RoleNotFound(roleName: String) extends KeycloakModificationError
  case class GroupNotFound(groupName: String) extends KeycloakModificationError
  case class GroupAlreadyExists(name: String) extends KeycloakModificationError
}
