package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.GroupModifierPackageAccess
import com.safetydata.cloakroom.{admin => javaAdmin}


case class GroupModifier(name: String,
  attributes: Option[Map[String, Seq[String]]] = None,
  roleModifier: Option[RoleModifier] = None
) {

  def toJavaGroupModifier: javaAdmin.GroupModifier = {
    GroupModifierPackageAccess.constructJavaGroupModifier(
      name,
      attributes,
      roleModifier.map(_.toJavaRoleModifier)
    )
  }

  def withName(name: String) = this.copy(name = name)
  def withAttributes(attributes: Map[String, Seq[String]]) = this.copy(attributes = Some(attributes))
  def addRoles(roles: Set[Role]) = this.copy(roleModifier = Some(RoleModifier.Add(roles)))
  def removeRoles(roles: Set[Role]) = this.copy(roleModifier = Some(RoleModifier.Remove(roles)))
  def setRoles(roles: Set[Role]) = this.copy(roleModifier = Some(RoleModifier.Set(roles)))
}

object GroupModifier {
  def forGroup(group: IGroup) = GroupModifier(group.name)
}