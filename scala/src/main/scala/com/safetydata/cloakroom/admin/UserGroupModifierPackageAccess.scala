package com.safetydata.cloakroom.admin

import collection.JavaConverters._

/**
 * This is only useful to expose package private java stuff
 * to subscala scala package without making it visible to API user
 */
private[admin] object UserGroupModifierPackageAccess {
  def add(groups: Set[scala.Group]): UserGroupModifier =
    UserGroupModifier.empty().add(groups.map(_.toJava).asJava)

  def remove(groups: Set[scala.Group]): UserGroupModifier =
    UserGroupModifier.empty().remove(groups.map(_.toJava).asJava)

  def set(groups: Set[scala.Group]): UserGroupModifier =
    UserGroupModifier.empty().set(groups.map(_.toJava).asJava)
}
