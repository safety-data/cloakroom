package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.GroupWithIdAndSize
import com.safetydata.cloakroom.{admin => javaAdmin}

case class GroupWithSize(group: Group, size: Long) extends IGroup {
  val name: String = group.name
  val attributes: Map[String, Seq[String]] = group.attributes
  val roles: Set[Role] = group.roles
}

object GroupWithSize {
  def fromJava(javaEnrichedGroupWithSize: GroupWithIdAndSize): (javaAdmin.GroupId, GroupWithSize) =
    (javaEnrichedGroupWithSize.id(), GroupWithSize(Group.fromJava(javaEnrichedGroupWithSize.dropId()), javaEnrichedGroupWithSize.size()))
}