package com.safetydata.cloakroom.admin

import com.safetydata.cloakroom.{admin => javaAdmin}
import _root_.scala.jdk.CollectionConverters._
import compat.java8.OptionConverters.RichOptionForJava8

/**
 * This is only useful to expose package private java stuff
 * to subscala scala package without making it visible to API user
 */
private[admin] object GroupModifierPackageAccess {
  private[admin] def constructJavaGroupModifier(
    name: String,
    attributes: Option[Map[String, Seq[String]]] = None,
    roleModifier: Option[RoleModifier] = None,
  ): javaAdmin.GroupModifier =
    new javaAdmin.GroupModifier(
      name,
      attributes.map(_.map(entry => entry._1 -> entry._2.asJava).asJava).asJava,
      roleModifier.getOrElse(RoleModifier.empty()),
    )
}
