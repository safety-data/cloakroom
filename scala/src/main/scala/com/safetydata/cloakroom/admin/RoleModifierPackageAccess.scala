package com.safetydata.cloakroom.admin

import _root_.scala.jdk.CollectionConverters._

/**
 * This is only useful to expose package private java stuff
 * to subscala scala package without making it visible to API user
 */
private[admin] object RoleModifierPackageAccess {
  def add(roles: Set[scala.Role]): RoleModifier =
    RoleModifier.empty().add(roles.map(_.toJaveRole).asJava)

  def remove(roles: Set[scala.Role]): RoleModifier =
    RoleModifier.empty().remove(roles.map(_.toJaveRole).asJava)

  def set(roles: Set[scala.Role]): RoleModifier =
    RoleModifier.empty().set(roles.map(_.toJaveRole).asJava)
}
