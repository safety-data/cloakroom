package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.exception._
import com.safetydata.cloakroom.admin.scala.KeycloakModificationError.{GroupNotFound, RoleNotFound}

import scala.collection.JavaConverters._

trait KeycloakErrorHandling {
  def handleCreationAndModifyError[T](value : => T): Either[KeycloakModificationError, T] = {
    try {
      Right(value)
    } catch {
      case roleNotFoundException: RoleNotFoundException =>
        Left(RoleNotFound(roleNotFoundException.roleName))
      case groupNotFoundException: GroupNotFoundException =>
        Left(GroupNotFound(groupNotFoundException.name))
      case emailAlreadyUsed: EmailAlreadyUsedException =>
        Left(KeycloakModificationError.EmailAlreadyUsed(emailAlreadyUsed.email))
      case userAlreadyExists: UserAlreadyExistsException =>
        Left(KeycloakModificationError.UserAlreadyExists(userAlreadyExists.username))
      case limitOfRoleReached: LimitOfUserPerRolesReachedException =>
        Left(KeycloakModificationError.LimitOfUserPerRolesReached(User.fromJava(limitOfRoleReached.user), limitOfRoleReached.rolesExceedsMax.asScala.map(Role.fromJava(_)).toSet))
      case groupAlreadyExists: GroupAlreadyExistsException =>
        Left(KeycloakModificationError.GroupAlreadyExists(groupAlreadyExists.name))
    }
  }
}
