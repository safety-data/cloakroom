package com.safetydata.cloakroom.admin
package scala

import com.safetydata.cloakroom.{admin => javaAdmin}


case class UserModifier(
  email: Option[String] = None,
  firstName: Option[String] = None,
  lastName: Option[String] = None,
  requiredActions: Option[Set[UserAction]] = None,
  enable: Option[Boolean] = None,
  attributes: Option[Map[String, Seq[String]]] = None,
  roleModifier: Option[RoleModifier] = None,
  groupModifier: Option[UserGroupModifier] = None
) {

  def toJavaUserModifier: javaAdmin.UserModifier = {
    UserModifierPackageAccess.constructJavaUserModifier(
      email,
      firstName,
      lastName,
      requiredActions.map(_.map(_.toJavaUserAction)),
      enable,
      attributes,
      roleModifier.map(_.toJavaRoleModifier),
      groupModifier.map(_.toJavaUserGroupModifier)
    )
  }

  def email(email: String): UserModifier = this.copy(email = Some(email))
  def firstName(firstName: String): UserModifier = this.copy(firstName = Some(firstName))
  def lastName(lastName: String): UserModifier = this.copy(lastName = Some(lastName))
  def activate: UserModifier = this.copy(enable = Some(true))
  def disable: UserModifier = this.copy(enable = Some(false))
  def attributes(attributes: Map[String, Seq[String]]): UserModifier = this.copy(attributes = Some(attributes))
  def requiredActions(actions: Set[UserAction]) = this.copy(requiredActions = Some(actions))
  def addRoles(roles: Set[Role]) = this.copy(roleModifier = Some(RoleModifier.Add(roles)))
  def removeRoles(roles: Set[Role]) = this.copy(roleModifier = Some(RoleModifier.Remove(roles)))
  def setRoles(roles: Set[Role]) = this.copy(roleModifier = Some(RoleModifier.Set(roles)))
  def addGroups(groups: Set[Group]) = this.copy(groupModifier = Some(UserGroupModifier.Add(groups)))
  def removeGroups(groups: Set[Group]) = this.copy(groupModifier = Some(UserGroupModifier.Remove(groups)))
  def setGroups(groups: Set[Group]) = this.copy(groupModifier = Some(UserGroupModifier.Set(groups)))
}

object UserModifier {
  def email(email: String) = UserModifier(email = Some(email))
  def firstName(firstName: String) = UserModifier(firstName = Some(firstName))
  def lastName(lastName: String) = UserModifier(lastName = Some(lastName))
  def activate = UserModifier(enable = Some(true))
  def disable = UserModifier(enable = Some(false))
  def requiredActions(actions: Set[UserAction]) = UserModifier(requiredActions = Some(actions))
  def attributes(attributes: Map[String, Seq[String]]) = UserModifier(attributes = Some(attributes))
  def addRoles(roles: Set[Role]) = UserModifier(roleModifier = Some(RoleModifier.Add(roles)))
  def removeRoles(roles: Set[Role]) = UserModifier(roleModifier = Some(RoleModifier.Remove(roles)))
  def setRoles(roles: Set[Role]) = UserModifier(roleModifier = Some(RoleModifier.Set(roles)))
  def addGroups(groups: Set[Group]) = UserModifier(groupModifier = Some(UserGroupModifier.Add(groups)))
  def removeGroups(groups: Set[Group]) = UserModifier(groupModifier = Some(UserGroupModifier.Remove(groups)))
  def setGroups(groups: Set[Group]) = UserModifier(groupModifier = Some(UserGroupModifier.Set(groups)))
}
