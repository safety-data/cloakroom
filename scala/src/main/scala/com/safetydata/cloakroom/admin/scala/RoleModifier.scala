package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.RoleModifierPackageAccess
import com.safetydata.cloakroom.{admin => javaAdmin}

sealed trait RoleModifier {
  def toJavaRoleModifier: javaAdmin.RoleModifier
}

object RoleModifier {
  case class Add(roles: collection.immutable.Set[Role]) extends RoleModifier {
    override def toJavaRoleModifier: javaAdmin.RoleModifier = RoleModifierPackageAccess.add(roles)
  }

  case class Remove(roles: collection.immutable.Set[Role]) extends RoleModifier {
    override def toJavaRoleModifier: javaAdmin.RoleModifier = RoleModifierPackageAccess.remove(roles)
  }

  case class Set(roles: collection.immutable.Set[Role]) extends RoleModifier {
    override def toJavaRoleModifier: javaAdmin.RoleModifier = RoleModifierPackageAccess.set(roles)
  }
}
