package com.safetydata.cloakroom.admin.scala

import com.safetydata.cloakroom.admin.scala.SortUtils.AttributeType

import com.safetydata.cloakroom.{admin => javaAdmin}

sealed trait GroupSort {
  private[scala] def toJava: javaAdmin.Group.Sort
}

object GroupSort {
  case class ByName(ascending: Boolean, caseInsensitive: Boolean = true) extends GroupSort {
    private[scala] override def toJava: javaAdmin.Group.Sort = new javaAdmin.Group.SortByGroupName(ascending, caseInsensitive)
  }
  case class BySize(ascending: Boolean) extends GroupSort {
    private[scala] override def toJava: javaAdmin.Group.Sort = new javaAdmin.Group.SortBySize(ascending)
  }
  case class ByAttribute(name: String, attributeType: AttributeType, ascending: Boolean, missingValuesFirst: Boolean = true) extends GroupSort {
    private[scala] override def toJava: javaAdmin.Group.Sort = new javaAdmin.Group.SortByAttribute(name, attributeType.toJava, ascending, missingValuesFirst)
  }
}
