import sbt.Keys.version

val scala213 = "2.13.10"
val scala3_2 = "3.2.2"

val keycloakVersion = "22.0.3"
val scalaTestVersion = "3.2.9"
val junitVersion = "4.13.2"
val junitInterfaceVersion = "0.13.2"

ThisBuild / version := "0.5.15"
ThisBuild / organization := "com.safety-data"
ThisBuild / organizationName := "Safety Data (OmniContact group)"
ThisBuild / organizationHomepage := Some(url("https://www.safety-data.com/"))

ThisBuild / scmInfo := Some(
  ScmInfo(
    url("https://gitlab.com/safety-data/cloakroom"),
    "scm:git@gitlab.com:safety-data/cloakroom.git"
  )
)
ThisBuild / developers := List(
  Developer(
    id    = "assaf.urieli@safety-data.com",
    name  = "Assaf Urieli",
    email = "assaf.urieli@safety-data.com",
    url   = url("https://www.safety-data.com/")
  )
)

ThisBuild / description := "A small library to easily parse access tokens from Keycloak/Openid and interact with the Keycloak API."
ThisBuild / licenses := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt"))
ThisBuild / homepage := Some(url("https://gitlab.com/safety-data/cloakroom"))

// Remove all additional repository other than Maven Central from POM
ThisBuild / pomIncludeRepository := { _ => false }
ThisBuild / publishTo := {
  val nexus = "https://s01.oss.sonatype.org/"
  if (version.value.trim.endsWith("SNAPSHOT"))
    Some("snapshots" at nexus + "content/repositories/snapshots")
  else
    Some("releases" at nexus + "service/local/staging/deploy/maven2")
}
ThisBuild / publishMavenStyle := true

Global / concurrentRestrictions  += Tags.limit(Tags.Test, 1)

inThisBuild(
  List(
    scalaVersion := scala213,
  )
)

lazy val commonSettings = Seq(
  credentials += Credentials(Path.userHome / ".sbt" / "sonatype_credentials")
)

Test / testOptions := Seq(Tests.Argument(TestFrameworks.JUnit, "-a", "-q"))

lazy val javaCommonSettings = Seq(
  // hack to make publish work with crossScalaVersions
  crossScalaVersions := List(scala213, scala3_2),
  crossPaths := false,
  autoScalaLibrary := false,
  publishConfiguration := publishConfiguration.value.withOverwrite(true),
  publishLocalConfiguration := publishLocalConfiguration.value.withOverwrite(true),
)

lazy val core = project.in(file("core"))
  .settings(commonSettings: _*)
  .settings(javaCommonSettings: _*)
  .settings(
    name := "cloakroom",
    projectDependencies ++= Seq(
      "junit" % "junit" % junitVersion % Test,
      "com.github.sbt" % "junit-interface" % junitInterfaceVersion % Test,
      "org.keycloak" % "keycloak-adapter-core" % keycloakVersion,
      "org.keycloak" % "keycloak-common" % keycloakVersion,
      "org.keycloak" % "keycloak-admin-client" % keycloakVersion,
      "org.jboss.logging" % "jboss-logging" % "3.4.3.Final",
      "com.typesafe" % "config" % "1.4.1",
      "com.fasterxml.jackson.core" % "jackson-core" % "2.13.1",
      "com.auth0" % "java-jwt" % "3.18.1" % Test,
      "org.apache.httpcomponents" % "httpclient" % "4.5.13", //needed by keycloak
      "ch.qos.logback" % "logback-classic" % "1.2.10",
      "org.slf4j" % "slf4j-api" % "1.7.33",
      "com.github.ben-manes.caffeine" % "caffeine" % "3.0.5",
    ),
  )

lazy val testUtil = project
  .in(file("test-util"))
  .settings(commonSettings: _*)
  .settings(javaCommonSettings: _*)
  .settings(
    name := "cloakroom-test-util",
    projectDependencies ++= Seq(
      "junit" % "junit" % junitVersion % Test,
    )
  )
  .dependsOn(core)

lazy val scala = project.in(file("scala"))
  .settings(commonSettings: _*)
  .settings(
    crossScalaVersions := Seq(scala213, scala3_2),
    name := "cloakroom-scala",
    projectDependencies ++= Seq(
      "org.scala-lang.modules" %% "scala-java8-compat" % "1.0.0",
      "org.scalatest" %% "scalatest" % scalaTestVersion % "test",
      "com.beachape" %% "enumeratum" % "1.7.2"
    )
  )
  .dependsOn(core % "compile -> compile;test -> test")

lazy val scalaTestUtil = project
  .settings(commonSettings: _*)
  .settings(
    name := "cloakroom-test-util-scala",
    projectDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % scalaTestVersion % "test",
    ),
    crossScalaVersions := Seq(scala213, scala3_2),
  )
  .in(file("scala-test-util"))
  .dependsOn(testUtil, core, scala)

lazy val keycloak = project.in(file("."))
  .aggregate(core, scala, testUtil, scalaTestUtil)
  .settings(
    name := "cloakroom-root",
    crossScalaVersions := Nil,
    publish / skip := false,
  )