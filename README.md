# Cloakroom

The aim of this simple library is to provide an easy way to validate a OpenID token issued by Keycloak.

## TokenVerifier

In order to verify a Token, you need to get a TokenVerifier implementation.
The simple way to get one is to use:
```java
TokenVerifier.fromTypeSafeConf("my_realm_name");
```

That will create a token verifier based on TypeSafe configuration at path `cloakroom.keycloak`.
The configuration should be the HOCON equivalent of an official Keycloak configuration, look at [AdapterConfig.java](https://www.keycloak.org/docs-api/4.8/javadocs/org/keycloak/representations/adapters/config/AdapterConfig.html)
to see all field available.
Here is an example of a correct `src/resources/application.conf` for using cloakroom:
```hocon
cloakroom {
  realm1 {
    realm: "realm1",
    auth-server-url: "http://localhost:8080/",
    ssl-required: "external",
    resource: "cloakroom", //client name under your realm
    public-client: true,
    use-resource-role-mappings: true
  }

  realm2 {
    realm: "realm2",
    auth-server-url: "http://localhost:8080/",
    ssl-required: "external",
    resource: "cloakroom", //client name under your realm
    public-client: true,
    use-resource-role-mappings: true
  }
}
```
This example has two realms but you can have a single realm as well.

But if you want to build your keycloak configuration/deployment yourself, you can do something like that:
```java
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.representations.adapters.config.AdapterConfig;

AdapterConfig adapterConfig = new AdapterConfig();
adapterConfig.setRealm("safetydata");
adapterConfig.setResource("cloakroom");
adapterConfig.setAuthServerUrl("http://localhost:8080/auth/");
adapterConfig.setSslRequired("external");
adapterConfig.setPublicClient(true);
adapterConfig.setUseResourceRoleMappings(true);

TokenVerifier.fromConfig(CloakroomConfig.fromKeycloakDeployment(KeycloakDeploymentBuilder.build(adapterConfig)));
```

Once you get your token verifier instance, you can check if a token is valid:
```java
TokenVerifer tokenVerifer = ...; // See above for constructing a token verifier
VerificationResult result = tokenVerifer.verify(token).get();
if (result.isValid()) {
  result.username().get(); // An Optional containing the username
  result.email().get(); // An Optional containing the email
  result.roles().get(); // An Optional containing roles specific to the client
  result.token().get(); // An Optional containing the full parsed token
  result.error(); // An empty optional in this case
} else {
  result.error().get(); // An Optional containing an instance of VerificationError
}
```

## HttpGet

On first token verification, the `TokenVerifier` must fetch realm public keys of Keycloak deployment.
This is done doing a GET http request using by default `java.net.http.HttpClient`.

If you want to customize, how this request is done, you can provide your own instance of `com.safetydata.cloakroom.HttpGet`
when constructing your token verifier.
For example:
```java
Token.fromTypeSafeConf(myHttpGetClient);
```

## Contributors

Cloakroom was initially developed for Safety Data by Luc Duzan.

More recent versions have included significant rewrites by Assaf Urieli.
