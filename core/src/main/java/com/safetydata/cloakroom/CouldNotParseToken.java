package com.safetydata.cloakroom;

import java.util.Optional;

public class CouldNotParseToken implements VerificationError {
    private final Optional<Throwable> underlayingError;

    public CouldNotParseToken(Throwable underlayingError) {
        this.underlayingError = Optional.ofNullable(underlayingError);
    }

    @Override
    public String reason() {
        return "Could not parse token";
    }

    @Override
    public Optional<Throwable> underlyingError() {
        return underlayingError;
    }
}
