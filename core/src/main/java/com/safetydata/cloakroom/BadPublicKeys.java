package com.safetydata.cloakroom;

import java.util.Optional;

public class BadPublicKeys implements VerificationError {
    @Override
    public String reason() {
        return "Token public keys is not the public key of wanted client";
    }

    @Override
    public Optional<Throwable> underlyingError() {
        return Optional.empty();
    }
}
