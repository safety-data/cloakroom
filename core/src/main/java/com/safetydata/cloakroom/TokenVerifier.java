package com.safetydata.cloakroom;

import com.safetydata.cloakroom.publickeys.HttpGet;
import com.safetydata.cloakroom.publickeys.KeycloakKeys;
import org.keycloak.RSATokenVerifier;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.common.VerificationException;
import org.keycloak.jose.jws.JWSHeader;
import org.keycloak.representations.AccessToken;

import java.security.PublicKey;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

public class TokenVerifier {
    public static TokenVerifier fromConfig(CloakroomConfig config) {
        KeycloakDeployment deployment = config.keycloakDeployment();
        KeycloakKeys keycloakKeys = KeycloakKeys.fromDeployment(deployment);

        return new TokenVerifier(keycloakKeys, deployment);
    }

    public static TokenVerifier fromConfig(CloakroomConfig config, HttpGet httpGet) {
        KeycloakDeployment deployment = config.keycloakDeployment();
        KeycloakKeys keycloakKeys = KeycloakKeys.fromDeployment(deployment, httpGet);

        return new TokenVerifier(keycloakKeys, deployment);
    }

    public static TokenVerifier fromTypeSafeConf(String realm) {
        return fromConfig(CloakroomConfig.fromTypeSafeConf(realm));
    }

    public static TokenVerifier fromTypeSafeConf(HttpGet httpGet, String realm) {
        return fromConfig(CloakroomConfig.fromTypeSafeConf(realm), httpGet);
    }

    private final KeycloakKeys keycloakKeys;
    private final KeycloakDeployment deployment;

    TokenVerifier(KeycloakKeys keycloakKeys, KeycloakDeployment deployment) {
        this.keycloakKeys = keycloakKeys;
        this.deployment = deployment;
    }

    public CompletableFuture<VerificationResult> verify(String token) {
        RSATokenVerifier tokenVerifier;
        try {
            tokenVerifier = createTokenVerifier(token);
        } catch (IllegalArgumentException e) {
            return CompletableFuture.completedFuture(VerificationResult.ofError(new CouldNotParseToken(e)));
        }

        return keycloakKeys.keys().
                thenApply(keys -> checkTokenUsingKeys(tokenVerifier, token, keys));
    }

    private RSATokenVerifier createTokenVerifier(String token) {
        return RSATokenVerifier
                .create(token)
                .realmUrl(this.deployment.getRealmInfoUrl());
    }

    private VerificationResult checkTokenUsingKeys(RSATokenVerifier tokenVerifier, String token, Map<String, PublicKey> keys) {
        JWSHeader header;
        try {
            header = tokenVerifier.getHeader();
        } catch (VerificationException e) {
            return VerificationResult.ofError(new CouldNotParseToken(e));
        }
        String keyId = header.getKeyId();
        if (!keys.containsKey(keyId)) {
            return VerificationResult.ofError(new BadPublicKeys());
        }

        AccessToken accessToken;
        try {
            accessToken = tokenVerifier.
                    publicKey(keys.get(keyId)).
                    verify().
                    getToken();
        } catch (VerificationException e) {
            return VerificationResult.ofError(new InvalidToken(e));
        }

        return VerificationResult.ofToken(deployment.getResourceName(), accessToken);
    }
}
