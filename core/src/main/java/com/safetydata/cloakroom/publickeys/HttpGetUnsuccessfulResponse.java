package com.safetydata.cloakroom.publickeys;

public class HttpGetUnsuccessfulResponse extends RuntimeException {
    static public HttpGetUnsuccessfulResponse of(int statusCode, String reason) {
        return new HttpGetUnsuccessfulResponse(String.format("Unsuccessful response: %s, body: \"%s\"", statusCode, reason));
    }

    private HttpGetUnsuccessfulResponse(String reason) {
        super(reason);
    }
}
