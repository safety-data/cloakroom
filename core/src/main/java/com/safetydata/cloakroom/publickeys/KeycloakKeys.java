package com.safetydata.cloakroom.publickeys;

import org.keycloak.adapters.KeycloakDeployment;

import java.security.PublicKey;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * Represent public key collection of a keycloak realm
 */
public interface KeycloakKeys {
    static KeycloakKeys fromDeployment(KeycloakDeployment deployment) {
        return new KeycloakKeysFromDeployment(deployment);
    }

    static KeycloakKeys fromDeployment(KeycloakDeployment keycloakDeployment, HttpGet customHttpClient) {
        return new KeycloakKeysFromDeployment(keycloakDeployment, customHttpClient);
    }

    CompletableFuture<Map<String, PublicKey>> keys();
}
