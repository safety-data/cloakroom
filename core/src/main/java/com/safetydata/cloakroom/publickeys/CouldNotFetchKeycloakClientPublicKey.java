package com.safetydata.cloakroom.publickeys;

public class CouldNotFetchKeycloakClientPublicKey extends RuntimeException {

    public CouldNotFetchKeycloakClientPublicKey(String reason, Throwable underlyingError) {
        super(reason, underlyingError);
    }
}
