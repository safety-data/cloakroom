package com.safetydata.cloakroom.publickeys;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.security.PublicKey;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@JsonIgnoreProperties(ignoreUnknown = true)
class Keys {
    public List<KeyData> keys;

    public Map<String, PublicKey> publicKeysById() {
        try {
            return keys.
                    stream().
                    collect(Collectors.toMap(
                            key -> key.kid,
                            key -> key.publicKeys()
                    ));
        } catch (IllegalStateException e) {
            throw new CouldNotFetchKeycloakClientPublicKey("Keycloak return two keys with same ids", e);
        }
    }
}
