package com.safetydata.cloakroom.publickeys;

import java.awt.event.TextEvent;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.concurrent.CompletableFuture;

class HttpGetImpl implements HttpGet {
    @Override
    public CompletableFuture<InputStream> get(String url) {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .build();

        return client.sendAsync(request, HttpResponse.BodyHandlers.ofInputStream())
                .thenApply(response -> {
                    if (response.statusCode() < 200 || response.statusCode() > 300) {
                        throw HttpGetUnsuccessfulResponse.of(response.statusCode(),
                                convertStreamToString(response.body()));
                    }
                    return response.body();
                });
    }

    private static String convertStreamToString(InputStream inputStream) {
        java.util.Scanner scanner = new java.util.Scanner(inputStream).useDelimiter("\\A");
        return scanner.hasNext() ? scanner.next() : "";
    }
}
