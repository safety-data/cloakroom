package com.safetydata.cloakroom.publickeys;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.keycloak.adapters.KeycloakDeployment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.security.PublicKey;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;


class KeycloakKeysFromDeployment implements KeycloakKeys {
    private static final Logger LOG = LoggerFactory.getLogger(KeycloakKeysFromDeployment.class);
    
    private final KeycloakDeployment deployment;
    private final HttpGet httpGet;
    private Optional<CompletableFuture<Map<String, PublicKey>>> cache;


    public KeycloakKeysFromDeployment(KeycloakDeployment deployment) {
        this(deployment, new HttpGetImpl());
    }

    public KeycloakKeysFromDeployment(KeycloakDeployment deployment, HttpGet httpGet) {
        this.deployment = deployment;
        this.httpGet = httpGet;
        this.cache = Optional.empty();
    }

    @Override
    public CompletableFuture<Map<String, PublicKey>> keys() {
        if (cache.isEmpty()) {
            cache = Optional.of(fetchKeys());
        }

        return cache.get();
    }

    private CompletableFuture<Map<String, PublicKey>> fetchKeys() {
        final String jwksURL = deployment.getJwksUrl();
        if (LOG.isDebugEnabled())
            LOG.debug(String.format("Fetching public keys from %s", jwksURL));
        return httpGet.get(jwksURL)
                .exceptionally(t -> {
                    throw new CouldNotFetchKeycloakClientPublicKey("Could not fetch public key", t);
                })
                .thenApply(this::parseKeys);
    }

    private Map<String, PublicKey> parseKeys(InputStream in) {
        ObjectMapper objectMapper = new ObjectMapper();
        Keys keys;
        try {
            keys = objectMapper.readValue(in, Keys.class);
        } catch (IOException e) {
            throw new CouldNotFetchKeycloakClientPublicKey("Could not read keys from keycloak", e);
        }

        return keys.publicKeysById();
    }
}
