package com.safetydata.cloakroom.publickeys;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.keycloak.jose.jws.AlgorithmType;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.Base64;

@JsonIgnoreProperties(ignoreUnknown = true)
class KeyData {
    public String kid;
    public String n;
    public String e;

    public PublicKey publicKeys() {
        KeyFactory factory;
        try {
            factory = KeyFactory.getInstance(AlgorithmType.RSA.toString());
        } catch (NoSuchAlgorithmException e) {
            // there is no way this going to happen...
            throw new RuntimeException("Could not find RSA algorithm", e);
        }
        BigInteger modulus = decodeInteger(n);
        BigInteger publicExponent = decodeInteger(e);
        try {
            return factory.generatePublic(new RSAPublicKeySpec(modulus, publicExponent));
        } catch (InvalidKeySpecException e) {
            throw new CouldNotFetchKeycloakClientPublicKey("Invalid RSA key", e);
        }
    }

    private BigInteger decodeInteger(String base64) {
        return new BigInteger(1, Base64.getUrlDecoder().decode(base64));
    }
}
