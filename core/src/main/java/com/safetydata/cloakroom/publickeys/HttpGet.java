package com.safetydata.cloakroom.publickeys;

import java.io.InputStream;
import java.util.concurrent.CompletableFuture;

public interface HttpGet {
    CompletableFuture<InputStream> get(String url);
}
