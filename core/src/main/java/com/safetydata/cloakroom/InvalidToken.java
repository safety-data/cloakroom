package com.safetydata.cloakroom;

import java.util.Optional;

public class InvalidToken implements VerificationError {
    private final Optional<Throwable> underlyingError;

    public InvalidToken(Throwable underlyingError) {
        this.underlyingError = Optional.ofNullable(underlyingError);
    }

    @Override
    public String reason() {
        return "Token is invalid";
    }

    @Override
    public Optional<Throwable> underlyingError() {
        return underlyingError;
    }
}
