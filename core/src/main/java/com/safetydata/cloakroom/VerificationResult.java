package com.safetydata.cloakroom;

import org.keycloak.representations.AccessToken;

import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;

public class VerificationResult {
    private final Optional<AccessToken> accessToken;
    private final Optional<VerificationError> error;
    private final String clientId;

    private VerificationResult(String clientId, Optional<AccessToken> accessToken, Optional<VerificationError> error) {
        assert (accessToken.isEmpty() != error.isEmpty());
        this.accessToken = accessToken;
        this.error = error;
        this.clientId = clientId;
    }

    static VerificationResult ofToken(String clientId, AccessToken token) {
        return new VerificationResult(clientId, Optional.of(token), Optional.empty());
    }

    static VerificationResult ofError(VerificationError error) {
        return new VerificationResult(null, Optional.empty(), Optional.of(error));
    }

    public boolean isValid() {
        return accessToken.isPresent();
    }

    /**
     * None if the token is valid
     * Error explaining why the token is not valid
     */
    public Optional<VerificationError> error() {
        return error;
    }

    /**
     * None if token is not valid
     */
    public Optional<AccessToken> token() {
        return this.accessToken;
    }

    /**
     * None if token is not valid
     * Role specific to keycloak client/resource
     */
    public Optional<Set<String>> roles() {
        return this.accessToken.map(accessToken ->
            Optional
                    .ofNullable(accessToken.getResourceAccess(this.clientId))
                    .map(AccessToken.Access::getRoles)
                    .orElse(Set.of())
        );
    }

    public Optional<String> email() {
        return this.accessToken.flatMap(accessToken -> {
            try {
                return Optional.ofNullable(accessToken.getEmail());
            } catch (NoSuchElementException e) {
                return Optional.empty();
            }
        });
    }

    public Optional<String> username() {
        return this.accessToken.flatMap(accessToken -> {
            try {
                return Optional.ofNullable(accessToken.getPreferredUsername());
            } catch (NoSuchElementException e) {
                return Optional.empty();
            }
        });
    }
}
