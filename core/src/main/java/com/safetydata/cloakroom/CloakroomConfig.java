package com.safetydata.cloakroom;

import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigRenderOptions;
import org.keycloak.Config;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;

import java.io.ByteArrayInputStream;

public class CloakroomConfig {
    private final KeycloakDeployment keycloakDeployment;

    public CloakroomConfig(KeycloakDeployment keycloakDeployment) {
        this.keycloakDeployment = keycloakDeployment;
    }

    /**
     * Use config at path "cloakroom.keycloak"
     */
    public static CloakroomConfig fromTypeSafeConf(String realm) {
        com.typesafe.config.Config config = ConfigFactory.load();
        config.checkValid(ConfigFactory.defaultReference(), String.format("cloakroom.%s", realm));
        
        String jsonConfig = config
                .getConfig(String.format("cloakroom.%s.keycloak", realm))
                .root()
                .render(ConfigRenderOptions.concise());

        return new CloakroomConfig(
                KeycloakDeploymentBuilder.build(
                        new ByteArrayInputStream(jsonConfig.getBytes())));
    }

    public static CloakroomConfig fromKeycloakDeployment(KeycloakDeployment deployment) {
        return new CloakroomConfig(deployment);
    }

    public KeycloakDeployment keycloakDeployment() {
        return keycloakDeployment;
    }
}
