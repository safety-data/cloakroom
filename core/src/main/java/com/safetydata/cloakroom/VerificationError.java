package com.safetydata.cloakroom;

import java.util.Optional;

public interface VerificationError {
    String reason();
    Optional<Throwable> underlyingError();
}
