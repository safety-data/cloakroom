package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.GroupRepresentation;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

class GroupWithIdImpl implements GroupWithId {
    protected final GroupId id;
    protected final Group group;

    GroupWithIdImpl(Group group, GroupId id) {
        this.id = id;
        this.group = group;
    }

    GroupWithIdImpl(GroupRepresentation groupRepr) {
        this.group = GroupImpl.fromKeycloakRepr(groupRepr);
        this.id = new GroupId(groupRepr.getId());
    }

    @Override
    public Group dropId() { return group; }

    @Override
    public GroupId id() { return id; }

    @Override
    public GroupWithId withAttributes(Map<String, List<String>> attributes) {
        return new GroupWithIdImpl(this.group.withAttributes(attributes), this.id);
    }

    @Override
    public GroupWithId withRoles(Set<Role> roles) {
        return new GroupWithIdImpl(this.group.withRoles(roles), this.id);
    }

    @Override
    public String name() {
        return this.group.name();
    }

    @Override
    public Map<String, List<String>> attributes() {
        return this.group.attributes();
    }

    @Override
    public Set<Role> roles() {
        return this.group.roles();
    }

    @Override
    public int compareTo(Group group) {
        return this.group.compareTo(group);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupWithIdImpl that = (GroupWithIdImpl) o;
        return id.equals(that.id) &&
            group.equals(that.group);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, group);
    }

    @Override
    public String toString() {
        return "GroupWithIdImpl{" +
            "id=" + id +
            ", group=" + group +
            '}';
    }
}
