package com.safetydata.cloakroom.admin.exception;

import com.safetydata.cloakroom.admin.UserId;

public class UserIdNotFoundException extends RuntimeException {
    public final UserId id;
    public UserIdNotFoundException(UserId id) {
        super(String.format("No user found with id: %s", id.id()));
        this.id = id;
    }
}
