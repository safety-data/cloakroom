package com.safetydata.cloakroom.admin.exception;

import jakarta.ws.rs.core.Response;

public class KeycloakApiException extends RuntimeException {

    public KeycloakApiException(String wantedAction, Response response) {
        super(
                String.format("Could not %s, status code: %d, explanation %s",
                        wantedAction,
                        response.getStatus(),
                        response.readEntity(String.class)));
    }

    public static void checkResponse(Response response, String wantedAction) throws KeycloakApiException {
        if (response.getStatusInfo().getFamily() != Response.Status.Family.SUCCESSFUL) {
            throw new KeycloakApiException(wantedAction, response);
        }
    }
}
