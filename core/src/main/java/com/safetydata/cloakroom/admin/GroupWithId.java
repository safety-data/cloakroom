package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.GroupRepresentation;

import java.util.Set;

public interface GroupWithId extends Group {
    static GroupWithId fromKeycloakRepr(GroupRepresentation groupRepr) {
        return GroupImpl.fromKeycloakRepr(groupRepr).withId(new GroupId(groupRepr.getId()));
    }
    
    Group dropId();

    GroupId id();

    GroupWithId withRoles(Set<Role> roles);
}
