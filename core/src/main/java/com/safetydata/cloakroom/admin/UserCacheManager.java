package com.safetydata.cloakroom.admin;

import java.util.Collection;
import java.util.Map;

interface UserCacheManager {
    UserWithId getUserAndUpdateCache(UserId userId);
    Map<UserId, UserWithId> getUsersFromCache(Collection<UserId> userIds);
    void removeFromCache(UserId userId);
    void refreshCache();
}
