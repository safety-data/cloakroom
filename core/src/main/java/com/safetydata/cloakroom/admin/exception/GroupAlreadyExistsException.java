package com.safetydata.cloakroom.admin.exception;

public class GroupAlreadyExistsException extends RuntimeException {
    public final String name;
    public GroupAlreadyExistsException(String name) {
        super(String.format("Group with following name: %s already exists", name));
        this.name = name;
    }
}
