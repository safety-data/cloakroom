package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.UserRepresentation;

import java.time.ZonedDateTime;
import java.util.*;

class UserWithIdImpl implements UserWithId {
    private final UserId id;
    private final UserImpl user;

    UserWithIdImpl(UserImpl user, UserId userId) {
        this.user = user;
        this.id = userId;
    }

    UserWithIdImpl(UserRepresentation userRepr) {
        this.user = UserImpl.fromKeycloakRepr(userRepr).build();
        this.id = new UserId(userRepr.getId());
    }

    static UserWithIdImpl fromKeycloakRepr(UserRepresentation keycloakRepr) {
        return new UserWithIdImpl(keycloakRepr);
    }

    @Override
    public UserId id() {
        return this.id;
    }

    @Override
    public String username() {
        return user.username();
    }

    @Override
    public Optional<String> email() {
        return user.email();
    }

    @Override
    public Optional<String> firstName() {
        return user.firstName();
    }

    @Override
    public Optional<String> lastName() {
        return user.lastName();
    }

    @Override
    public Set<UserAction> requiredActions() {
        return this.user.requiredActions();
    }

    @Override
    public Optional<ZonedDateTime> createDate() {
        return this.user.createDate();
    }

    @Override
    public Map<String, List<String>> attributes() {
        return this.user.attributes();
    }

    @Override
    public Set<Role> roles() {
        return this.user.roles();
    }

    @Override
    public Set<Group> groups() {
        return this.user.groups();
    }

    @Override
    public UserWithId withRoles(Set<Role> roles) {
        return new UserWithIdImpl(user.withRoles(roles), id);
    }

    @Override
    public UserWithId withGroups(Set<Group> groups) {
        return new UserWithIdImpl(user.withGroups(groups), id);
    }

    @Override
    public UserWithId withAttributes(Map<String, List<String>> attributes) {
        return new UserWithIdImpl(user.withAttributes(attributes), id);
    }

    UserWithId withId(UserId id) {
        return user.withId(id);
    }

    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }

    @Override
    public User dropId() {
        return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserWithIdImpl that = (UserWithIdImpl) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserWithId{");
        sb.append("id=").append(id);
        sb.append(", user=").append(user);
        sb.append('}');
        return sb.toString();
    }
}
