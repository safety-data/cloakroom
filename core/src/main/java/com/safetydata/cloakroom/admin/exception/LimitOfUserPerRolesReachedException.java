package com.safetydata.cloakroom.admin.exception;

import com.safetydata.cloakroom.admin.Role;
import com.safetydata.cloakroom.admin.User;
;
import java.util.Set;
import java.util.stream.Collectors;

public class LimitOfUserPerRolesReachedException extends RuntimeException {
    public final User user;
    public final Set<Role> rolesExceedsMax;
    public LimitOfUserPerRolesReachedException(User user, Set<Role> rolesExceedsMax) {
        super(String.format("Couldn't create/modify user: %s because the maximum number of users for role(s) %s has been reached",
                user.username(),
                rolesExceedsMax.stream().map(Role::name).collect(Collectors.joining(", "))));
        this.user= user;
        this.rolesExceedsMax = rolesExceedsMax;
    }

}
