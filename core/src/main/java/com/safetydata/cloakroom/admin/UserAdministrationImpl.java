package com.safetydata.cloakroom.admin;

import com.safetydata.cloakroom.admin.exception.*;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.ClientErrorException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.Response;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class UserAdministrationImpl extends KeycloakAdministrationHelper implements UserAdministration {

    private final CloakroomConfigWithAdmin cloakroomConfig;
    private final UserCacheManager userCacheManager;
    private final GroupCacheManager groupCacheManager;
    private final RealmResource realm;
    private final UsersResource users;
    private final GroupsResource groupResource;
    private final Config fileConfig;
    private final Map<Role, Integer> roleLimitations;
    private final Set<Pattern> invisibleUserPatterns;

    private static final Logger LOG = LoggerFactory.getLogger(UserAdministrationImpl.class);


    UserAdministrationImpl(CloakroomConfigWithAdmin cloakroomConfig, Map<String, Integer> roleLimitations, UserCacheManager userCacheManager, GroupCacheManager groupCacheManager) {
        super(cloakroomConfig);
        this.cloakroomConfig = cloakroomConfig;
        this.userCacheManager = userCacheManager;
        this.groupCacheManager = groupCacheManager;
        this.realm = cloakroomConfig.adminKeycloak.realm(this.cloakroomConfig.keycloakDeployment().getRealm());
        this.users = realm.users();
        this.groupResource = realm.groups();
        this.fileConfig = ConfigFactory.load();
        this.roleLimitations = convertMapRestrictions(roleLimitations);
        String invisibleUsersConfPath = String.format("cloakroom.%s.invisible-users", this.cloakroomConfig.keycloakDeployment().getRealm());
        List<String> invisibleUsers = this.fileConfig.getStringList(invisibleUsersConfPath);
        this.invisibleUserPatterns = invisibleUsers.stream().map(Pattern::compile).collect(Collectors.toSet());
    }

    @Override
    public UserWithId addUser(User user) {
        return this.addUser(user, true);
    }

    @Override
    public UserWithId addUser(User user, boolean sendEmail) {
        Set<Role> rolesExceedsMaxAuthorizedBeforeAction = getRoleOverLimitation(user.roles(), RoleLimitationParameter.AlsoIncludeRoleExactlyAtLimitation);

        if(rolesExceedsMaxAuthorizedBeforeAction.size() > 0) {
            throw new LimitOfUserPerRolesReachedException(user, rolesExceedsMaxAuthorizedBeforeAction);
        }

        UserWithId enrichedUserWithId = addUserWithoutSendingEmail(user);

        Set<Role> rolesExceedsMaxAuthorizedAfterAction = getRoleOverLimitation(user.roles(), RoleLimitationParameter.IgnoreRoleExactlyAtLimitation);

        if(rolesExceedsMaxAuthorizedAfterAction.size() > 0) {
            deleteUser(enrichedUserWithId);
            throw new LimitOfUserPerRolesReachedException(user, rolesExceedsMaxAuthorizedBeforeAction);
        } else if (sendEmail) {
            sendEmailIfRequired(enrichedUserWithId);
        }

        return userCacheManager.getUserAndUpdateCache(enrichedUserWithId.id());
    }

    private UserWithId addUserWithoutSendingEmail(User user) {
        UserRepresentation userRepresentation = user.keycloakRepr();
        // ensure groups and roles exist before adding user
        List<GroupRepresentation> groupsToAdd = representationOfGroups(user.groups());
        List<RoleRepresentation> rolesToAdd = representationsOfRoles(user.roles());
        
        Response response = this.users.create(userRepresentation);

        if (response.getStatusInfo() == Response.Status.CONFLICT) {
            String message = response.readEntity(String.class);
            if (message.contains("same email")) {
                throw new EmailAlreadyUsedException(user.email().get());
            } else {
                throw new UserAlreadyExistsException(user.username());
            }
        }
        KeycloakApiException.checkResponse(response, "create user with username " + user.username());
        
        String id = response.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");
        UserId userId = new UserId(id);
        userRepresentation.setId(id);

        UserResource userResource = this.userResource(userId);
        groupsToAdd.forEach(groupToAdd -> userResource.joinGroup(groupToAdd.getId()));

        // refresh group cache to update group size
        groupsToAdd.forEach(groupToAdd -> groupCacheManager.getGroupAndUpdateCache(new GroupId(groupToAdd.getId())));
        
        this.getUserRoleResource(userResource).add(rolesToAdd);

        return UserWithIdImpl.fromKeycloakRepr(userRepresentation).withRoles(user.roles()).withGroups(user.groups());
    }

    private void sendEmailIfRequired(UserWithId user) {
        if(user.isEnabled() && !user.requiredActions().isEmpty() && user.email().isPresent()) {
            this.sendRequiredActionsEmail(user.id(), user.keycloakRepr().getRequiredActions());
        }
    }

    private void sendEmailIfRequired(UserWithId user, UserModifier modifier) {
        if (user.isEnabled() && modifier.concernRequiredAction() ||
                modifier.makeUserEnable() && !user.requiredActions().isEmpty()
        ) {
            this.sendRequiredActionsEmail(user.id(), UserAction.toApi(user.requiredActions()));
        }
    }

    private void sendRequiredActionsEmail(UserId userId, List<String> requiredActions) {
        try {
            String baseUrl = this.client.toRepresentation().getBaseUrl();
            this.userResource(userId).executeActionsEmail(this.clientName, baseUrl, this.linkLifespanSeconds, requiredActions);
        } catch (NotFoundException nfe) {
            throw new UserIdNotFoundException(userId);
        }
    }

    private Stream<UserRepresentation> filterInvisibleUsers(List<UserRepresentation> users) {
        return users.stream().filter(user -> invisibleUserPatterns.stream().noneMatch(pattern -> {
            Matcher matcher = pattern.matcher(user.getUsername());
            return matcher.matches();
        }));
    }

    public PaginatedUsers listUsers(int firstResult, int maxResults) {
        return this.listUsers(firstResult, maxResults, new User.SortByUsername(true), Optional.empty());
    }

    public PaginatedUsers listUsers(int firstResult, int maxResults, User.Sort sort) {
        return this.listUsers(firstResult, maxResults, sort, Optional.empty());
    }

    public PaginatedUsers listUsers(int firstResult, int maxResults, User.Sort sort, Optional<User.Filter> filter) {
        return this.listUsers(firstResult, maxResults, sort, filter, Optional.empty());
    }

    public PaginatedUsers listUsers(int firstResult, int maxResults, User.Sort sort, Optional<User.Filter> filter, Optional<Role> requiredRole) {
        List<UserId> userIds = this.users.list(0, Integer.MAX_VALUE).stream()
            .map(u -> new UserId(u.getId()))
            .collect(Collectors.toList());

        return this.getPaginatedUsers(userCacheManager, userIds, firstResult, maxResults, sort, filter, requiredRole);
    }

    @Override
    public void deleteUser(UserId id) {
        this.users.delete(id.id());
        this.userCacheManager.removeFromCache(id);
    }

    //because Java8 sucks when it comes to immutable sets, we have to implement this by hand.
    private static<T> Set<T> mergeSets(Set<T> leftSet, Set<T> rightSet) {
        return Stream.of(leftSet, rightSet)
                .flatMap(x -> x.stream())
                .collect(Collectors.toSet());
    }

    private static<T> Set<T> substractSets(Set<T> leftSet, Set<T> rightSet) {
        return leftSet.stream()
                .filter(e -> !rightSet.contains(e))
                .collect(Collectors.toSet());
    }

    @Override
    public UserWithId modifyUser(UserId id, UserModifier modifier) {
        return this.modifyUser(id, modifier, true);
    }

    @Override
    public UserWithId modifyUser(UserId id, UserModifier modifier, boolean sendEmail) {
        Set<Role> userRoles = userRoles(id);

        Set<Role> addedRoles = modifier.roleModifier().add().orElse(Set.of());
        Set<Role> setRoles = modifier.roleModifier().set().orElse(Set.of());
        Set<Role> effectivelyAddedRoles = substractSets(mergeSets(addedRoles, setRoles), userRoles);

        if (LOG.isDebugEnabled()) {
            LOG.debug("userRoles: " + userRoles);
            LOG.debug("addedRoles: " + addedRoles);
            LOG.debug("setRoles: " + setRoles);
            LOG.debug("effectivelyAddedRoles: " + effectivelyAddedRoles);
        }
        Set<Role> addingOrSettingsRolesNotAllowed = getRoleOverLimitation(effectivelyAddedRoles, RoleLimitationParameter.AlsoIncludeRoleExactlyAtLimitation);

        if(addingOrSettingsRolesNotAllowed.size() > 0) {
            throw new LimitOfUserPerRolesReachedException(getUserById(id), roleLimitations.keySet());
        }

        modifyUserWithoutSendingEmail(id, modifier);

        UserWithId user = userCacheManager.getUserAndUpdateCache(id);

        Set<Role> rolesAddedInExcess = getRoleOverLimitation(user.roles(), RoleLimitationParameter.IgnoreRoleExactlyAtLimitation); //can happen if // modification

        if (sendEmail) {
            sendEmailIfRequired(user, modifier);
        }

        if(rolesAddedInExcess.size() > 0) {
            modifyUserWithoutSendingEmail(id, UserModifier.removeRoles(rolesAddedInExcess));
            userCacheManager.getUserAndUpdateCache(id);
            throw new PartiallyModifiedUserException(user, rolesAddedInExcess);
        }

        return user;
    }

    private void modifyUserWithoutSendingEmail(UserId id, UserModifier modifier) {
        UserRepresentation userRepresentation = modifier.toKeycloakRepr();
        UserResource userResource = this.userResource(id);
        
        // ensure groups and roles exist before modifying user
        Optional<List<GroupRepresentation>> groupsToSet =  modifier.groupModifier().set().map(gs -> representationOfGroups(gs));
        Optional<List<GroupRepresentation>> groupsToAdd = modifier.groupModifier().add().map(gs -> representationOfGroups(gs));
        Optional<List<GroupRepresentation>> groupsToRemove = modifier.groupModifier().remove().map(gs -> representationOfGroups(gs));

        Optional<List<RoleRepresentation>> rolesToSet =  modifier.roleModifier().set().map(rs -> representationsOfRoles(rs));
        Optional<List<RoleRepresentation>> rolesToAdd =  modifier.roleModifier().add().map(rs -> representationsOfRoles(rs));
        Optional<List<RoleRepresentation>> rolesToRemove =  modifier.roleModifier().remove().map(rs -> representationsOfRoles(rs));
        
        try {
            userResource.update(userRepresentation);
        } catch (NotFoundException nfe) {
            throw new UserIdNotFoundException(id);
        } catch (ClientErrorException clientErrorException) {
            if (clientErrorException.getResponse().getStatusInfo() == Response.Status.CONFLICT && modifier.email.isPresent()) {
                throw new EmailAlreadyUsedException(modifier.email.get());
            }
            throw clientErrorException;
        }

        if (groupsToSet.isPresent()) {
            List<String> newGroups = groupsToSet.get().stream().map(g -> g.getId()).collect(Collectors.toList());
            List<String> oldGroups = userResource.groups().stream().map(g -> g.getId()).collect(Collectors.toList());
            Set<String> toRemove = new HashSet<>(oldGroups);
            toRemove.removeAll(newGroups);
            Set<String> toAdd = new HashSet<>(newGroups);
            toAdd.removeAll(oldGroups);
            
            toRemove.forEach(groupId -> userResource.leaveGroup(groupId));
            toAdd.forEach(groupId -> userResource.joinGroup(groupId));

            toRemove.forEach(groupId -> groupCacheManager.getGroupAndUpdateCache(new GroupId(groupId)));
            toAdd.forEach(groupId -> groupCacheManager.getGroupAndUpdateCache(new GroupId(groupId)));
        }

        if (groupsToAdd.isPresent()) {
            groupsToAdd.get().forEach(groupToAdd -> userResource.joinGroup(groupToAdd.getId()));
            groupsToAdd.get().forEach(groupToAdd -> groupCacheManager.getGroupAndUpdateCache(new GroupId(groupToAdd.getId())));
        }
        
        if (groupsToRemove.isPresent()) {
            groupsToRemove.get().forEach(groupToRemove -> userResource.leaveGroup(groupToRemove.getId()));
            groupsToRemove.get().forEach(groupToRemove -> groupCacheManager.getGroupAndUpdateCache(new GroupId(groupToRemove.getId())));
        }
        
        RoleScopeResource userRoleResource = getUserRoleResource(userResource);
        if (rolesToAdd.isPresent()) {
            userRoleResource.add(rolesToAdd.get());
        }

        if (rolesToRemove.isPresent()) {
            userRoleResource.remove(rolesToRemove.get());
        }

        if (rolesToSet.isPresent()) {
            Set<Role> oldRoles = userRoles(userRoleResource);
            oldRoles.removeAll(modifier.roleModifier().set().get());
            userRoleResource.remove(representationsOfRoles(oldRoles));
            userRoleResource.add(rolesToSet.get());
        }
    }

    @Override
    public Optional<UserWithId> getUserByUsername(String username) {
        List<UserRepresentation> usersRepr = this.users.search(username).stream().filter(u -> username.equals(u.getUsername())).collect(Collectors.toList());;
        if (usersRepr.isEmpty()) {
            return Optional.empty();
        } else if (usersRepr.size() > 1) {
            throw new IllegalStateException("This should never happen, we got more than one user with the same username");
        }
        UserRepresentation userRepr = usersRepr.get(0);
        UserId id = new UserId(userRepr.getId());
        return Optional.of(this.getUserById(id));
    }

    @Override
    public UserWithId getUserById(UserId id) {
        return this.userCacheManager.getUserAndUpdateCache(id);
    }

    @Override
    public void refreshCache() {
        this.userCacheManager.refreshCache();
    }


    @Override
    public Set<Role> effectiveUserRoles(UserId id) {
        try {
            return getUserRoleResource(this.userResource(id)).
                listEffective().
                stream().
                map(Role::new).
                collect(Collectors.toSet());
        } catch (NotFoundException nfe) {
            throw new UserIdNotFoundException(id);
        }
    }


    @Override
    public int userCount() {
        List<UserRepresentation> users = this.users.list(0, Integer.MAX_VALUE);

        return (int) filterInvisibleUsers(users).count();
    }
    
    private enum RoleLimitationParameter {
        AlsoIncludeRoleExactlyAtLimitation(1), IgnoreRoleExactlyAtLimitation(0);

        private final int inc;

        private RoleLimitationParameter(int inc) {
            this.inc = inc;
        }

        int getInc() {
            return inc;
        }
    }

    private Set<Role> getRoleOverLimitation(Set<Role> amongRoles, RoleLimitationParameter roleLimitationParameter) {
        return amongRoles
                .stream()
                .filter(role -> {
                    if(roleLimitations.containsKey(role)) {
                        int nbUsersWithRole = getUsersWithRole(role.name()).size();
                        boolean exceed = (nbUsersWithRole + roleLimitationParameter.getInc()) > roleLimitations.get(role);
                        if(exceed) {
                            LOG.debug("Role : " + role.name() + "\n" +
                                    "nbUsersWithRole : " + nbUsersWithRole + "\n" +
                                    "roleLimitationParameter.getInc() : " + roleLimitationParameter.getInc() + "\n" +
                                    "roleLimitations.get(role) : " + roleLimitations.get(role));
                        }
                        return exceed;
                    }
                    return false; })
                .collect(Collectors.toSet());
    }

    private Map<Role, Integer> convertMapRestrictions(Map<String, Integer> rolesRestrictions) {
        return rolesRestrictions
                .entrySet()
                .stream()
                .map(entry -> Map.entry(getRoleByName(entry.getKey()).orElseThrow(() -> new RoleNotFoundException(entry.getKey(), cloakroomConfig)), entry.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }


}
