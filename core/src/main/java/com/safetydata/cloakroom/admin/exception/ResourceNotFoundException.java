package com.safetydata.cloakroom.admin.exception;

public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException(String message, Throwable error) {
        super(message, error);
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }
}
