package com.safetydata.cloakroom.admin.exception;

public class UserAlreadyExistsException extends RuntimeException {
    public final String username;
    public UserAlreadyExistsException(String username) {
        super(String.format("User with following username: %s already exist", username));
        this.username = username;
    }
}
