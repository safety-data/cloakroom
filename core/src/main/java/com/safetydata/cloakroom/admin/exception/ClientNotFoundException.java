package com.safetydata.cloakroom.admin.exception;

public class ClientNotFoundException extends ResourceNotFoundException {
    public ClientNotFoundException(String clientName) {
        super("Could not find client: " + clientName);
    }
}
