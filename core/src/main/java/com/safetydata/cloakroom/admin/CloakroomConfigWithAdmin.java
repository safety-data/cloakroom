package com.safetydata.cloakroom.admin;

import com.safetydata.cloakroom.CloakroomConfig;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.keycloak.OAuth2Constants;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;

public class CloakroomConfigWithAdmin extends CloakroomConfig {
    public final Keycloak adminKeycloak;

    public CloakroomConfigWithAdmin(KeycloakDeployment keycloakDeployment, Keycloak adminKeycloak) {
        super(keycloakDeployment);
        this.adminKeycloak = adminKeycloak;
    }

    public static CloakroomConfigWithAdmin fromTypeSafeConf(String realm) {
        Config adminConfig = ConfigFactory.load().getConfig(String.format("cloakroom.%s.admin", realm));
        KeycloakDeployment deployment = CloakroomConfig.fromTypeSafeConf(realm).keycloakDeployment();
        Keycloak adminKeycloak = KeycloakBuilder.builder()
                .serverUrl(deployment.getAuthServerBaseUrl())
                .realm(deployment.getRealm())
                .grantType(OAuth2Constants.PASSWORD)
                .clientId(deployment.getResourceName())
                .username(adminConfig.getString("username"))
                .password(adminConfig.getString("password"))
                .build();
        return new CloakroomConfigWithAdmin(deployment, adminKeycloak);
    }
}
