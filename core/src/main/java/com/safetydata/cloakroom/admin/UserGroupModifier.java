package com.safetydata.cloakroom.admin;

import java.util.Optional;
import java.util.Set;

public class UserGroupModifier {
    private final Optional<Set<Group>> add;
    private final Optional<Set<Group>> remove;
    private final Optional<Set<Group>> set;

    private UserGroupModifier(Optional<Set<Group>> add, Optional<Set<Group>> remove, Optional<Set<Group>> set) {
        this.add = add;
        this.remove = remove;
        this.set = set;
    }

    static UserGroupModifier empty() {
        return new UserGroupModifier(
                Optional.empty(),
                Optional.empty(),
                Optional.empty()
        );
    }

    public UserGroupModifier add(Set<Group> groupsToAdd) {
        if(this.set.isPresent()) {
            throw new IllegalArgumentException("Can't modify user by adding group and setting groups at the same time");
        }
        return new UserGroupModifier(
                Optional.of(groupsToAdd),
                this.remove,
                this.set
        );
    }

    public UserGroupModifier remove(Set<Group> groupsToRemove) {
        if(this.set.isPresent()) {
            throw new IllegalArgumentException("Can't modify user by removing group and setting groups at the same time");
        }
        return new UserGroupModifier(
                this.add,
                Optional.of(groupsToRemove),
                this.set
        );
    }

    public UserGroupModifier set(Set<Group> groupsToSet) {
        if(this.remove.isPresent()) {
            throw new IllegalArgumentException("Can't modify user by removing group and setting groups at the same time");
        }
        if(this.add.isPresent()) {
            throw new IllegalArgumentException("Can't modify user by adding group and setting groups at the same time");
        }
        return new UserGroupModifier(
                this.add,
                this.remove,
                Optional.of(groupsToSet)
        );
    }

    Optional<Set<Group>> add() {
        return add;
    }

    Optional<Set<Group>> remove() {
        return remove;
    }

    Optional<Set<Group>> set() {
        return set;
    }
}
