package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.GroupRepresentation;

import java.util.*;

class GroupImpl implements Group {
    private final String name;
    private final Map<String, List<String>> attributes;
    private final Set<Role> roles;

    GroupImpl(String name, Map<String, List<String>> attributes, Set<Role> roles) {
        this.name = name;
        this.attributes = attributes;
        this.roles = roles;
    }

    GroupImpl(GroupRepresentation groupRepr) {
        this.name = groupRepr.getName();
        this.attributes = Optional.ofNullable(groupRepr.getAttributes()).orElse(Map.of());
        this.roles = Set.of();
    }

    static GroupImpl fromKeycloakRepr(GroupRepresentation keycloakRepr) {
        return new GroupImpl(keycloakRepr);
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Map<String, List<String>> attributes() {
        return attributes;
    }

    @Override
    public Set<Role> roles() {
        return roles;
    }

    @Override
    public Group withAttributes(Map<String, List<String>> attributes) {
        return new GroupImpl(this.name, attributes, this.roles);
    }

    @Override
    public Group withRoles(Set<Role> roles) {
        return new GroupImpl(this.name, this.attributes, roles);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GroupImpl group = (GroupImpl) o;
        return Objects.equals(name, group.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Group{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }
    
    public int compareTo(Group group) {
        return this.name.compareTo(group.name());
    }

    GroupWithId withId(GroupId id) {
        return new GroupWithIdImpl(this, id);
    }
}
