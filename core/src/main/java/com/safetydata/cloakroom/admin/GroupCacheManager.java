package com.safetydata.cloakroom.admin;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

interface GroupCacheManager {
    GroupWithIdAndSize getGroupAndUpdateCache(GroupId groupId);
    Map<GroupId, GroupWithIdAndSize> getGroupsFromCache(Collection<GroupId> groupIds);
    void removeFromCache(GroupId groupId);
    boolean groupNameAlreadyExists(String groupName, Optional<GroupId> idFromModify);
    void refreshCache();
}
