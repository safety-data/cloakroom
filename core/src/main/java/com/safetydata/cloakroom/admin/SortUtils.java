package com.safetydata.cloakroom.admin;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeParseException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class SortUtils {
    public enum AttributeType {
        string,
        /**
         * Assumes the attribute is stored as a String parsable as an Integer, otherwise treats it as 0.
         */
        integer,
        /**
         * Assumes the attribute is a ZonedDateTime in ISO-8601 format, otherwise treats it as the beginning of time.
         */
        dateTime
    }

    public static final ZonedDateTime minDate = ZonedDateTime.of(LocalDateTime.MIN, ZoneOffset.UTC);
    public static final ZonedDateTime maxDate = ZonedDateTime.of(LocalDateTime.MAX, ZoneOffset.UTC);

    private interface SortInternal<T> {
        boolean ascending();
        Comparator<T> ascendingComparator();

        default Comparator<T> descendingComparator() {
            return (g1, g2) -> ascendingComparator().compare(g2, g1);
        }
    }

    interface SortFor<T> extends SortInternal<T> {
        default Comparator<T> comparator() {
            if (ascending()) {
                return ascendingComparator();
            } else {
                return descendingComparator();
            }
        }
    }

    public static final <T extends HasAttributes> Comparator<T> comparatorByAttribute(String attribute, AttributeType attributeType, Comparator<T> defaultComparator, boolean missingValuesFirst) {
        return (elem1, elem2) -> {
            Optional<String> e1Value = Optional.ofNullable(elem1.attributes().get(attribute)).orElse(List.of()).stream().findFirst();
            Optional<String> e2Value = Optional.ofNullable(elem2.attributes().get(attribute)).orElse(List.of()).stream().findFirst();

            if (e1Value.isEmpty() && e2Value.isEmpty()) {
                return defaultComparator.compare(elem1, elem2);
            } else if (e1Value.isEmpty()) {
                if (missingValuesFirst) {
                    return -1;
                } else {
                    return 1;
                }
            } else if (e2Value.isEmpty()) {
                if (missingValuesFirst) {
                    return 1;
                } else {
                    return -1;
                }
            }

            String e1ValueDefined = e1Value.get();
            String e2ValueDefined = e2Value.get();

            int result = 0;
            switch (attributeType) {
                case string:
                    result = e1ValueDefined.compareTo(e2ValueDefined);
                    break;
                case integer:
                    int int1;
                    try {
                        int1 = Integer.parseInt(e1ValueDefined);
                    } catch (NumberFormatException nfe) {
                        if (missingValuesFirst) {
                            int1 = Integer.MIN_VALUE;
                        } else {
                            int1 = Integer.MAX_VALUE;
                        }
                    }
                    int int2;
                    try {
                        int2 = Integer.parseInt(e2ValueDefined);
                    } catch (NumberFormatException nfe) {
                        if (missingValuesFirst) {
                            int2 = Integer.MIN_VALUE;
                        } else {
                            int2 = Integer.MAX_VALUE;
                        }
                    }
                    result = Integer.compare(int1, int2);
                    break;
                case dateTime:
                    ZonedDateTime dateTime1;
                    try {
                        dateTime1 = ZonedDateTime.parse(e1ValueDefined);
                    } catch (DateTimeParseException dateTimeParseException) {
                        if (missingValuesFirst) {
                            dateTime1 = minDate;
                        } else {
                            dateTime1 = maxDate;
                        }
                    }
                    ZonedDateTime dateTime2;
                    try {
                        dateTime2 = ZonedDateTime.parse(e2ValueDefined);
                    } catch (DateTimeParseException dateTimeParseException) {
                        if (missingValuesFirst) {
                            dateTime2 = minDate;
                        } else {
                            dateTime2 = maxDate;
                        }
                    }

                    result = dateTime1.compareTo(dateTime2);
                    break;
            }
            if (result == 0) {
                return defaultComparator.compare(elem1, elem2);
            }
            return result;
        };
    }
}
