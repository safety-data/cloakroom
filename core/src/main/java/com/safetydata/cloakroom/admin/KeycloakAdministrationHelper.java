package com.safetydata.cloakroom.admin;

import com.safetydata.cloakroom.admin.exception.*;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ws.rs.NotFoundException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class KeycloakAdministrationHelper {
    private final CloakroomConfigWithAdmin cloakroomConfig;
    private final RealmResource realm;
    protected final String clientName;
    protected final ClientResource client;
    protected final String clientAppId;
    private final GroupsResource groups;
    private final UsersResource users;
    private final Set<Pattern> invisibleUserPatterns;
    protected final int linkLifespanSeconds;

    private static final Logger LOG = LoggerFactory.getLogger(KeycloakAdministrationHelper.class);

    public KeycloakAdministrationHelper(CloakroomConfigWithAdmin cloakroomConfig) {
        this.cloakroomConfig = cloakroomConfig;
        this.realm = cloakroomConfig.adminKeycloak.realm(this.cloakroomConfig.keycloakDeployment().getRealm());
        this.clientName = cloakroomConfig.keycloakDeployment().getResourceName();
        this.clientAppId = findClientAppId();
        this.client = realm.clients().get(clientAppId);
        this.groups = realm.groups();
        this.users = realm.users();
        Config config = ConfigFactory.load();
        String invisibleUsersConfPath = String.format("cloakroom.%s.invisible-users", this.cloakroomConfig.keycloakDeployment().getRealm());
        List<String> invisibleUsers = config.getStringList(invisibleUsersConfPath);
        this.invisibleUserPatterns = invisibleUsers.stream().map(Pattern::compile).collect(Collectors.toSet());
        this.linkLifespanSeconds = config.getInt(String.format("cloakroom.%s.link-lifespan-seconds", this.cloakroomConfig.keycloakDeployment().getRealm()));
    }

    private String findClientAppId() {
        String clientName = cloakroomConfig.keycloakDeployment().getResourceName();
        List<ClientRepresentation> clients = realm.clients().findByClientId(clientName);
        if (clients.size() == 0) {
            throw new ClientNotFoundException(clientName);
        } else if (clients.size() > 1) {
            throw new RuntimeException("This should not happen, we found more than one client with name/id: " + clientName);
        }
        return clients.get(0).getId();
    }

    public long groupSize(GroupId id) {
        try {
            GroupResource groupResource = this.groupResource(id);
            return groupResource.members(0, -1, true).size();
        } catch (NotFoundException nfe) {
            throw new GroupIdNotFoundException(id);
        }
    }


    protected Set<Role> groupRoles(RoleScopeResource roleResource) {
        return roleResource.
            listAll().
            stream().
            map(Role::new).
            collect(Collectors.toSet());
    }

    protected GroupResource groupResource(GroupId id) {
        return groups.group(id.id());
    }

    protected UserResource userResource(UserId id) {
        return users.get(id.id());
    }

    protected GroupRepresentation getGroupRepr(Group group) {
        Optional<GroupRepresentation> groupRepr = groups
            .groups(group.name(),0, Integer.MAX_VALUE)
            .stream()
            .filter(gr -> gr.getName().equals(group.name()))
            .findFirst();
        
        return groupRepr.orElseThrow(() -> new GroupNotFoundException(group.name()));
    }
    
    protected RoleRepresentation getClientRoleRepr(Role role) {
        try {
            return client.roles().get(role.name()).toRepresentation();
        } catch (NotFoundException exception) {
            throw new RoleNotFoundException(role, cloakroomConfig, exception);
        }
    }

    protected List<RoleRepresentation> representationsOfRoles(Collection<Role> roles) {
        return roles.
            stream().
            map(this::getClientRoleRepr).
            collect(Collectors.toList());
    }

    protected List<GroupRepresentation> representationOfGroups(Collection<Group> groups) {
        return groups
            .stream()
            .map(this::getGroupRepr)
            .collect(Collectors.toList());
    }
    
    protected RoleScopeResource getUserRoleResource(UserResource userResource) {
        return userResource.roles().clientLevel(clientAppId);
    }

    protected RoleScopeResource getGroupRoleResource(GroupResource groupResource) {
        return groupResource.roles().clientLevel(clientAppId);
    }

    protected Optional<Role> getRoleByName(String roleName) {
        try {
            return Optional.of(new Role(client.roles().get(roleName).toRepresentation()));
        } catch (NotFoundException exception) {
            return Optional.empty();
        }
    }

    protected List<UserId> getUsersWithRole(String roleName) {
        Set<UserRepresentation> setUserRep = client.roles().get(roleName)
            .getRoleUserMembers();
        LOG.debug("setUserRep : ");
        setUserRep.forEach(u -> LOG.debug(u.getUsername()));
        List<UserId> userIds = setUserRep
            .stream()
            .map(u -> new UserId(u.getId()))
            .collect(Collectors.toList());

        return userIds;
    }

    public List<GroupId> getGroupsWithRole(String roleName) {
        List<GroupId> groupIds = client.roles().get(roleName)
            .getRoleGroupMembers()
            .stream()
            .map(g -> new GroupId(g.getId()))
            .collect(Collectors.toList());

        return groupIds;
    }

    protected UserWithId enrichUserRepr(UserRepresentation userRepr) {
        UserId userId = new UserId(userRepr.getId());
        Set<Role> roles = userRoles(userId);
        Set<Group> groups = userGroups(userId);
        return UserWithIdImpl.fromKeycloakRepr(userRepr).withRoles(roles).withGroups(groups);
    }


    public Set<Role> userRoles(UserId id) {
        try {
            return userRoles(getUserRoleResource(this.userResource(id)));
        } catch (NotFoundException nfe) {
            throw new UserIdNotFoundException(id);
        }
    }

    protected Set<Role> userRoles(RoleScopeResource userRoleResource) {
        return userRoleResource.
            listAll().
            stream().
            map(Role::new).
            collect(Collectors.toSet());
    }


    public Set<Group> userGroups(UserId id) {
        try {
            return userGroups(this.userResource(id));
        } catch (NotFoundException nfe) {
            throw new UserIdNotFoundException(id);
        }
    }

    private Set<Group> userGroups(UserResource userResource) {
        return userResource
            .groups()
            .stream()
            .map(GroupImpl::new)
            .collect(Collectors.toSet());
    }

    protected PaginatedUsers getPaginatedUsers(UserCacheManager userCacheManager, Collection<UserId> userIds, int firstResult, int maxResults, User.Sort sort) {
        return this.getPaginatedUsers(userCacheManager, userIds, firstResult, maxResults, sort, Optional.empty(), Optional.empty());
    }

    protected PaginatedUsers getPaginatedUsers(UserCacheManager userCacheManager, Collection<UserId> userIds, int firstResult, int maxResults, User.Sort sort, Optional<User.Filter> filter, Optional<Role> requiredRole) {
        Map<UserId, UserWithId> userMap = userCacheManager.getUsersFromCache(userIds);

        List<UserWithId> sortedUsers = userMap.values().stream()
            .filter(user -> invisibleUserPatterns.stream().noneMatch(pattern -> {
                Matcher matcher = pattern.matcher(user.username());
                return matcher.matches();
            }))
            .filter(user -> requiredRole.map(r -> user.roles().contains(r)).orElse(true))
            .filter(user -> filter.map( f -> {
                switch (f.field()) {
                    case username:
                        return f.filter().matcher(user.username()).matches();
                    case email:
                        return f.filter().matcher(user.email().orElse("")).matches();
                    default:
                        return true;
                }
            }).orElse(true))
            .sorted(sort.comparator())
            .collect(Collectors.toList());

        int first = Math.max(firstResult, 0);
        int last = Math.min(first + maxResults, sortedUsers.size());
        first = Math.min(first, last);
        return new PaginatedUsers(sortedUsers.subList(first, last), sortedUsers.size());
    }
    protected PaginatedGroups getPaginatedGroups(GroupCacheManager groupCacheManager, Collection<GroupId> groupIds, int firstResult, int maxResults, Group.Sort sort) {
        return this.getPaginatedGroups(groupCacheManager, groupIds, firstResult, maxResults, sort, Optional.empty());
    }

    protected PaginatedGroups getPaginatedGroups(GroupCacheManager groupCacheManager, Collection<GroupId> groupIds, int firstResult, int maxResults, Group.Sort sort, Optional<Pattern> nameFilter) {
        Map<GroupId, GroupWithIdAndSize> groupMap = groupCacheManager.getGroupsFromCache(groupIds);

        List<GroupWithIdAndSize> sortedGroups = groupMap.values().stream()
            .filter(group -> nameFilter.map(filter -> filter.matcher(group.name()).matches()).orElse(true))
            .sorted(sort.comparator())
            .collect(Collectors.toList());

        int first = Math.max(firstResult, 0);
        int last = Math.min(first + maxResults, sortedGroups.size());
        first = Math.min(first, last);
        return new PaginatedGroups(sortedGroups.subList(first, last), sortedGroups.size());
    }
}
