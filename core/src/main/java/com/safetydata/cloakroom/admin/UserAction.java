package com.safetydata.cloakroom.admin;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public enum UserAction {
    VERIFY_EMAIL,
    UPDATE_PROFILE,
    CONFIGURE_TOTP,
    UPDATE_PASSWORD;

    static List<String> toApi(Set<UserAction> actions) {
        return actions.stream().map(UserAction::name).collect(Collectors.toList());
    }

    static Set<UserAction> fromApi(List<String> actions) {
        return actions.stream().map(UserAction::valueOf).collect(Collectors.toSet());
    }
}
