package com.safetydata.cloakroom.admin;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.safetydata.cloakroom.admin.exception.GroupIdNotFoundException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RoleScopeResource;

import jakarta.ws.rs.NotFoundException;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

class GroupCacheManagerImpl extends KeycloakAdministrationHelper implements GroupCacheManager {
    private final CloakroomConfigWithAdmin cloakroomConfig;
    private final RealmResource realm;
    private final GroupsResource groups;

    private final Config config = ConfigFactory.load().getConfig("cloakroom");
    private final int refreshInterval = config.getInt("group-cache.refresh-interval-seconds");
    private final int maxCacheSize = config.getInt("group-cache.maximum-size");

    GroupCacheManagerImpl(CloakroomConfigWithAdmin cloakroomConfig) {
        super(cloakroomConfig);
        this.cloakroomConfig = cloakroomConfig;
        this.realm = cloakroomConfig.adminKeycloak.realm(this.cloakroomConfig.keycloakDeployment().getRealm());
        this.groups = realm.groups();
    }

    private final LoadingCache<GroupId, GroupWithIdAndSize> cache = Caffeine.newBuilder()
        .maximumSize(maxCacheSize)
        .refreshAfterWrite(refreshInterval, TimeUnit.SECONDS)
        .build(key -> getGroupByIdWithSize(key));

    @Override
    public GroupWithIdAndSize getGroupAndUpdateCache(GroupId groupId) {
        GroupWithIdAndSize refreshedGroup = this.getGroupByIdWithSize(groupId);
        cache.put(groupId, refreshedGroup);
        return refreshedGroup;
    }

    @Override
    public Map<GroupId, GroupWithIdAndSize> getGroupsFromCache(Collection<GroupId> groupIds) {
        return cache.getAll(groupIds);
    }

    @Override
    public void removeFromCache(GroupId groupId) {
        cache.invalidate(groupId);
    }

    @Override
    public boolean groupNameAlreadyExists(String groupName, Optional<GroupId> idFromModify) {
        Collection<GroupWithIdAndSize> groupsFromCache = cache.asMap().values();
        List<GroupWithIdAndSize> groupsFilteredByName = groupsFromCache
                .stream()
                .filter(group -> group.name().equalsIgnoreCase(groupName) && idFromModify.map(id -> !group.id().equals(id)).orElse(true))
                .toList();
        return groupsFilteredByName.size()>0;
    }


    @Override
    public void refreshCache() {
        List<GroupId> groupIds = groups.groups().stream()
            .map(g -> new GroupId(g.getId()))
            .collect(Collectors.toList());
        cache.refreshAll(groupIds);
    }

    private GroupWithIdAndSize getGroupByIdWithSize(GroupId id) {
        try {
            long groupSize = groupSize(id);
            RoleScopeResource groupRoleResource = this.getGroupRoleResource(this.groupResource(id));
            Set<Role> roles = groupRoles(groupRoleResource);
            return GroupWithIdAndSize.fromKeycloakRepr(groupResource(id).toRepresentation(), groupSize).withRoles(roles);
        } catch (NotFoundException nfe) {
            throw new GroupIdNotFoundException(id);
        }
    }

}
