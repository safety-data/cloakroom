package com.safetydata.cloakroom.admin;

import java.util.*;
import java.util.regex.Pattern;

public interface UserAdministration {

    UserWithId addUser(User user);

    UserWithId addUser(User user, boolean sendEmail);

    UserWithId modifyUser(UserId id, UserModifier modifier);

    UserWithId modifyUser(UserId id, UserModifier modifier, boolean sendEmail);

    default UserWithId modifyUser(UserWithId user, UserModifier modifier) {
        return modifyUser(user.id(), modifier);
    }

    PaginatedUsers listUsers(int firstResult, int maxResults);

    PaginatedUsers listUsers(int firstResult, int maxResults, User.Sort sort);

    PaginatedUsers listUsers(int firstResult, int maxResults, User.Sort sort, Optional<User.Filter> filter);

    PaginatedUsers listUsers(int firstResult, int maxResults, User.Sort sort, Optional<User.Filter> filter, Optional<Role> requiredRole);

    void deleteUser(UserId id);

    default void deleteUser(UserWithId user) {
        deleteUser(user.id());
    }

    Set<Role> userRoles(UserId id);

    default Set<Role> userRoles(UserWithId user) {
        return userRoles(user.id());
    }

    /**
     * By default when you consult roles, you don't see implicit roles brings by composite roles
     * Using this method, you will see all roles of an user composite one, normal one and roles inherited from composite roles
     *
     * @param id
     * @return
     */
    Set<Role> effectiveUserRoles(UserId id);

    int userCount();

    default Set<Role> effectiveUserRoles(UserWithId user) {
        return effectiveUserRoles(user.id());
    }

    Optional<UserWithId> getUserByUsername(String username);

    //TODO: Maybe we should return an optional because the user can be deleted meanwhile
    UserWithId getUserById(UserId id);

    void refreshCache();
}