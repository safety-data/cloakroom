package com.safetydata.cloakroom.admin.exception;

import com.safetydata.cloakroom.admin.GroupId;

public class GroupIdNotFoundException extends RuntimeException {
    public final GroupId id;
    public GroupIdNotFoundException(GroupId id) {
        super(String.format("No group found with id: %s", id.id()));
        this.id = id;
    }
}
