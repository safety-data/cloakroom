package com.safetydata.cloakroom.admin;

import java.util.Optional;
import java.util.Set;

public class RoleModifier {
    private final Optional<Set<Role>> add;
    private final Optional<Set<Role>> remove;
    private final Optional<Set<Role>> set;

    private RoleModifier(Optional<Set<Role>> add, Optional<Set<Role>> remove, Optional<Set<Role>> set) {
        this.add = add;
        this.remove = remove;
        this.set = set;
    }

    static RoleModifier empty() {
        return new RoleModifier(
                Optional.empty(),
                Optional.empty(),
                Optional.empty()
        );
    }

    public RoleModifier add(Set<Role> rolesToAdd) {
        if(this.set.isPresent()) {
            throw new IllegalArgumentException("Can't modify user by adding role and setting roles at the same time");
        }
        return new RoleModifier(
                Optional.of(rolesToAdd),
                this.remove,
                this.set
        );
    }

    public RoleModifier remove(Set<Role> rolesToRemove) {
        if(this.set.isPresent()) {
            throw new IllegalArgumentException("Can't modify user by removing role and setting roles at the same time");
        }
        return new RoleModifier(
                this.add,
                Optional.of(rolesToRemove),
                this.set
        );
    }

    public RoleModifier set(Set<Role> rolesToSet) {
        if(this.remove.isPresent()) {
            throw new IllegalArgumentException("Can't modify user by removing role and setting roles at the same time");
        }
        if(this.add.isPresent()) {
            throw new IllegalArgumentException("Can't modify user by adding role and setting roles at the same time");
        }
        return new RoleModifier(
                this.add,
                this.remove,
                Optional.of(rolesToSet)
        );
    }

    Optional<Set<Role>> add() {
        return add;
    }

    Optional<Set<Role>> remove() {
        return remove;
    }

    Optional<Set<Role>> set() {
        return set;
    }
}
