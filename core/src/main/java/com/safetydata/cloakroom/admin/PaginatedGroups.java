package com.safetydata.cloakroom.admin;

import java.util.List;

public class PaginatedGroups {
    private final List<GroupWithIdAndSize> groups;
    private final int totalCount;

    public PaginatedGroups(List<GroupWithIdAndSize> groups, int totalCount) {
        this.groups = groups;
        this.totalCount = totalCount;
    }

    public List<GroupWithIdAndSize> groups() {
        return groups;
    }

    public int totalCount() {
        return totalCount;
    }
}
