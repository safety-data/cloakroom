package com.safetydata.cloakroom.admin;

import java.util.Map;

public class KeycloakAdministration {
    private final RoleAdministration roleAdministration;
    private final UserAdministration userAdministration;
    private final GroupAdministration groupAdministration;
    
    KeycloakAdministration(CloakroomConfigWithAdmin cloakroomConfig, Map<String, Integer> roleLimitations) {
        GroupCacheManager groupCacheManager = new GroupCacheManagerImpl(cloakroomConfig);
        UserCacheManager userCacheManager = new UserCacheManagerImpl(cloakroomConfig);
        this.roleAdministration = new RoleAdministration(cloakroomConfig, userCacheManager, groupCacheManager);
        this.groupAdministration = new GroupAdministration(cloakroomConfig, userCacheManager, groupCacheManager);
        this.userAdministration = new UserAdministrationImpl(cloakroomConfig, roleLimitations, userCacheManager, groupCacheManager);
    }

    public RoleAdministration getRoleAdministration() {
        return roleAdministration;
    }

    public UserAdministration getUserAdministration() {
        return userAdministration;
    }

    public GroupAdministration getGroupAdministration() {
        return groupAdministration;
    }

    public static KeycloakAdministration fromConfig(CloakroomConfigWithAdmin config, Map<String, Integer> rolesWithMaxUsersLimitations) {
        return new KeycloakAdministration(config, rolesWithMaxUsersLimitations);
    }

    public static KeycloakAdministration fromConfig(CloakroomConfigWithAdmin config) {
        return fromConfig(config, Map.of());
    }

    public static KeycloakAdministration fromTypeSafeConf(String realm) {
        return fromTypeSafeConf(realm, Map.of());
    }

    public static KeycloakAdministration fromTypeSafeConf(String realm, Map<String, Integer> rolesWithMaxUsersLimitations) {
        return fromConfig(CloakroomConfigWithAdmin.fromTypeSafeConf(realm), rolesWithMaxUsersLimitations);
    }
}
