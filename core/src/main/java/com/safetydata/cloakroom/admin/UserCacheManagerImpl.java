package com.safetydata.cloakroom.admin;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.safetydata.cloakroom.admin.exception.UserIdNotFoundException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import jakarta.ws.rs.NotFoundException;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

class UserCacheManagerImpl extends KeycloakAdministrationHelper implements UserCacheManager {
    private final CloakroomConfigWithAdmin cloakroomConfig;
    private final RealmResource realm;
    private final UsersResource users;

    private final Config config = ConfigFactory.load().getConfig("cloakroom");
    private final int refreshInterval = config.getInt("user-cache.refresh-interval-seconds");
    private final int maxCacheSize = config.getInt("user-cache.maximum-size");

    UserCacheManagerImpl(CloakroomConfigWithAdmin cloakroomConfig) {
        super(cloakroomConfig);
        this.cloakroomConfig = cloakroomConfig;
        this.realm = cloakroomConfig.adminKeycloak.realm(this.cloakroomConfig.keycloakDeployment().getRealm());
        this.users = realm.users();
    }

    private final LoadingCache<UserId, UserWithId> cache = Caffeine.newBuilder()
        .maximumSize(maxCacheSize)
        .refreshAfterWrite(refreshInterval, TimeUnit.SECONDS)
        .build(key -> getUserById(key));

    @Override
    public UserWithId getUserAndUpdateCache(UserId userId) {
        UserWithId refreshedUser = this.getUserById(userId);
        this.cache.put(userId, refreshedUser);
        return refreshedUser;
    }

    @Override
    public Map<UserId, UserWithId> getUsersFromCache(Collection<UserId> userIds) {
        return cache.getAll(userIds);
    }

    @Override
    public void removeFromCache(UserId userId) {
        this.cache.invalidate(userId);
    }

    @Override
    public void refreshCache() {
        List<UserId> userIds = users.list().stream()
            .map(u -> new UserId(u.getId()))
            .collect(Collectors.toList());
        this.cache.refreshAll(userIds);
    }

    public UserWithId getUserById(UserId id) {
        try {
            return enrichUserRepr(this.userResource(id).toRepresentation());
        } catch (NotFoundException nfe) {
            throw new UserIdNotFoundException(id);
        }
    }
}
