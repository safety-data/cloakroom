package com.safetydata.cloakroom.admin;

import java.util.List;
import java.util.Map;

public interface HasAttributes {
    Map<String, List<String>> attributes();
}
