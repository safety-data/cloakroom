package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.UserRepresentation;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface UserWithId extends User {
    UserWithId withGroups(Set<Group> groups);

    UserWithId withRoles(Set<Role> roles);

    UserWithId withAttributes(Map<String, List<String>> attributes);

    User dropId();

    UserId id();

    static UserWithId fromKeycloakRepr(UserRepresentation userRepr) {
        return new UserWithIdImpl(userRepr);
    }
}
