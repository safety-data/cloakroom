package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.GroupRepresentation;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class GroupWithIdAndSizeImpl extends GroupWithIdImpl implements GroupWithIdAndSize {
    private final long size;

    public GroupWithIdAndSizeImpl(Group group, GroupId id, long size) {
        super(group, id);
        this.size = size;
    }

    GroupWithIdAndSizeImpl(GroupRepresentation groupRepr, long size) {
        super(groupRepr);
        this.size = size;
    }

    @Override
    public long size() {
        return size;
    }

    @Override
    public GroupWithIdAndSize withRoles(Set<Role> roles) {
        return new GroupWithIdAndSizeImpl(this.group.withRoles(roles), this.id, this.size);
    }

    @Override
    public GroupWithIdAndSize withAttributes(Map<String, List<String>> attributes) {
        return new GroupWithIdAndSizeImpl(this.group.withAttributes(attributes), this.id, this.size);
    }
}
