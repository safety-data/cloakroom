package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.UserRepresentation;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.stream.Collectors;

class UserImpl implements User {
    private final String username;
    private final Optional<String> email;
    private final Optional<String> firstName;
    private final Optional<String> lastName;
    private final Set<UserAction> requiredActions;
    private final boolean enabled;
    private final Optional<ZonedDateTime> createDate;
    private final Map<String, List<String>> attributes;
    private final Set<Role> roles;
    private final Set<Group> groups;

    UserImpl(
            String username,
            Optional<String> email,
            Optional<String> firstName,
            Optional<String> lastName,
            Set<UserAction> requiredActions,
            boolean enabled,
            Optional<ZonedDateTime> createDate,
            Map<String, List<String>> attributes,
            Set<Role> roles,
            Set<Group> groups
    ) {
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.enabled = enabled;
        this.requiredActions = requiredActions;
        this.createDate = createDate;
        this.attributes = attributes;
        this.groups = groups;
        this.roles = roles;
    }

    static UserImpl.Builder fromKeycloakRepr(UserRepresentation keycloakRepr) {
        UserImpl.Builder builder = new User.Builder();
        Optional.ofNullable(keycloakRepr.getFirstName()).ifPresent(builder::firstName);
        Optional.ofNullable(keycloakRepr.getLastName()).ifPresent(builder::lastName);
        Optional.ofNullable(keycloakRepr.getEmail()).ifPresent(builder::email);

        Optional.ofNullable(keycloakRepr.getRequiredActions()).
                map((s) -> s.stream().map(UserAction::valueOf).collect(Collectors.toSet())).
                ifPresent(builder::requiredActions);

        builder.setEnable(keycloakRepr.isEnabled());
        builder.username(keycloakRepr.getUsername());

        Optional.ofNullable(keycloakRepr.getCreatedTimestamp())
            .ifPresent(createDate -> builder.createDate(ZonedDateTime.ofInstant(Instant.ofEpochMilli(createDate), ZoneOffset.UTC)));

        Optional.ofNullable(keycloakRepr.getAttributes())
            .ifPresent(attributes -> builder.attributes(attributes));

        return builder;
    }

    public static UserImpl.Builder builder() {
        return new UserImpl.Builder();
    }

    @Override
    public String username() {
        return username;
    }

    @Override
    public Optional<String> email() {
        return email;
    }

    @Override
    public Optional<String> firstName() {
        return firstName;
    }

    @Override
    public Optional<String> lastName() {
        return lastName;
    }

    @Override
    public Set<UserAction> requiredActions() {
        return requiredActions;
    }

    @Override
    public Optional<ZonedDateTime> createDate() {
        return createDate;
    }

    @Override
    public Map<String, List<String>> attributes() {
        return this.attributes;
    }

    @Override
    public Set<Role> roles() {
        return this.roles;
    }

    @Override
    public Set<Group> groups() {
        return this.groups;
    }

    @Override
    public UserImpl withRoles(Set<Role> roles) {
        return new UserImpl(
            this.username,
            this.email,
            this.firstName,
            this.lastName,
            this.requiredActions,
            this.enabled,
            this.createDate,
            this.attributes,
            roles,
            this.groups
        );
    }

    @Override
    public UserImpl withGroups(Set<Group> groups) {
        return new UserImpl(
            this.username,
            this.email,
            this.firstName,
            this.lastName,
            this.requiredActions,
            this.enabled,
            this.createDate,
            this.attributes,
            this.roles,
            groups
        );
    }

    @Override
    public UserImpl withAttributes(Map<String, List<String>> attributes) {
        return new UserImpl(
            this.username,
            this.email,
            this.firstName,
            this.lastName,
            this.requiredActions,
            this.enabled,
            this.createDate,
            attributes,
            this.roles,
            this.groups
        );
    }

    UserWithId withId(UserId id) {
        return new UserWithIdImpl(this, id);
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserImpl user = (UserImpl) o;
        return enabled == user.enabled &&
                Objects.equals(username, user.username) &&
                Objects.equals(email, user.email) &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(requiredActions, user.requiredActions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, email, firstName, lastName, requiredActions, enabled);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UserImpl{");
        sb.append("username='").append(username).append('\'');
        sb.append(", email=").append(email);
        sb.append(", firstName=").append(firstName);
        sb.append(", lastName=").append(lastName);
        sb.append(", requiredActions=").append(requiredActions);
        sb.append(", enable=").append(enabled);
        sb.append('}');
        return sb.toString();
    }
}
