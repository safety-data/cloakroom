package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.GroupRepresentation;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface GroupWithIdAndSize extends GroupWithId {
    long size();

    static GroupWithIdAndSize fromKeycloakRepr(GroupRepresentation groupRepr, long size) {
        return new GroupWithIdAndSizeImpl(groupRepr, size);
    }

    GroupWithIdAndSize withRoles(Set<Role> roles);
    GroupWithIdAndSize withAttributes(Map<String, List<String>> attributes);
}
