package com.safetydata.cloakroom.admin;

import com.safetydata.cloakroom.admin.exception.GroupAlreadyExistsException;
import com.safetydata.cloakroom.admin.exception.GroupIdNotFoundException;
import com.safetydata.cloakroom.admin.exception.KeycloakApiException;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.keycloak.admin.client.resource.GroupResource;
import org.keycloak.admin.client.resource.GroupsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;

import jakarta.ws.rs.ClientErrorException;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class GroupAdministration extends KeycloakAdministrationHelper {
    private final CloakroomConfigWithAdmin cloakroomConfig;
    private final RealmResource realm;
    private final GroupsResource groups;

    private final GroupCacheManager groupCacheManager;
    private final UserCacheManager userCacheManager;

    GroupAdministration(CloakroomConfigWithAdmin cloakroomConfig, UserCacheManager userCacheManager, GroupCacheManager groupCacheManager) {
        super(cloakroomConfig);
        this.cloakroomConfig = cloakroomConfig;
        this.groupCacheManager = groupCacheManager;
        this.userCacheManager = userCacheManager;
        this.realm = cloakroomConfig.adminKeycloak.realm(this.cloakroomConfig.keycloakDeployment().getRealm());
        this.groups = realm.groups();
    }

    public GroupWithIdAndSize addGroup(Group group) {
        // This ensures the roles exist before adding the group
        List<RoleRepresentation> roles = representationsOfRoles(group.roles());
        Config config = ConfigFactory.load().getConfig("cloakroom");
        boolean allowGroupsWithIdenticalCaseInsensitiveNames = config.getBoolean("group-cache.allow-groups-with-identical-case-insensitive-names");

        if(!allowGroupsWithIdenticalCaseInsensitiveNames && groupCacheManager.groupNameAlreadyExists(group.name(), Optional.empty())){
            throw new GroupAlreadyExistsException(group.name());
        }

        GroupRepresentation groupRepresentation = group.keycloakRepr();
        Response response = this.groups.add(groupRepresentation);

        if (response.getStatusInfo() == Response.Status.CONFLICT) {
            throw new GroupAlreadyExistsException(group.name());
        }
        KeycloakApiException.checkResponse(response, "create group with name " + group.name());

        // The group id is added at the end of the location path. This regex searches the last string following the last /
        String id = response.getLocation().getPath().replaceAll(".*/([^/]+)$", "$1");
        GroupId groupId = new GroupId(id);

        addRoles(groupId, roles);

       return groupCacheManager.getGroupAndUpdateCache(groupId);
    }
    
    public Optional<GroupWithIdAndSize> getGroupByName(String name) {
        List<GroupRepresentation> groupsRepr = this.groups.groups(name, 0, Integer.MAX_VALUE)
            .stream()
            .filter(g -> name.equals(g.getName())) // Note, keycloak tends to add wildcards around search string, so we limit here
            .collect(Collectors.toList());;
        if (groupsRepr.isEmpty()) {
            return Optional.empty();
        } else if (groupsRepr.size() > 1) {
            throw new IllegalStateException("This should never happen, we got more than one group with the same name");
        }
        // getGroupById returns more info than the list (e.g. attributes).
        GroupRepresentation groupRepr = groupsRepr.get(0);
        GroupId id = new GroupId(groupRepr.getId());
        GroupWithIdAndSize groupWithId = getGroupById(id);
        return Optional.of(groupWithId);
    }
    
    public GroupWithIdAndSize getGroupById(GroupId id) {
        return groupCacheManager.getGroupAndUpdateCache(id);
    }

    public GroupWithIdAndSize modifyGroup(GroupId id, GroupModifier modifier) {
        // Ensure all roles exist before modifying the group
        Optional<List<RoleRepresentation>> rolesToSet = modifier.roleModifier().set().map(rs -> this.representationsOfRoles(rs));
        Optional<List<RoleRepresentation>> rolesToAdd = modifier.roleModifier().add().map(rs -> this.representationsOfRoles(rs));
        Optional<List<RoleRepresentation>> rolesToRemove = modifier.roleModifier().remove().map(rs -> this.representationsOfRoles(rs));
        Config config = ConfigFactory.load().getConfig("cloakroom");
        boolean allowGroupsWithIdenticalCaseInsensitiveNames = config.getBoolean("group-cache.allow-groups-with-identical-case-insensitive-names");

        if(!allowGroupsWithIdenticalCaseInsensitiveNames && groupCacheManager.groupNameAlreadyExists(modifier.name, Optional.of(id))){
            throw new GroupAlreadyExistsException(modifier.name);
        }

        GroupRepresentation groupRepresentation = modifier.toKeycloakRepr();
        GroupResource groupResource = groupResource(id);
        try {
            groupResource.update(groupRepresentation);
        } catch (NotFoundException nfe) {
            throw new GroupIdNotFoundException(id);
        } catch (ClientErrorException clientErrorException) {
            if (clientErrorException.getResponse().getStatusInfo() == Response.Status.CONFLICT ) {
                throw new GroupAlreadyExistsException(modifier.name);
            }
            throw clientErrorException;
        }
        
        if (rolesToSet.isPresent()) {
            Set<Role> oldRoles = groupRoles(id);
            oldRoles.removeAll(modifier.roleModifier().set().get());
            removeRoles(id, representationsOfRoles(oldRoles));
            addRoles(id, rolesToSet.get());
        }
        
        if (rolesToAdd.isPresent()) {
            addRoles(id, rolesToAdd.get());
        }
        
        if (rolesToRemove.isPresent()) {
            removeRoles(id, rolesToRemove.get());
        }

        return groupCacheManager.getGroupAndUpdateCache(id);
    }

    public void deleteGroup(GroupId id) {
        try {
            groupResource(id).remove();
        } catch (NotFoundException nfe) {
            throw new GroupIdNotFoundException(id);
        }
        groupCacheManager.removeFromCache(id);
    }

    public Set<Role> groupRoles(GroupId id) {
        Group group = groupCacheManager.getGroupAndUpdateCache(id);
        return group.roles();
    }

    public PaginatedGroups listGroups(int firstResult, int maxResults) {
        return this.listGroups(firstResult, maxResults, new Group.SortByGroupName(true), Optional.empty());
    }

    public PaginatedGroups listGroups(int firstResult, int maxResults, Group.Sort sort) {
        return this.listGroups(firstResult, maxResults, sort, Optional.empty());
    }

    public PaginatedGroups listGroups(int firstResult, int maxResults, Group.Sort sort, Optional<Pattern> nameFilter) {
        List<GroupId> groupIds = groups.groups().stream()
            .map(g -> new GroupId(g.getId()))
            .collect(Collectors.toList());

       return this.getPaginatedGroups(groupCacheManager, groupIds, firstResult, maxResults, sort, nameFilter);
    }
    
    public long groupCount() {
        return groups.count().values().stream().findFirst().orElseThrow();
    }

    public PaginatedUsers groupMembers(GroupId id, int firstResult, int maxResults) {
        return this.groupMembers(id, firstResult, maxResults, new User.SortByUsername(true));
    }

    public PaginatedUsers groupMembers(GroupId id, int firstResult, int maxResults, User.Sort sort) {
        return groupMembers(id, firstResult, maxResults, sort, Optional.empty(), Optional.empty());
    }

    public PaginatedUsers groupMembers(GroupId id, int firstResult, int maxResults, User.Sort sort, Optional<User.Filter> filter) {
        return groupMembers(id, firstResult, maxResults, sort, filter, Optional.empty());
    }

    public PaginatedUsers groupMembers(GroupId id, int firstResult, int maxResults, User.Sort sort, Optional<User.Filter> filter, Optional<Role> requiredRole) {
        List<UserId> userIds;
        try {
            GroupResource groupResource = this.groupResource(id);
            userIds = groupResource
                .members(0, Integer.MAX_VALUE)
                .stream()
                .map(u -> new UserId(u.getId()))
                .collect(Collectors.toList());
        } catch (NotFoundException nfe) {
            throw new GroupIdNotFoundException(id);
        }
        return this.getPaginatedUsers(userCacheManager, userIds, firstResult, maxResults, sort, filter, requiredRole);
    }

    private void addRoles(GroupId id, List<RoleRepresentation> roles) {
        try {
            getGroupRoleResource(groupResource(id)).add(roles);
        } catch (NotFoundException nfe) {
            throw new GroupIdNotFoundException(id);
        }
    }

    private void removeRoles(GroupId id, List<RoleRepresentation> roles) {
        try {
            getGroupRoleResource(groupResource(id)).remove(roles);
        } catch (NotFoundException nfe) {
            throw new GroupIdNotFoundException(id);
        }
    }

    void deleteGroup(GroupWithId group) {
        deleteGroup(group.id());
    }

    public void refreshCache() {
        this.groupCacheManager.refreshCache();
    }
}
