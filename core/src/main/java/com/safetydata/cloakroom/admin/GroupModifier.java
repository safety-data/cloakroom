package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.GroupRepresentation;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Note - after testing, a GroupModifier must have a name, otherwise it crashes.
 */
public class GroupModifier {
    final String name;
    final Optional<Map<String, List<String>>> attributes;
    private final RoleModifier roleModifier;

    /**
     * Consider using static constructor helper instead
     */
    GroupModifier(
            String name,
            Optional<Map<String, List<String>>> attributes,
            RoleModifier roleModifier
            ) {
        this.name = name;
        this.attributes = attributes;
        this.roleModifier = roleModifier;
    }

    public static GroupModifier forGroup(Group group) {
        return new GroupModifier(
                group.name(),
                Optional.empty(),
                RoleModifier.empty()
        );
    }


    public GroupModifier withName(String name) {
        return new GroupModifier(
                name,
                this.attributes,
                this.roleModifier
        );
    }

     public GroupModifier withAttributes(Map<String, List<String>> attributes) {
        return new GroupModifier(
            this.name,
            Optional.of(attributes),
            this.roleModifier
        );
    }

    public GroupModifier withSettedRoles(Role... roles) {
        return withSettedRoles(Set.of(roles));
    }
    public GroupModifier withSettedRoles(Set<Role> roles) {
        return new GroupModifier(
                this.name,
                this.attributes,
                this.roleModifier.set(roles)
        );
    }

    public GroupModifier withAddedRoles(Role... roles) {
        return withAddedRoles(Set.of(roles));
    }
    public GroupModifier withAddedRoles(Set<Role> roles) {
        return new GroupModifier(
                this.name,
                this.attributes,
                this.roleModifier.add(roles)
        );
    }

    public GroupModifier withRemovedRoles(Role... roles) {
        return withRemovedRoles(Set.of(roles));
    }
    public GroupModifier withRemovedRoles(Set<Role> roles) {
        return new GroupModifier(
                this.name,
                this.attributes,
                this.roleModifier.remove(roles)
        );
    }

    GroupRepresentation toKeycloakRepr() {
        GroupRepresentation repr = new GroupRepresentation();
        repr.setName(name);
        attributes.ifPresent(repr::setAttributes);
        return repr;
    }

    RoleModifier roleModifier() {
        return roleModifier;
    }
}
