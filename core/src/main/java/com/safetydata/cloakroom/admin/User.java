package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.UserRepresentation;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.regex.Pattern;

public interface User extends HasAttributes {
    interface Sort extends SortUtils.SortFor<User> {}

    class SortByUsername implements User.Sort {
        private final boolean ascending;
        private boolean caseInsensitive;
        private Comparator<User> comparator = null;

        public SortByUsername(boolean ascending) {
            this(ascending, true);
        }

        public SortByUsername(boolean ascending, boolean caseInsensitive) {
            this.ascending = ascending;
            this.caseInsensitive = caseInsensitive;
        }

        @Override
        public final boolean ascending() {
            if (comparator==null) {
                comparator = (u1, u2) -> {
                    if (caseInsensitive) {
                        return u1.username().compareToIgnoreCase(u2.username());
                    }
                    else return u1.username().compareTo(u2.username());
                };
            }
            return ascending;
        }

        @Override
        public final Comparator<User> ascendingComparator() {
            return comparator;
        }
    }

    class SortByEmail implements User.Sort {
        private final boolean ascending;
        private final boolean missingValuesFirst;
        private final boolean caseInsensitive;

        private Comparator<User> comparator = null;

        public SortByEmail(boolean ascending) {
            this(ascending, true, true);
        }

        public SortByEmail(boolean ascending, boolean missingValuesFirst) { this(ascending, missingValuesFirst, true); }

        public SortByEmail(boolean ascending, boolean missingValuesFirst, boolean caseInsensitive) {
            this.ascending = ascending;
            this.missingValuesFirst = missingValuesFirst;
            this.caseInsensitive = caseInsensitive;
        }

        @Override
        public final boolean ascending() {
            return ascending;
        }

        @Override
        public final Comparator<User> ascendingComparator() {
            if (comparator==null) {
                comparator = (u1, u2) -> {
                    if (u1.email().isEmpty() && u2.email().isEmpty()) {
                        if (caseInsensitive) {
                            return u1.username().compareToIgnoreCase(u2.username());
                        }
                        else return u1.username().compareTo(u2.username());
                    } else if (u1.email().isEmpty()) {
                        if (missingValuesFirst) {
                            return -1;
                        } else {
                            return 1;
                        }
                    } else if (u2.email().isEmpty()) {
                        if (missingValuesFirst) {
                            return 1;
                        } else {
                            return -1;
                        }
                    } else {
                        int result;
                        if (caseInsensitive) {
                            result = u1.email().get().compareToIgnoreCase(u2.email().get());
                        }
                        else result =  u1.email().get().compareTo(u2.email().get());
                        if (result==0) {
                            if (caseInsensitive) {
                                return u1.username().compareToIgnoreCase(u2.username());
                            }
                            else return u1.username().compareTo(u2.username());
                        }
                        return result;
                    }
                };
            }
            return comparator;
        }
    }

    class SortByCreateDate implements User.Sort {
        private final boolean ascending;
        private final boolean missingValuesFirst;

        private Comparator<User> comparator = null;

        public SortByCreateDate(boolean ascending) {
            this(ascending, true);
        }

        public SortByCreateDate(boolean ascending, boolean missingValuesFirst) {
            this.ascending = ascending;
            this.missingValuesFirst = missingValuesFirst;
        }

        @Override
        public final boolean ascending() {
            return ascending;
        }

        @Override
        public final Comparator<User> ascendingComparator() {
            if (comparator==null) {
                comparator = (u1, u2) -> {
                    int result = u1.createDate().orElse(missingValuesFirst ? SortUtils.minDate : SortUtils.maxDate).compareTo(u2.createDate().orElse(missingValuesFirst ? SortUtils.minDate : SortUtils.maxDate));
                    if (result==0) {
                        return u1.username().compareTo(u2.username());
                    }
                    return result;
                };
            }
            return comparator;
        }
    }

    class SortByAttribute implements User.Sort {
        private final String name;
        private final SortUtils.AttributeType attributeType;
        private final boolean ascending;
        private final boolean missingValuesFirst;

        public SortByAttribute(String name, SortUtils.AttributeType attributeType, boolean ascending) {
            this(name, attributeType, ascending, true);
        }

        public SortByAttribute(String name, SortUtils.AttributeType attributeType, boolean ascending, boolean missingValuesFirst) {
            this.name = name;
            this.attributeType = attributeType;
            this.ascending = ascending;
            this.missingValuesFirst = missingValuesFirst;
        }

        @Override
        public final boolean ascending() {
            return ascending;
        }

        @Override
        public final Comparator<User> ascendingComparator() {
            return SortUtils.comparatorByAttribute(name, attributeType, Comparator.comparing(User::username), missingValuesFirst);
        }
    }

    enum FilterField {
        username,
        email
    }

    class Filter {
        private final FilterField field;
        private final Pattern filter;

        public Filter(FilterField field, Pattern filter) {
            this.field = field;
            this.filter = filter;
        }

        public FilterField field() {
            return field;
        }

        public Pattern filter() {
            return filter;
        }
    }

    String username();

    Optional<String> email();

    Optional<String> firstName();

    Optional<String> lastName();

    Set<UserAction> requiredActions();

    Optional<ZonedDateTime> createDate();

    Map<String, List<String>> attributes();

    Set<Role> roles();

    Set<Group> groups();

    User withRoles(Set<Role> roles);

    User withGroups(Set<Group> groups);

    User withAttributes(Map<String, List<String>> attributes);

    boolean isEnabled();

    default UserRepresentation keycloakRepr() {
        UserRepresentation keycloakRepr = new UserRepresentation();
        keycloakRepr.setEnabled(isEnabled());
        keycloakRepr.setUsername(username());
        email().ifPresent(keycloakRepr::setEmail);
        firstName().ifPresent(keycloakRepr::setFirstName);
        lastName().ifPresent(keycloakRepr::setLastName);
        keycloakRepr.setRequiredActions(UserAction.toApi(this.requiredActions()));
        keycloakRepr.setAttributes(attributes());

        // excluding createDate as we never want to explicitly set the create date in keycloak

        return keycloakRepr;
    }

    public static Builder builder() {
        return new Builder();
    }
    
    public static class Builder {
        private Optional<String> username;
        private Optional<String> email;
        private Optional<String> firstName;
        private Optional<String> lastName;
        private Optional<Boolean> enable;
        private Set<UserAction> requiredActions;
        private Optional<ZonedDateTime> createDate;
        private Map<String, List<String>> attributes;
        private Set<Role> roles;
        private Set<Group> groups;

        Builder() {
            username = Optional.empty();
            email = Optional.empty();
            firstName = Optional.empty();
            lastName = Optional.empty();
            enable = Optional.empty();
            requiredActions = new TreeSet<>();
            createDate = Optional.empty();
            attributes = Map.of();
            roles = Set.of();
            groups = Set.of();
        }

        public Builder username(String username) {
            this.username = Optional.of(username);
            return this;
        }

        public Builder email(String email) {
            this.email = Optional.of(email);
            return this;
        }

        public Builder firstName(String firstName) {
            this.firstName = Optional.of(firstName);
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = Optional.of(lastName);
            return this;
        }

        public Builder enable() {
            this.enable = Optional.of(true);
            return this;
        }

        public Builder disable() {
            this.enable = Optional.of(false);
            return this;
        }

        public Builder setEnable(boolean enable) {
            this.enable = Optional.of(enable);
            return this;
        }

        public Builder requiredActions(Set<UserAction> requiredActions) {
            this.requiredActions = requiredActions;
            return this;
        }

        public Builder requiredActions(UserAction... requiredActions) {
            this.requiredActions(Set.of(requiredActions));
            return this;
        }

        public Builder createDate(ZonedDateTime createDate) {
            this.createDate = Optional.of(createDate);
            return this;
        }

        public Builder attributes(Map<String, List<String>> attributes) {
            this.attributes = attributes;
            return this;
        }

        public Builder roles(Set<Role> roles) {
            this.roles = roles;
            return this;
        }

        public Builder groups(Set<Group> groups) {
            this.groups = groups;
            return this;
        }

        public UserImpl build() {
            if (username.isEmpty()) {
                throw new IllegalStateException("User username must be set");
            }

            if (enable.isEmpty()) {
                throw new IllegalStateException("User must be explicitly enable or disable");
            }

            return new UserImpl(
                username.get(),
                email,
                firstName,
                lastName,
                requiredActions,
                enable.get(),
                createDate,
                attributes,
                roles,
                groups
            );
        }
    }
}
