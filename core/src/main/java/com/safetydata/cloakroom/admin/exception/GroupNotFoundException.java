package com.safetydata.cloakroom.admin.exception;

public class GroupNotFoundException extends RuntimeException {
    public final String name;
    public GroupNotFoundException(String name) {
        super(String.format("Group with following name: %s not found", name));
        this.name = name;
    }
}
