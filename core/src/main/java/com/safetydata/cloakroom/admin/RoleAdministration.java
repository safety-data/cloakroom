package com.safetydata.cloakroom.admin;

import com.safetydata.cloakroom.admin.exception.RoleNotFoundException;
import org.keycloak.admin.client.resource.RolesResource;

import jakarta.ws.rs.NotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class RoleAdministration extends KeycloakAdministrationHelper {
    private final RolesResource roles;
    private final CloakroomConfigWithAdmin cloakroomConfig;
    private final UserCacheManager userCacheManager;
    private final GroupCacheManager groupCacheManager;
    
    RoleAdministration(CloakroomConfigWithAdmin cloakroomConfig, UserCacheManager userCacheManager, GroupCacheManager groupCacheManager) {
        super(cloakroomConfig);
        this.cloakroomConfig = cloakroomConfig;
        this.userCacheManager = userCacheManager;
        this.groupCacheManager = groupCacheManager;
        this.roles = this.client.roles();
    }

    /**
     * List all available roles for client.
     */
    public Set<Role> listAvailableRoles() {
        return client.roles().list().
            stream().
            map(Role::new).
            collect(Collectors.toSet());
    }

    public Optional<Role> getRoleByName(String roleName) {
        return super.getRoleByName(roleName);
    }
    
    public void addRole(Role role) {
        this.roles.create(role.keycloakRepr());
    }
    
    public void deleteRole(String roleName) {
        try {
            this.roles.deleteRole(roleName);
        } catch (NotFoundException nfe) {
            // ignore
        }
    }
    
    public void modifyRole(String roleName, Role role) {
        try {
            this.roles.get(roleName).update(role.keycloakRepr());
        } catch (NotFoundException nfe) {
            throw new RoleNotFoundException(roleName, this.cloakroomConfig);
        }
    }

    public PaginatedGroups listGroupsWithRole(Role role, int firstResult, int maxResults, Group.Sort sort) {
        return this.listGroupsWithRole(role, firstResult, maxResults, sort, Optional.empty());
    }

    public PaginatedGroups listGroupsWithRole(Role role, int firstResult, int maxResults, Group.Sort sort, Optional<Pattern> nameFilter) {
        List<GroupId> groupIds = super.getGroupsWithRole(role.name());
        return this.getPaginatedGroups(groupCacheManager, groupIds, firstResult, maxResults, sort, nameFilter);
    }

    public PaginatedUsers listUsersWithRole(Role role, int firstResult, int maxResults, User.Sort sort) {
        return listUsersWithRole(role, firstResult, maxResults, sort, Optional.empty(), Optional.empty());
    }

    public PaginatedUsers listUsersWithRole(Role role, int firstResult, int maxResults, User.Sort sort, Optional<User.Filter> filter) {
        return listUsersWithRole(role, firstResult, maxResults, sort, filter, Optional.empty());
    }

    public PaginatedUsers listUsersWithRole(Role role, int firstResult, int maxResults, User.Sort sort, Optional<User.Filter> filter, Optional<Role> requiredRole) {
        List<UserId> userIds = super.getUsersWithRole(role.name());
        return this.getPaginatedUsers(userCacheManager, userIds, firstResult, maxResults, sort, filter, requiredRole);
    }
}
