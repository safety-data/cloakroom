package com.safetydata.cloakroom.admin.exception;

public class EmailAlreadyUsedException extends RuntimeException {
    public final String email;
    public EmailAlreadyUsedException(String email) {
        super(String.format("Email %s is already used by an other user", email));
        this.email = email;
    }
}
