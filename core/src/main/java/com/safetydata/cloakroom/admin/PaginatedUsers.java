package com.safetydata.cloakroom.admin;

import java.util.List;

public class PaginatedUsers {
    private final List<UserWithId> users;
    private final int totalCount;

    public PaginatedUsers(List<UserWithId> users, int totalCount) {
        this.users = users;
        this.totalCount = totalCount;
    }

    public List<UserWithId> users() {
        return users;
    }

    public int totalCount() {
        return totalCount;
    }
}
