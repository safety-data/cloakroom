package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.GroupRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.*;

public interface Group extends Comparable<Group>, HasAttributes {

    public interface Sort extends SortUtils.SortFor<GroupWithIdAndSize> {}

    public static class SortByGroupName implements Sort {
        private final boolean ascending;
        private final boolean caseInsensitive;
        private Comparator<GroupWithIdAndSize> comparator = null;

        public SortByGroupName(boolean ascending) {
            this(ascending, true);
        }

        public SortByGroupName(boolean ascending, boolean caseInsensitive) {
            this.ascending = ascending;
            this.caseInsensitive = caseInsensitive;
        }

        @Override
        public final boolean ascending() {
            return ascending;
        }

        @Override
        public final Comparator<GroupWithIdAndSize> ascendingComparator() {
            if (comparator==null) {
                comparator = (g1, g2) -> {
                    if (caseInsensitive) {
                        return g1.name().compareToIgnoreCase(g2.name());
                    } else return g1.name().compareTo(g2.name());
                };
            }
            return comparator;
        }
    }

    public static class SortByAttribute implements Sort {
        private final String name;
        private final SortUtils.AttributeType attributeType;
        private final boolean ascending;
        private final boolean missingValuesFirst;

        public SortByAttribute(String name, SortUtils.AttributeType attributeType, boolean ascending) {
            this(name, attributeType, ascending, true);
        }

        public SortByAttribute(String name, SortUtils.AttributeType attributeType, boolean ascending, boolean missingValuesFirst) {
            this.name = name;
            this.attributeType = attributeType;
            this.ascending = ascending;
            this.missingValuesFirst = missingValuesFirst;
        }

        @Override
        public final boolean ascending() {
            return ascending;
        }

        @Override
        public final Comparator<GroupWithIdAndSize> ascendingComparator() {
            return SortUtils.comparatorByAttribute(name, attributeType, Comparator.comparing(Group::name), missingValuesFirst);
        }
    }

    public static class SortBySize implements Sort {
        private final boolean ascending;

        public SortBySize(boolean ascending) {
            this.ascending = ascending;
        }

        private final Comparator<GroupWithIdAndSize> comparator = (g1, g2) -> {
            int result = Long.compare(g1.size(), g2.size());
            if (result==0) {
                return g1.name().compareTo(g2.name());
            }
            return result;
        };

        @Override
        public final boolean ascending() {
            return ascending;
        }

        @Override
        public final Comparator<GroupWithIdAndSize> ascendingComparator() {
            return comparator;
        }
    }

    String name();
    Map<String, List<String>> attributes();
    Set<Role> roles();

    Group withAttributes(Map<String, List<String>> attributes);
    Group withRoles(Set<Role> roles);

    default GroupRepresentation keycloakRepr() {
        GroupRepresentation keycloakRepr = new GroupRepresentation();
        keycloakRepr.setName(name());
        keycloakRepr.setAttributes(attributes());

        return keycloakRepr;
    }

    public static Builder builder() {
        return new Builder();
    }
    
    public static class Builder {
        private Optional<String> name;
        private Map<String, List<String>> attributes;
        private Set<Role> roles;

        Builder() {
            name = Optional.empty();
            attributes = Map.of();
            roles = Set.of();
        }

        public Builder name(String name) {
            this.name = Optional.of(name);
            return this;
        }

        public Builder attributes(Map<String, List<String>> attributes) {
            this.attributes = attributes;
            return this;
        }

        public Builder roles(Set<Role> roles) {
            this.roles = roles;
            return this;
        }

        public Group build() {
            if (name.isEmpty()) {
                throw new IllegalStateException("Group name must be set");
            }

            Group group = new GroupImpl(
                name.get(),
                this.attributes,
                this.roles
            );

            return group;
        }
    }
}
