package com.safetydata.cloakroom.admin.exception;

import com.safetydata.cloakroom.admin.Role;
import com.safetydata.cloakroom.admin.User;

import java.util.Set;
import java.util.stream.Collectors;

public class PartiallyModifiedUserException extends RuntimeException{

    public PartiallyModifiedUserException(User user, Set<Role> rolesExceedsMax) {
        super(String.format("Couldn't add roles: %s to the User: %s, because these roles would exceed the accepted limit",
                rolesExceedsMax.stream().map(Role::name).collect(Collectors.joining(", ")),
                user.username()));
    }

}
