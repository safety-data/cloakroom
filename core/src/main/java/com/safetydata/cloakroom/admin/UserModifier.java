package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.UserRepresentation;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class UserModifier {
    final Optional<String> email;
    final Optional<String> firstName;
    final Optional<String> lastName;
    final Optional<Set<UserAction>> requiredActions;
    final Optional<Boolean> enable;
    final Optional<Map<String, List<String>>> attributes;
    private final RoleModifier roleModifier;
    private final UserGroupModifier groupModifier;

    /**
     * Consider using static constructor helper instead
     */
    UserModifier(
            Optional<String> email,
            Optional<String> firstName,
            Optional<String> lastName,
            Optional<Set<UserAction>> requiredActions,
            Optional<Boolean> enable,
            Optional<Map<String, List<String>>> attributes,
            RoleModifier roleModifier,
            UserGroupModifier groupModifier
            ) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.enable = enable;
        this.requiredActions = requiredActions;
        this.attributes = attributes;
        this.roleModifier = roleModifier;
        this.groupModifier = groupModifier;
    }

    private static UserModifier empty() {
        return new UserModifier(
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            RoleModifier.empty(),
            UserGroupModifier.empty()
        );
    }

    public static UserModifier email(String email) {
        return empty().withEmail(email);
    }
    public UserModifier withEmail(String email) {
        return new UserModifier(
            Optional.of(email),
            this.firstName,
            this.lastName,
            this.requiredActions,
            this.enable,
            this.attributes,
            this.roleModifier,
            this.groupModifier
        );
    }

    public static UserModifier firstName(String firstName) {
        return empty().withFirstName(firstName);
    }
    public UserModifier withFirstName(String firstName) {
        return new UserModifier(
            this.email,
            Optional.of(firstName),
            this.lastName,
            this.requiredActions,
            this.enable,
            this.attributes,
            this.roleModifier,
            this.groupModifier
        );
    }

    public static UserModifier lastName(String lastName) {
        return empty().withLastName(lastName);
    }
    public UserModifier withLastName(String lastName) {
        return new UserModifier(
            this.email,
            this.firstName,
            Optional.of(lastName),
            this.requiredActions,
            this.enable,
            this.attributes,
            this.roleModifier,
            this.groupModifier
        );
    }

    public static UserModifier activated() {
        return empty().activate();
    }
    public UserModifier activate() {
        return new UserModifier(
            email,
            this.firstName,
            this.lastName,
            this.requiredActions,
            Optional.of(true),
            this.attributes,
            this.roleModifier,
            this.groupModifier
        );
    }

    public static UserModifier disable() {
        return empty().setDisable();
    }
    public UserModifier setDisable() {
        return new UserModifier(
            this.email,
            this.firstName,
            this.lastName,
            this.requiredActions,
            Optional.of(false),
            this.attributes,
            this.roleModifier,
            this.groupModifier
        );
    }

    public static UserModifier attributes(Map<String, List<String>> attributes) {
        return empty().withAttributes(attributes);
    }
    public UserModifier withAttributes(Map<String, List<String>> attributes) {
        return new UserModifier(
            this.email,
            this.firstName,
            this.lastName,
            this.requiredActions,
            this.enable,
            Optional.of(attributes),
            this.roleModifier,
            this.groupModifier
        );
    }

    public static UserModifier requiredActions(Set<UserAction> actions) {
        return empty().withRequiredActions(actions);
    }
    public static UserModifier requiredActions(UserAction... actions) {
        return empty().withRequiredActions(actions);
    }
    public UserModifier withRequiredActions(UserAction... actions) {
        return withRequiredActions(Set.of(actions));
    }

    public UserModifier withRequiredActions(Set<UserAction> actions) {
        return new UserModifier(
            this.email,
            this.firstName,
            this.lastName,
            Optional.of(actions),
            this.enable,
            this.attributes,
            this.roleModifier,
            this.groupModifier
        );
    }

    public static UserModifier setRoles(Set<Role> roles) {
        return empty().withSettedRoles(roles);
    }
    public static UserModifier setRoles(Role... roles) {
        return empty().withSettedRoles(roles);
    }
    public UserModifier withSettedRoles(Role... roles) {
        return withSettedRoles(Set.of(roles));
    }
    public UserModifier withSettedRoles(Set<Role> roles) {
        return new UserModifier(
            this.email,
            this.firstName,
            this.lastName,
            this.requiredActions,
            this.enable,
            this.attributes,
            this.roleModifier.set(roles),
            this.groupModifier
        );
    }

    public static UserModifier addRoles(Set<Role> roles) {
        return empty().withAddedRoles(roles);
    }
    public static UserModifier addRoles(Role... roles) {
        return empty().withAddedRoles(roles);
    }
    public UserModifier withAddedRoles(Role... roles) {
        return withAddedRoles(Set.of(roles));
    }
    public UserModifier withAddedRoles(Set<Role> roles) {
        return new UserModifier(
            this.email,
            this.firstName,
            this.lastName,
            this.requiredActions,
            this.enable,
            this.attributes,
            this.roleModifier.add(roles),
            this.groupModifier
        );
    }

    public static UserModifier removeRoles(Set<Role> roles) {
        return empty().withRemovedRoles(roles);
    }
    public static UserModifier removeRoles(Role... roles) {
        return empty().withRemovedRoles(roles);
    }
    public UserModifier withRemovedRoles(Role... roles) {
        return withRemovedRoles(Set.of(roles));
    }
    public UserModifier withRemovedRoles(Set<Role> roles) {
        return new UserModifier(
            this.email,
            this.firstName,
            this.lastName,
            this.requiredActions,
            this.enable,
            this.attributes,
            this.roleModifier.remove(roles),
            this.groupModifier
        );
    }

    public static UserModifier setGroups(Set<Group> groups) {
        return empty().withSettedGroups(groups);
    }
    public static UserModifier setGroups(Group... groups) {
        return empty().withSettedGroups(groups);
    }
    public UserModifier withSettedGroups(Group... groups) {
        return withSettedGroups(Set.of(groups));
    }
    public UserModifier withSettedGroups(Set<Group> groups) {
        return new UserModifier(
            this.email,
            this.firstName,
            this.lastName,
            this.requiredActions,
            this.enable,
            this.attributes,
            this.roleModifier,
            this.groupModifier.set(groups)
        );
    }

    public static UserModifier addGroups(Set<Group> groups) {
        return empty().withAddedGroups(groups);
    }
    public static UserModifier addGroups(Group... groups) {
        return empty().withAddedGroups(groups);
    }
    public UserModifier withAddedGroups(Group... groups) {
        return withAddedGroups(Set.of(groups));
    }
    public UserModifier withAddedGroups(Set<Group> groups) {
        return new UserModifier(
            this.email,
            this.firstName,
            this.lastName,
            this.requiredActions,
            this.enable,
            this.attributes,
            this.roleModifier,
            this.groupModifier.add(groups)
        );
    }

    public static UserModifier removeGroups(Set<Group> groups) {
        return empty().withRemovedGroups(groups);
    }
    public static UserModifier removeGroups(Group... groups) {
        return empty().withRemovedGroups(groups);
    }
    public UserModifier withRemovedGroups(Group... groups) {
        return withRemovedGroups(Set.of(groups));
    }
    public UserModifier withRemovedGroups(Set<Group> groups) {
        return new UserModifier(
            this.email,
            this.firstName,
            this.lastName,
            this.requiredActions,
            this.enable,
            this.attributes,
            this.roleModifier,
            this.groupModifier.remove(groups)
        );
    }

    UserRepresentation toKeycloakRepr() {
        UserRepresentation repr = new UserRepresentation();
        email.ifPresent(repr::setEmail);
        firstName.ifPresent(repr::setFirstName);
        lastName.ifPresent(repr::setLastName);
        enable.ifPresent(repr::setEnabled);
        attributes.ifPresent(repr::setAttributes);
        requiredActions.map(UserAction::toApi).ifPresent(repr::setRequiredActions);
        return repr;
    }

    boolean concernRequiredAction() {
        return this.requiredActions.isPresent();
    }

    boolean makeUserEnable() {
        return this.enable.orElse(false);
    }

    RoleModifier roleModifier() {
        return roleModifier;
    }

    UserGroupModifier groupModifier() {
        return groupModifier;
    }
}
