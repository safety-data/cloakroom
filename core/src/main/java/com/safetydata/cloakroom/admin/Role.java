package com.safetydata.cloakroom.admin;

import org.keycloak.representations.idm.RoleRepresentation;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * In cloakroom when we speak about role, we do not mean realm role, but role specific to the client/resource specify in the conf
 * Constructor is hidden so you can't construct an role that does not exist
 */
public class Role implements Comparable<Role> {
    private final String name;

    public Role(String name) {
        this.name = name;
    }

    Role(RoleRepresentation roleRepresentation) {
        this.name = roleRepresentation.getName();
    }

    RoleRepresentation keycloakRepr() {
        RoleRepresentation keycloakRepr = new RoleRepresentation();
        keycloakRepr.setName(name());

        return keycloakRepr;
    }
    
    public String name() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return Objects.equals(name, role.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Role{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(Role role) {
        return this.name.compareTo(role.name);
    }
}
