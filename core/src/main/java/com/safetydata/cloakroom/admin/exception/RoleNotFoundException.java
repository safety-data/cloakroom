package com.safetydata.cloakroom.admin.exception;

import com.safetydata.cloakroom.admin.CloakroomConfigWithAdmin;
import com.safetydata.cloakroom.admin.Role;

public class RoleNotFoundException extends ResourceNotFoundException {
    public String roleName;
    public RoleNotFoundException(Role role, CloakroomConfigWithAdmin config, Throwable subError) {
        super(
                String.format("Role %s for client %s does not exist",
                        role.name(),
                        config.keycloakDeployment().getResourceName()),
                subError);
        this.roleName = role.name();
    }

    public RoleNotFoundException(String roleName, CloakroomConfigWithAdmin config) {
        super(
                String.format("Role %s for client %s does not exist",
                        roleName,
                        config.keycloakDeployment().getResourceName()));
    }
}
