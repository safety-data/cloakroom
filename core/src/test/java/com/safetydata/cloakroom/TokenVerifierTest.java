package com.safetydata.cloakroom;

import com.typesafe.config.ConfigFactory;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import static org.junit.Assert.*;

public class TokenVerifierTest {
    private static final String REALM = "test-realm";
    
    @Test
    public void parseBadTokenShouldFailWithCouldNotParseToken() {
        TokenVerifier tokenVerifier = TokenVerifier.fromTypeSafeConf(REALM);
        VerificationResult result = tokenVerifier.verify("bad_token").join();
        assertFalse(result.isValid());
        assertEquals(result.token(), Optional.empty());
        assertTrue(result.error().get() instanceof CouldNotParseToken);
    }

    @Test
    public void parseTokenWithBadSshKeyShouldFailWithBadSshKey() throws IOException, InterruptedException {
        TokenVerifier tokenVerifier = new TokenVerifier(() -> CompletableFuture.completedFuture(Map.of()), TokenTestUtil.getDumpDeployment());
        VerificationResult result = tokenVerifier.verify(TokenTestUtil.getTokenFromKeycloak(REALM)).join();
        assertFalse(result.isValid());
        assertEquals(result.token(), Optional.empty());
        assertTrue(result.error().get() instanceof BadPublicKeys);
    }

    @Test
    public void parseValidToken() throws IOException, InterruptedException {
        String token = TokenTestUtil.getTokenFromKeycloak(REALM);
        TokenVerifier tokenVerifier = TokenVerifier.fromTypeSafeConf(REALM);
        VerificationResult result = tokenVerifier.verify(token).join();
        assertTrue(result.isValid());
        assertEquals(result.token().get().issuedFor, "cloakroom");
        assertTrue(result.token().isPresent());
        assertEquals(
                result.roles().get(),
                Set.copyOf(ConfigFactory.load().getStringList("cloakroom.test.roles")));

        assertEquals(
                result.username().get(),
                ConfigFactory.load().getString("cloakroom.test.username"));

        assertEquals(
                result.email().get(),
                ConfigFactory.load().getString("cloakroom.test.email"));
    }

    @Test
    public void parseTokenWithoutAnyClientRoleShouldNotFailWhenGettingRoles() throws IOException, InterruptedException {
        String token = TokenTestUtil.getTokenFromKeycloakWithoutAnyRole(REALM);
        TokenVerifier tokenVerifier = TokenVerifier.fromTypeSafeConf(REALM);
        VerificationResult result = tokenVerifier.verify(token).join();
        assertTrue(result.isValid());
        assertEquals(result.token().get().issuedFor, "cloakroom");
        assertTrue(result.token().isPresent());
        assertEquals(
                result.roles().get(),
                Set.of());
    }

    @Test
    public void parseInvalidTokenShouldFailWithInvalidToken() {
        TokenTestUtil.TokenAndKey tokenAndKey = TokenTestUtil.generateInvalidToken();
        TokenVerifier tokenVerifier = new TokenVerifier(() -> CompletableFuture.completedFuture(Map.of(tokenAndKey.keyId, tokenAndKey.publicKey)), TokenTestUtil.getDumpDeployment());
        VerificationResult result = tokenVerifier.verify(tokenAndKey.token).join();
        assertFalse(result.isValid());
        assertTrue(result.token().isEmpty());
        assertTrue(result.error().get() instanceof InvalidToken);
    }
}