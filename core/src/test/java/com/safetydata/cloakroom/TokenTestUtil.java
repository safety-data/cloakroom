package com.safetydata.cloakroom;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.representations.adapters.config.AdapterConfig;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

public class TokenTestUtil {
    private static final String usernameWithoutAnyRoles = "userwithoutanyrole";

    public static RSAPublicKey generatePublicKey() {
        return ((RSAPublicKey) generateKeys().getPublic());
    }

    public static KeyPair generateKeys() {
        KeyPairGenerator kpg = null;
        try {
            kpg = KeyPairGenerator.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            // this should not happen
            throw new RuntimeException(e);
        }
        kpg.initialize(514);
        return kpg.generateKeyPair();
    }

    // Invalid because it will be missing a lot of claim such as "subject"
    public static TokenAndKey generateInvalidToken() {
        TokenAndKey result = new TokenAndKey();
        try {
            KeyPair keyPair = generateKeys();
            result.publicKey = (RSAPublicKey) keyPair.getPublic();
            Algorithm algorithm = Algorithm.RSA256(result.publicKey, (RSAPrivateKey) keyPair.getPrivate());
            result.keyId = "keyId";
            result.token = JWT.create()
                    .withIssuer("auth0")
                    .withKeyId(result.keyId)
                    .sign(algorithm);
            return result;
        } catch (JWTCreationException exception) {
            //this should not happen
            throw new RuntimeException(exception);
        }

    }

    public static KeycloakDeployment getDumpDeployment() {
        AdapterConfig adapterConfig = new AdapterConfig();
        adapterConfig.setRealm("realm");
        adapterConfig.setResource("client");
        adapterConfig.setAuthServerUrl("http://localhost:42");
        return KeycloakDeploymentBuilder.build(adapterConfig);
    }

    public static String getTokenFromKeycloak(String realm) throws IOException, InterruptedException {
        Config testConfig = ConfigFactory.load().getConfig("cloakroom.test");
        String username = testConfig.getString("username");
        String password = testConfig.getString("password");
        return getTokenFromKeycloak(realm, username, password);
    }

    public static String getTokenFromKeycloakWithoutAnyRole(String realm) throws IOException, InterruptedException {
        Config testConfig = ConfigFactory.load().getConfig("cloakroom.test");
        String password = testConfig.getString("password");
        return getTokenFromKeycloak(realm, usernameWithoutAnyRoles, password);
    }

    public static String getTokenFromKeycloak(String realm, String username, String password) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newHttpClient();
        KeycloakDeployment deployment = CloakroomConfig.fromTypeSafeConf(realm).keycloakDeployment();
        String url = deployment.getTokenUrl();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url))
                .POST(HttpRequest.BodyPublishers.ofString(
                        String.format("grant_type=password&username=%s&password=%s&client_id=%s",
                                username, password, deployment.getResourceName())
                ))
                .setHeader("Content-Type", "application/x-www-form-urlencoded")
                .build();

        InputStream is = client.send(request, HttpResponse.BodyHandlers.ofInputStream()).body();
        ObjectMapper objectMapper = new ObjectMapper();
        AccessTokenWrapper accessTokenWrapper = objectMapper.readValue(is, AccessTokenWrapper.class);

        if (accessTokenWrapper.error != null) {
            throw new RuntimeException("Could not fetch token: " + accessTokenWrapper.error);
        }

        return accessTokenWrapper.accessToken;
    }

    public static class TokenAndKey {
        public String token;
        public RSAPublicKey publicKey;
        public String keyId;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class AccessTokenWrapper {

        @JsonProperty("access_token")
        public String accessToken;

        public String error;
    }
}
