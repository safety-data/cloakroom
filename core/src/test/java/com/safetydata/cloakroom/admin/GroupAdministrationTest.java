package com.safetydata.cloakroom.admin;

import com.safetydata.cloakroom.admin.exception.GroupAlreadyExistsException;
import com.safetydata.cloakroom.admin.exception.GroupIdNotFoundException;
import com.safetydata.cloakroom.admin.exception.RoleNotFoundException;
import com.safetydata.cloakroom.mail.FakeSMTPChecker;
import com.typesafe.config.ConfigFactory;
import org.junit.*;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.safetydata.cloakroom.admin.GroupTestUtil.group;
import static org.junit.Assert.assertEquals;

public class GroupAdministrationTest {
    private static GroupAdministration groupAdministration;
    private static UserAdministration userAdministration;
    private static final String testGroupName = "cloakroom_test_group";
    private static final String totoGroupName = "toto";
    private static final String titiGroupName = "titi";
    private static FakeSMTPChecker smtpChecker;
    private static final String REALM = "test-realm";
    
    @BeforeClass
    public static void beforeAll() {
        smtpChecker = FakeSMTPChecker.fromTypesafeConf();
        KeycloakAdministration keycloakAdministration = KeycloakAdministration.fromTypeSafeConf(REALM);
        groupAdministration = keycloakAdministration.getGroupAdministration();
        smtpChecker = FakeSMTPChecker.fromTypesafeConf();
        userAdministration = keycloakAdministration.getUserAdministration();
    }

    @Before
    public void beforeEach() {
        smtpChecker.clearAllMail();
        GroupTestUtil.cleanGroups(groupAdministration);
        List<Group> groups = new ArrayList<>();
        groups.add(group("group1b", "group1_role"));
        groups.add(group("group1", "group1_role"));
        groups.add(group("group2", "son_b"));
        groups.add(group("group3"));
        GroupTestUtil.createGroups(groups, groupAdministration);
    }

    @Test
    public void getGroupByName() {
        // Note we have a group1 and a group1b.
        // Since keycloak tends to add wildcards around search strings, this test proves we only return group1.
        GroupWithId fetchedGroup = groupAdministration.getGroupByName("group1").get();
        assertEquals("group1", fetchedGroup.name());
    }

    @Test(expected = GroupIdNotFoundException.class)
    public void getNonexistentGroup() {
        groupAdministration.getGroupById(new GroupId("blah"));
    }

    @Test
    public void addAndGetGroup() {
        Group group = Group.builder()
            .name(testGroupName)
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .build();

        GroupWithId returnedGroup = groupAdministration.addGroup(group);

        GroupWithId createdGroup = groupAdministration.getGroupByName(group.name()).get();
        assertEquals(createdGroup.dropId(), group);
        assertEquals(returnedGroup.dropId(), group);
    }

    @Test(expected = GroupAlreadyExistsException.class)
    public void addGroupWithAnAlreadyExistingNameShouldFailWithCorrectError() {
        Group group = Group.builder()
            .name(titiGroupName)
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .build();

        Group otherGroup = Group.builder()
            .name(titiGroupName)
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .build();

        groupAdministration.addGroup(group);
        groupAdministration.addGroup(otherGroup);
    }

    @Test(expected = RoleNotFoundException.class)
    public void addGroupWithRoleThatDoesNotExistShouldFailWithProperException() {
        Group group = Group.builder()
            .name(titiGroupName)
            .roles(RoleTestUtil.roles("I don't exist"))
            .build();
        
        groupAdministration.addGroup(group);
    }
    
    @Test
    public void modifyGroup() {
        Group group = Group.builder()
            .name(testGroupName)
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .build();

        GroupWithId returnedGroup = groupAdministration.addGroup(group);
        
        GroupModifier modifier = GroupModifier.forGroup(returnedGroup).withName(titiGroupName);
        
        groupAdministration.modifyGroup(returnedGroup.id(), modifier);

        GroupWithId fetchedGroup = groupAdministration.getGroupById(returnedGroup.id());
        assertEquals(titiGroupName, fetchedGroup.name());
    }

    @Test
    public void modifyGroupWithSetRoles() {
        Group group = Group.builder()
            .name(testGroupName)
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .build();

        GroupWithId returnedGroup = groupAdministration.addGroup(group);

        Set<Role> newRoles = RoleTestUtil.roles("son_b", "son_c");
        GroupModifier modifier = GroupModifier.forGroup(returnedGroup).withSettedRoles(newRoles);

        groupAdministration.modifyGroup(returnedGroup.id(), modifier);

        GroupWithId fetchedGroup = groupAdministration.getGroupById(returnedGroup.id());
        assertEquals(newRoles, fetchedGroup.roles());
    }

    @Test
    public void modifyGroupWithAddRoles() {
        Set<Role> initialRoles = RoleTestUtil.roles("son_a", "son_b");
        Group group = Group.builder()
            .name(testGroupName)
            .roles(initialRoles)
            .build();

        GroupWithId returnedGroup = groupAdministration.addGroup(group);

        Set<Role> rolesToAdd = RoleTestUtil.roles("son_b", "son_c");
        GroupModifier modifier = GroupModifier.forGroup(returnedGroup).withAddedRoles(rolesToAdd);

        groupAdministration.modifyGroup(returnedGroup.id(), modifier);

        Set<Role> finalRoles = new HashSet<>(initialRoles);
        finalRoles.addAll(rolesToAdd);
        
        GroupWithId fetchedGroup = groupAdministration.getGroupById(returnedGroup.id());
        assertEquals(finalRoles, fetchedGroup.roles());
    }

    @Test
    public void modifyGroupWithRemoveRoles() {
        Set<Role> initialRoles = RoleTestUtil.roles("son_a", "son_b");
        Group group = Group.builder()
            .name(testGroupName)
            .roles(initialRoles)
            .build();

        GroupWithId returnedGroup = groupAdministration.addGroup(group);

        Set<Role> rolesToRemove = RoleTestUtil.roles("son_b", "son_c");
        GroupModifier modifier = GroupModifier.forGroup(returnedGroup).withRemovedRoles(rolesToRemove);

        Set<Role> finalRoles = new HashSet<>(initialRoles);
        finalRoles.removeAll(rolesToRemove);
            
        groupAdministration.modifyGroup(returnedGroup.id(), modifier);

        GroupWithId fetchedGroup = groupAdministration.getGroupById(returnedGroup.id());
        assertEquals(finalRoles, fetchedGroup.roles());
    }

    @Test
    public void modifyGroupWithABunchOfStuff() {
        Set<Role> initialRoles = RoleTestUtil.roles("son_a", "son_b");
        Group group = Group.builder()
            .name(testGroupName)
            .roles(initialRoles)
            .build();

        GroupWithId returnedGroup = groupAdministration.addGroup(group);

        Set<Role> rolesToAdd = RoleTestUtil.roles("son_c");
        Set<Role> rolesToRemove = RoleTestUtil.roles("son_b");
        GroupModifier modifier = GroupModifier.forGroup(returnedGroup).withName(titiGroupName)
            .withRemovedRoles(rolesToRemove)
            .withAddedRoles(rolesToAdd);

        Set<Role> finalRoles = new HashSet<>(initialRoles);
        finalRoles.addAll(rolesToAdd);
        finalRoles.removeAll(rolesToRemove);

        groupAdministration.modifyGroup(returnedGroup.id(), modifier);

        GroupWithId fetchedGroup = groupAdministration.getGroupById(returnedGroup.id());
        assertEquals(titiGroupName, fetchedGroup.name());
        assertEquals(finalRoles, fetchedGroup.roles());
    }

    @Test(expected = RoleNotFoundException.class)
    public void modifyGroupToAddRolesThatDoesNotExistShouldFailWithProperException() {
        Group group = Group.builder()
            .name(titiGroupName)
            .build();

        GroupWithId returnedGroup = groupAdministration.addGroup(group);
        GroupModifier modifier = GroupModifier.forGroup(returnedGroup).withAddedRoles(RoleTestUtil.roles("I don't exist"));

        groupAdministration.modifyGroup(returnedGroup.id(), modifier);
    }

    @Test(expected = GroupAlreadyExistsException.class)
    public void modifyGroupToNameThatAlreadyExist() {
        System.setProperty("config.file", getClass().getClassLoader().getResource("caseInsensitiveGroupNames.conf").getPath());
        ConfigFactory.invalidateCaches();

        Set<Role> initialRoles = RoleTestUtil.roles("son_a", "son_b");
        Group group = Group.builder()
            .name("diblah")
            .roles(initialRoles)
            .build();

        GroupWithId returnedGroup = groupAdministration.addGroup(group);

        Group anotherGroup = Group.builder()
            .name("blah")
            .roles(initialRoles)
            .build();

        groupAdministration.addGroup(anotherGroup);

        GroupModifier modifier = GroupModifier.forGroup(returnedGroup).withName("Blah");


        groupAdministration.modifyGroup(returnedGroup.id(), modifier);

        System.clearProperty("config.file");
        ConfigFactory.invalidateCaches();
    }

    @Test 
    public void listGroups() {
        Set<String> groupsReturned = groupAdministration.listGroups(0, 10).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toSet());

        Set<String> expected = new HashSet<>(Arrays.asList("group3", "group1b", "group2", "group1"));
        assertEquals(expected, groupsReturned);
    }

    @Test
    public void listGroupsWithPagination() {
        PaginatedGroups paginatedGroups = groupAdministration.listGroups(1, 2, new Group.SortByGroupName(true));
        List<String> groupsByName = paginatedGroups.groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        List<String> expected = Arrays.asList("group1b", "group2");
        assertEquals(expected, groupsByName);
        assertEquals(4, paginatedGroups.totalCount());

        List<String> groupsByReverseName = groupAdministration.listGroups(1, 2, new Group.SortByGroupName(false)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        expected = Arrays.asList("group2", "group1b");
        assertEquals(expected, groupsByReverseName);
    }

    @Test
    public void listGroupsWithPaginationWithCase() {
        GroupTestUtil.cleanGroups(groupAdministration);
        Group group5 = group("agroup5", "another_role");
        Group group6 = group("Agroup6", "another_role");
        Group group7 = group("bgroup7", "another_role");
        Group group8 = group("Bgroup8", "another_role");
        GroupTestUtil.createGroups(Arrays.asList(group5, group6, group7, group8), groupAdministration);

        List<String> groupsByNameWithCaseInsensitive = groupAdministration.listGroups(0, 4, new Group.SortByGroupName(true))
                .groups().stream().map(Group::name).toList();
        List<String> expected = Arrays.asList(group5.name(), group6.name(), group7.name(), group8.name());
        assertEquals(expected, groupsByNameWithCaseInsensitive);

        List<String> groupsByNameWithCaseSensitive = groupAdministration.listGroups(0, 4, new Group.SortByGroupName(true, false))
                .groups().stream().map(Group::name).toList();
        expected = Arrays.asList(group6.name(), group8.name(), group5.name(), group7.name());
        assertEquals(expected, groupsByNameWithCaseSensitive);

        List<String> groupsByReverseNameWithCaseInsensitive = groupAdministration.listGroups(0, 4, new Group.SortByGroupName(false))
                .groups().stream().map(Group::name).toList();
        expected = Arrays.asList(group8.name(), group7.name(), group6.name(), group5.name());
        assertEquals(expected, groupsByReverseNameWithCaseInsensitive);

        List<String> groupsByReverseNameWithCasesensitive = groupAdministration.listGroups(0, 4, new Group.SortByGroupName(false, false))
                .groups().stream().map(Group::name).toList();
        expected = Arrays.asList(group7.name(), group5.name(), group8.name(), group6.name());
        assertEquals(expected, groupsByReverseNameWithCasesensitive);
    }

    @Test
    public void listGroupsWithPattern() {
        Pattern pattern = Pattern.compile(".*1.*");
        PaginatedGroups paginatedGroups = groupAdministration.listGroups(0, 4, new Group.SortByGroupName(true), Optional.of(pattern));
            List<String> groupsByName = paginatedGroups.groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        List<String> expected = Arrays.asList("group1", "group1b");
        assertEquals(expected, groupsByName);
        assertEquals(2, paginatedGroups.totalCount());

        List<String> groupsByReverseName = groupAdministration.listGroups(0, 4, new Group.SortByGroupName(false), Optional.of(pattern)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        expected = Arrays.asList("group1b", "group1");
        assertEquals(expected, groupsByReverseName);
    }

    @Test
    public void listGroupsBySize() {
        // list groups initially to load cache and prove it's refreshing
        groupAdministration.listGroups(0, 4, new Group.SortBySize(true));

        UserWithId user1 = userAdministration.getUserByUsername("another.usertest").get();
        Set<Group> user1Groups = GroupTestUtil.groups("group1", "group2");
        UserModifier user1Modifier = UserModifier.setGroups(user1Groups);
        userAdministration.modifyUser(user1.id(), user1Modifier);

        UserWithId user2 = userAdministration.getUserByUsername("usertest").get();
        Set<Group> user2Groups = GroupTestUtil.groups("group2", "group3");
        UserModifier user2Modifier = UserModifier.setGroups(user2Groups);
        userAdministration.modifyUser(user2.id(), user2Modifier);

        List<String> groupsBySize = groupAdministration.listGroups(0, 5, new Group.SortBySize(true)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        List<String> expected = Arrays.asList("group1b", "group1", "group3", "group2");
        assertEquals(expected, groupsBySize);

        List<String> groupsByDescendingSize = groupAdministration.listGroups(0, 5, new Group.SortBySize(false)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        expected = Arrays.asList("group2", "group3", "group1", "group1b");
        assertEquals(expected, groupsByDescendingSize);
    }

    @Test
    public void listGroupsByAttribute() {
        GroupWithId group1 = groupAdministration.getGroupByName("group1").get();
        Map<String, List<String>> group1Attributes = Map.of(
            "fruit", List.of("banana"),
            "count", List.of("42"),
            "date", List.of(ZonedDateTime.of(2000, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC).toString())
        );
        GroupModifier modifier1 = GroupModifier
            .forGroup(group1)
            .withAttributes(group1Attributes);
        groupAdministration.modifyGroup(group1.id(), modifier1);

        GroupWithId group2 = groupAdministration.getGroupByName("group2").get();
        Map<String, List<String>> group2Attributes = Map.of(
            "fruit", List.of("apple"),
            "count", List.of("43"),
            "date", List.of(ZonedDateTime.of(1999, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC).toString())
        );
        GroupModifier modifier2 = GroupModifier
            .forGroup(group2)
            .withAttributes(group2Attributes);
        groupAdministration.modifyGroup(group2.id(), modifier2);

        GroupWithId group3 = groupAdministration.getGroupByName("group3").get();
        Map<String, List<String>> group3Attributes = Map.of(
            "fruit", List.of("orange"),
            "count", List.of("41"),
            "date", List.of(ZonedDateTime.of(2001, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC).toString())
        );
        GroupModifier modifier3 = GroupModifier
            .forGroup(group3)
            .withAttributes(group3Attributes);
        groupAdministration.modifyGroup(group3.id(), modifier3);

        List<String> groupsByStringAttribute = groupAdministration.listGroups(0, 4, new Group.SortByAttribute("fruit", SortUtils.AttributeType.string,true)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        List<String> expected = Arrays.asList("group1b", "group2", "group1", "group3");
        assertEquals(expected, groupsByStringAttribute);

        List<String> groupsByStringAttributeReverse = groupAdministration.listGroups(0, 4, new Group.SortByAttribute("fruit", SortUtils.AttributeType.string,false)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        expected = Arrays.asList("group3", "group1", "group2", "group1b");
        assertEquals(expected, groupsByStringAttributeReverse);

        List<String> groupsByStringAttributeMissingValuesLast = groupAdministration.listGroups(0, 4, new Group.SortByAttribute("fruit", SortUtils.AttributeType.string,true, false)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        expected = Arrays.asList("group2", "group1", "group3", "group1b");
        assertEquals(expected, groupsByStringAttributeMissingValuesLast);

        List<String> groupsByIntAttribute = groupAdministration.listGroups(0, 4, new Group.SortByAttribute("count", SortUtils.AttributeType.integer,true)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        expected = Arrays.asList("group1b", "group3", "group1", "group2");
        assertEquals(expected, groupsByIntAttribute);

        List<String> groupsByIntAttributeReverse = groupAdministration.listGroups(0, 4, new Group.SortByAttribute("count", SortUtils.AttributeType.integer,false)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        expected = Arrays.asList("group2", "group1", "group3", "group1b");
        assertEquals(expected, groupsByIntAttributeReverse);

        List<String> groupsByIntAttributeMissingValuesLast = groupAdministration.listGroups(0, 4, new Group.SortByAttribute("count", SortUtils.AttributeType.integer,true, false)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        expected = Arrays.asList("group3", "group1", "group2", "group1b");
        assertEquals(expected, groupsByIntAttributeMissingValuesLast);

        List<String> groupsByDateTimeAttribute = groupAdministration.listGroups(0, 4, new Group.SortByAttribute("date", SortUtils.AttributeType.dateTime,true)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        expected = Arrays.asList("group1b", "group2", "group1", "group3");
        assertEquals(expected, groupsByDateTimeAttribute);

        List<String> groupsByDateTimeAttributeReverse = groupAdministration.listGroups(0, 4, new Group.SortByAttribute("date", SortUtils.AttributeType.dateTime,false)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        expected = Arrays.asList("group3", "group1", "group2", "group1b");
        assertEquals(expected, groupsByDateTimeAttributeReverse);

        List<String> groupsByDateTimeAttributeMissingValuesLast = groupAdministration.listGroups(0, 4, new Group.SortByAttribute("date", SortUtils.AttributeType.dateTime,true, false)).groups()
            .stream()
            .map(g -> g.name())
            .collect(Collectors.toList());

        expected = Arrays.asList("group2", "group1", "group3", "group1b");
        assertEquals(expected, groupsByDateTimeAttributeMissingValuesLast);
    }
    
    @Test
    public void groupCount() {
        long groupCount = groupAdministration.groupCount();
        assertEquals(4, groupCount);
    }
    
    @Test
    public void deleteGroup() {
        assertEquals(groupAdministration.getGroupByName(testGroupName), Optional.empty());
        
        Group group = Group.builder()
            .name(testGroupName)
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .build();

        GroupId groupId = groupAdministration.addGroup(group).id();
        
        assertEquals(groupAdministration.getGroupByName(testGroupName).get().dropId(), group);

        groupAdministration.deleteGroup(groupId);
        assertEquals(groupAdministration.getGroupByName(testGroupName), Optional.empty());
    }

    @Test
    public void  createGroupWithAttributes() {
        Map<String, List<String>> attributes = Map.of("description", List.of("I'm happy"));

        Group group = Group.builder()
            .name(testGroupName)
            .attributes(attributes)
            .build();

        GroupWithId returnedGroup = groupAdministration.addGroup(group);

        GroupWithId createdGroup = groupAdministration.getGroupByName(group.name()).get();
        assertEquals(createdGroup.dropId(), group);
        assertEquals(returnedGroup.dropId(), group);
        assertEquals(returnedGroup.attributes(), attributes);
    }

    @Test
    public void modifyAttributes() {
        Map<String, List<String>> oldAttributes = Map.of("description", List.of("I'm happy"));
        Group group = Group.builder()
            .name(testGroupName)
            .attributes(oldAttributes)
            .build();

        groupAdministration.addGroup(group);

        GroupWithId returnedGroup = groupAdministration.getGroupByName(testGroupName).get();

        Map<String, List<String>> newAttributes = Map.of("status", List.of("valid"));
        GroupModifier modifier = GroupModifier
            .forGroup(returnedGroup)
            .withAttributes(Map.of("status", List.of("valid")));

        groupAdministration.modifyGroup(returnedGroup.id(), modifier);

        GroupWithId fetchedGroup = groupAdministration.getGroupById(returnedGroup.id());
        assertEquals(testGroupName, fetchedGroup.name());
        assertEquals(newAttributes, fetchedGroup.attributes());
    }

    @Test
    public void modifyWithoutChangingAttributes() {
        Map<String, List<String>> oldAttributes = Map.of("description", List.of("I'm happy"));
        Group group = Group.builder()
            .name(testGroupName)
            .attributes(oldAttributes)
            .build();

        groupAdministration.addGroup(group);

        GroupWithId returnedGroup = groupAdministration.getGroupByName(testGroupName).get();

        GroupModifier modifier = GroupModifier
            .forGroup(returnedGroup)
            .withName(totoGroupName);

        groupAdministration.modifyGroup(returnedGroup.id(), modifier);

        GroupWithId fetchedGroup = groupAdministration.getGroupById(returnedGroup.id());
        assertEquals(totoGroupName, fetchedGroup.name());
        assertEquals(oldAttributes, fetchedGroup.attributes());
    }

    @Test
    public void getGroupMembersAndSize() {
        UserWithId user1 = userAdministration.getUserByUsername("another.usertest").get();
        Set<Group> user1Groups = GroupTestUtil.groups("group1", "group2");
        UserModifier user1Modifier = UserModifier.setGroups(user1Groups);
        userAdministration.modifyUser(user1.id(), user1Modifier);

        UserWithId user2 = userAdministration.getUserByUsername("usertest").get();
        Set<Group> user2Groups = GroupTestUtil.groups("group2", "group3");
        UserModifier user2Modifier = UserModifier.setGroups(user2Groups);
        userAdministration.modifyUser(user2.id(), user2Modifier);

        GroupWithId group1 = groupAdministration.getGroupByName("group1").get();
        GroupWithId group2 = groupAdministration.getGroupByName("group2").get();
        GroupWithId group3 = groupAdministration.getGroupByName("group3").get();

        assertEquals(1, groupAdministration.groupSize(group1.id()));
        assertEquals(2, groupAdministration.groupSize(group2.id()));
        assertEquals(1, groupAdministration.groupSize(group3.id()));

        assertEquals(List.of(user1.username()), groupAdministration.groupMembers(group1.id(), 0, 3).users().stream().map(u -> u.username()).collect(Collectors.toList()));
        assertEquals(List.of(user1.username(), user2.username()), groupAdministration.groupMembers(group2.id(), 0, 3).users().stream().map(u -> u.username()).collect(Collectors.toList()));
        assertEquals(List.of(user2.username()), groupAdministration.groupMembers(group2.id(), 1, 3).users().stream().map(u -> u.username()).collect(Collectors.toList()));
        assertEquals(List.of(user1.username()), groupAdministration.groupMembers(group2.id(), 0, 1).users().stream().map(u -> u.username()).collect(Collectors.toList()));

        assertEquals(List.of(user1.username()), groupAdministration.groupMembers(group2.id(), 0, 3, new User.SortByUsername(true), Optional.of(new User.Filter(User.FilterField.username, Pattern.compile("another.*")))).users().stream().map(u -> u.username()).collect(Collectors.toList()));

    }

    @Test(expected = GroupAlreadyExistsException.class)
    public void addGroupWithSameNameWithCaseSensitivityShouldReturnConflictException() {
        System.setProperty("config.file", getClass().getClassLoader().getResource("caseInsensitiveGroupNames.conf").getPath());
        ConfigFactory.invalidateCaches();

        Group group1 = Group.builder()
            .name("cloakroom_test_group")
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .build();

        Group group2 = Group.builder()
            .name("Cloakroom_Test_Group")
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .build();

        groupAdministration.getGroupByName(group1.name()).ifPresent(groupAdministration::deleteGroup);
        groupAdministration.getGroupByName(group2.name()).ifPresent(groupAdministration::deleteGroup);

        groupAdministration.addGroup(group1);

        groupAdministration.addGroup(group2);

        System.clearProperty("config.file");
        ConfigFactory.invalidateCaches();
    }

    @Test(expected = GroupIdNotFoundException.class)
    public void getMembersForNonExistentGroup() {
        groupAdministration.groupMembers(new GroupId("blah"), 0, 3);
    }
}
