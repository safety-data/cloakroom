package com.safetydata.cloakroom.admin;

import org.junit.*;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.safetydata.cloakroom.admin.GroupTestUtil.group;
import static org.junit.Assert.assertEquals;

public class RoleAdministrationTest {
    private static final String REALM = "test-realm";
    private static RoleAdministration roleAdministration;
    private static GroupAdministration groupAdministration;
    private static final String testRoleName = "test";
    private static final String totoRoleName = "toto";

    @BeforeClass
    public static void beforeAll() {
        KeycloakAdministration keycloakAdministration = KeycloakAdministration.fromTypeSafeConf(REALM);
        roleAdministration = keycloakAdministration.getRoleAdministration();
        groupAdministration = keycloakAdministration.getGroupAdministration();
    }

    @Before
    public void beforeEach(){
        GroupTestUtil.cleanGroups(groupAdministration);
        roleAdministration.deleteRole(testRoleName);
        roleAdministration.deleteRole(totoRoleName);
        List<Group> groups = new ArrayList<>();
        groups.add(group("group1b", "group1_role"));
        groups.add(group("group1", "group1_role"));
        GroupTestUtil.createGroups(groups, groupAdministration);
    }
    
    @Test
    public void listAvailableRoles() {
        assertEquals(roleAdministration.listAvailableRoles(), Set.of(
            new Role("nobody_as_it"),
            new Role("composite"),
            new Role("son_a"),
            new Role("son_b"),
            new Role("son_c"),
            new Role("another_role"),
            new Role("group1_role")
        ));
    }

    @Test
    public void getRoleByNameShouldReturnEmptyForNonExistingRole() {
        assertEquals(roleAdministration.getRoleByName("blaaaaaaaaaaa"), Optional.empty());
    }

    @Test
    public void getRoleByNameShouldWorkForExistingRole() {
        assertEquals(roleAdministration.getRoleByName("composite"), Optional.of(new Role("composite")));
    }

    @Test
    public void addModifyDeleteRole() {
        Role role = new Role(testRoleName);
        assertEquals(roleAdministration.getRoleByName(testRoleName), Optional.empty());
        
        roleAdministration.addRole(role);
        
        assertEquals(roleAdministration.getRoleByName(testRoleName), Optional.of(role));
        
        Role newRole = new Role(totoRoleName);
        roleAdministration.modifyRole(testRoleName, newRole);

        assertEquals(roleAdministration.getRoleByName(testRoleName), Optional.empty());
        assertEquals(roleAdministration.getRoleByName(totoRoleName), Optional.of(newRole));
        
        roleAdministration.deleteRole(totoRoleName);
        assertEquals(roleAdministration.getRoleByName(totoRoleName), Optional.empty());
    }
    
    @Test
    public void listUsersWithRole() {
        PaginatedUsers users = roleAdministration.listUsersWithRole(new Role("composite"), 0, 100, new User.SortByUsername(true));
        Set<String> usernames = users.users().stream().map(u -> u.username()).collect(Collectors.toSet());
        assertEquals(new HashSet<String>(Arrays.asList("usertest", "another.usertest")), usernames);

        PaginatedUsers usersWithFilter = roleAdministration.listUsersWithRole(new Role("composite"), 0, 100, new User.SortByUsername(true), Optional.of(new User.Filter(User.FilterField.username, Pattern.compile("another.*"))));
        Set<String> usernamesWithFilter = usersWithFilter.users().stream().map(u -> u.username()).collect(Collectors.toSet());
        assertEquals(new HashSet<String>(Arrays.asList("another.usertest")), usernamesWithFilter);
    }

    @Test
    public void listGroupsWithRole() {
        PaginatedGroups groups = roleAdministration.listGroupsWithRole(new Role("group1_role"), 0, 100, new Group.SortByGroupName(true));
        Set<String> groupNames = groups.groups().stream().map(g -> g.name()).collect(Collectors.toSet());
        assertEquals(new HashSet<String>(Arrays.asList("group1", "group1b")), groupNames);

        Pattern pattern = Pattern.compile(".*b.*");
        PaginatedGroups filteredGroups = roleAdministration.listGroupsWithRole(new Role("group1_role"), 0, 100, new Group.SortByGroupName(true), Optional.of(pattern));
        Set<String> filteredGroupNames = filteredGroups.groups().stream().map(g -> g.name()).collect(Collectors.toSet());
        assertEquals(new HashSet<String>(Arrays.asList("group1b")), filteredGroupNames);
    }
}
