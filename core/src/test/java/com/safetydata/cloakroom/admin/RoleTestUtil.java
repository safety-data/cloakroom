package com.safetydata.cloakroom.admin;

import com.safetydata.cloakroom.admin.Role;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RoleTestUtil {
    public static Set<Role> roles(String... name) {
        return Stream.of(name).map(Role::new).collect(Collectors.toSet());
    }

    public static Role role(String name) {
        return new Role(name);
    }
}
