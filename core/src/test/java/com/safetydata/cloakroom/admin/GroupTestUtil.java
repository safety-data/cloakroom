package com.safetydata.cloakroom.admin;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GroupTestUtil {
    public static Set<Group> groups(String... name) {
        return Stream.of(name).map(n -> Group.builder().name(n).build()).collect(Collectors.toSet());
    }

    public static Group group(String name) {
        return Group.builder().name(name).build();
    }

    public static Group group(String name, String... roles) {
        return Group.builder().name(name).roles(RoleTestUtil.roles(roles)).build();
    }

    public static void cleanGroups(GroupAdministration groupAdministration) {
        groupAdministration.listGroups(0, Integer.MAX_VALUE).groups().stream().map(Group::name).forEach(group -> groupAdministration.getGroupByName(group).ifPresent(groupAdministration::deleteGroup));
    }

    public static void createGroups(List<Group> groupsName, GroupAdministration groupAdministration) {
        groupsName.forEach(groupAdministration::addGroup);
    }
}
