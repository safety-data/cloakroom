package com.safetydata.cloakroom.admin;

import com.safetydata.cloakroom.admin.exception.*;
import com.safetydata.cloakroom.mail.FakeSMTPChecker;
import com.safetydata.cloakroom.mail.Mail;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.junit.*;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.UserRepresentation;

import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.safetydata.cloakroom.admin.GroupTestUtil.group;
import static org.junit.Assert.*;

public class UserAdministrationTest {
    private static GroupAdministration groupAdministration;
    private static UserAdministration userAdministrationWithoutRoleLimitation;
    private static RoleAdministration roleAdministration;
    private static String testUsername;
    private static String totoUsername;
    private static String titiUsername;
    private static String invisibleUser;
    private static FakeSMTPChecker smtpChecker;
    private static final String REALM = "test-realm";

    private static final Config config = ConfigFactory.load();
    private static final String anotherUsername = "another.usertest";
    private static final String cloakroomAdmin = config.getString("cloakroom.test-realm.admin.username");
    private static final String username = "usertest";
    private static final String usernameWithoutAnyRoles = "userwithoutanyrole";

    @BeforeClass
    public static void beforeAll() {
        KeycloakAdministration keycloakAdministration = KeycloakAdministration.fromTypeSafeConf(REALM);
        smtpChecker = FakeSMTPChecker.fromTypesafeConf();
        groupAdministration = keycloakAdministration.getGroupAdministration();
        userAdministrationWithoutRoleLimitation = keycloakAdministration.getUserAdministration();
        testUsername = "cloakroom_test_user";
        totoUsername = "toto";
        titiUsername = "titi";
        invisibleUser = "notvisible1";
        roleAdministration = keycloakAdministration.getRoleAdministration();
    }

    @Before
    public void beforeEach(){
        GroupTestUtil.cleanGroups(groupAdministration);
        smtpChecker.clearAllMail();
        GroupTestUtil.cleanGroups(groupAdministration);
        userAdministrationWithoutRoleLimitation.getUserByUsername(testUsername).ifPresent(userAdministrationWithoutRoleLimitation::deleteUser);
        userAdministrationWithoutRoleLimitation.getUserByUsername(totoUsername).ifPresent(userAdministrationWithoutRoleLimitation::deleteUser);
        userAdministrationWithoutRoleLimitation.getUserByUsername(titiUsername).ifPresent(userAdministrationWithoutRoleLimitation::deleteUser);
        userAdministrationWithoutRoleLimitation.getUserByUsername(invisibleUser).ifPresent(userAdministrationWithoutRoleLimitation::deleteUser);
        List<Group> groups = new ArrayList<>();
        groups.add(group("group1b", "group1_role"));
        groups.add(group("group1", "group1_role"));
        groups.add(group("group2", "son_b"));
        groups.add(group("group3"));
        GroupTestUtil.createGroups(groups, groupAdministration);
    }

    @Test
    public void addAndGetUser() {
        long now = System.currentTimeMillis();

        User user = User.builder()
            .username(testUsername)
            .email(testUsername + "@panda.com")
            .firstName("Jup")
            .lastName("Yter")
            .disable()
            .attributes(Map.of("mood", List.of("happy", "lucky")))
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .build();

        UserWithId returnedUser = userAdministrationWithoutRoleLimitation.addUser(user);

        Optional<UserWithId> createdUser = userAdministrationWithoutRoleLimitation.getUserByUsername(user.username());
        assertEquals(createdUser, Optional.of(returnedUser));
        assertEquals(returnedUser.dropId(), user);
        assertTrue(createdUser.get().createDate().isPresent());
        assertTrue(createdUser.get().createDate().get().toInstant().toEpochMilli() > now);
    }

    @Test(expected = UserIdNotFoundException.class)
    public void getNonexistentUser() {
        userAdministrationWithoutRoleLimitation.getUserById(new UserId("blah"));
    }

    @Test
    public void addAndGetUserIfCurrentNumberUserWithRoleIsUnderMax() {
        Integer maxUser = 2;
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        Map<String, Integer> limitedRoles = Map.of("son_a", maxUser);
        UserAdministration userAdministrationWithRoleLimitation = KeycloakAdministration.fromConfig(CloakroomConfigWithAdmin.fromTypeSafeConf(REALM), limitedRoles).getUserAdministration();
        userAdministrationWithRoleLimitation.addUser(user);

        Optional<UserWithId> createdUser = userAdministrationWithRoleLimitation.getUserByUsername(user.username());
        assertTrue(createdUser.get().roles().contains(RoleTestUtil.role("son_a")));
    }

    @Test
    public void getUserNameReturnsOneUserEvenIfKeycloakSearchGiveTwoUsers() {
        //GIVEN
        String username = "usertest";
        String anotherUsername = "another.usertest";
        CloakroomConfigWithAdmin cloakroomConfigWithAdmin = CloakroomConfigWithAdmin.fromTypeSafeConf(REALM);
        UsersResource usersResource = cloakroomConfigWithAdmin.adminKeycloak.realm(cloakroomConfigWithAdmin.keycloakDeployment().getRealm()).users();
        List<UserRepresentation> users = usersResource.search(username);
        assertEquals(2, users.size());
        assertEquals(List.of(
                userAdministrationWithoutRoleLimitation.getUserByUsername(anotherUsername).get().username(),
                userAdministrationWithoutRoleLimitation.getUserByUsername(username).get().username()
        ), users.stream().map(UserRepresentation::getUsername).collect(Collectors.toList()));
        //WHEN
        Optional<UserWithId> result = userAdministrationWithoutRoleLimitation.getUserByUsername(username);
        //THEN
        assertTrue(result.isPresent());
        assertEquals(username, result.get().username());
    }

    @Test(expected = LimitOfUserPerRolesReachedException.class)
    public void addUserIfCurrentUserWithRoleExceedsMaxThrowsException() {
        Integer maxUser = 2;
        User user1 = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        User user2 = User.builder()
                .username(totoUsername)
                .email(totoUsername + "@panda.com")
                .firstName("Butter")
                .lastName("Fly")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        User user3 = User.builder()
                .username(titiUsername)
                .email(titiUsername + "@panda.com")
                .firstName("Chet")
                .lastName("Baker")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        Map<String, Integer> limitedRoles = Map.of("son_a", maxUser);
        UserAdministration userAdministrationWithRoleLimitation = KeycloakAdministration.fromConfig(CloakroomConfigWithAdmin.fromTypeSafeConf(REALM), limitedRoles).getUserAdministration();
        userAdministrationWithRoleLimitation.addUser(user1);
        userAdministrationWithRoleLimitation.addUser(user2);
        userAdministrationWithRoleLimitation.addUser(user3);
    }

    @Test
    public void modifyUserToAddRolesUnderMaxIsOk() {
        User user1 = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        User user2 = User.builder()
                .username(totoUsername)
                .email(totoUsername + "@panda.com")
                .firstName("Butter")
                .lastName("Fly")
                .disable()
                .roles(RoleTestUtil.roles("son_b"))
                .build();

        Integer maxUser = 3;
        Map<String, Integer> limitedRoles = Map.of("son_a", maxUser);

        UserAdministration userAdministrationWithRoleLimitation = KeycloakAdministration.fromConfig(CloakroomConfigWithAdmin.fromTypeSafeConf(REALM), limitedRoles).getUserAdministration();
        userAdministrationWithRoleLimitation.addUser(user1);
        UserWithId createdUser = userAdministrationWithRoleLimitation.addUser(user2);

        Assert.assertEquals(1, roleAdministration.getUsersWithRole("son_a").size());
        Assert.assertEquals(2, roleAdministration.getUsersWithRole("son_b").size());

        userAdministrationWithRoleLimitation.modifyUser(createdUser.id(), UserModifier.addRoles(RoleTestUtil.role("son_a")));

        Assert.assertEquals(2, roleAdministration.getUsersWithRole("son_a").size());
        Assert.assertEquals(2, roleAdministration.getUsersWithRole("son_b").size());
    }

    @Test(expected = RoleNotFoundException.class)
    public void addUserWithRoleThatDoesNotExistShouldFailWithProperException() {
        User user1 = User.builder()
            .username(testUsername)
            .roles(RoleTestUtil.roles("I don't exist"))
            .disable()
            .build();

        userAdministrationWithoutRoleLimitation.addUser(user1);
    }
    
    @Test(expected = RoleNotFoundException.class)
    public void modifyUserToAddRolesThatDoesNotExistShouldFailWithProperException() {
        User user1 = User.builder()
                .username(testUsername)
                .disable()
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user1);
        userAdministrationWithoutRoleLimitation.modifyUser(createdUser.id(), UserModifier.addRoles(RoleTestUtil.role("yolo")));
    }

    @Test
    public void modifyUserToSetRolesUnderMaxIsOk() {
        User user1 = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        User user2 = User.builder()
                .username(totoUsername)
                .email(totoUsername + "@panda.com")
                .firstName("Butter")
                .lastName("Fly")
                .disable()
                .roles(RoleTestUtil.roles("son_b"))
                .build();

        Role roleA = roleAdministration.getRoleByName("son_a").get();

        Integer maxUser = 3;
        Map<String, Integer> limitedRoles = Map.of(roleA.name(), maxUser);

        UserAdministration userAdministrationWithRoleLimitation = KeycloakAdministration.fromConfig(CloakroomConfigWithAdmin.fromTypeSafeConf(REALM), limitedRoles).getUserAdministration();
        userAdministrationWithRoleLimitation.addUser(user1);
        UserWithId createdUser = userAdministrationWithRoleLimitation.addUser(user2);
        Assert.assertEquals(1, roleAdministration.getUsersWithRole("son_a").size());
        Assert.assertEquals(2, roleAdministration.getUsersWithRole("son_b").size());
        userAdministrationWithRoleLimitation.modifyUser(createdUser.id(), UserModifier.setRoles(roleA));
        Assert.assertEquals(2, roleAdministration.getUsersWithRole("son_a").size());
        Assert.assertEquals(1, roleAdministration.getUsersWithRole("son_b").size());
    }

    @Test(expected = LimitOfUserPerRolesReachedException.class)
    public void modifyUserToAddRolesIfItMakesExceedsMaxUserShouldFail() {
        User user1 = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        User user2 = User.builder()
                .username(totoUsername)
                .email(totoUsername + "@panda.com")
                .firstName("Butter")
                .lastName("Fly")
                .disable()
                .roles(RoleTestUtil.roles("son_b"))
                .build();

        Integer maxUser = 1;
        Map<String, Integer> limitedRoles = Map.of("son_a", maxUser);

        UserAdministration userAdministrationWithRoleLimitation = KeycloakAdministration.fromConfig(CloakroomConfigWithAdmin.fromTypeSafeConf(REALM), limitedRoles).getUserAdministration();
        userAdministrationWithRoleLimitation.addUser(user1);
        UserWithId createdUser = userAdministrationWithRoleLimitation.addUser(user2);
        userAdministrationWithRoleLimitation.modifyUser(createdUser.id(), UserModifier.addRoles(RoleTestUtil.role("son_a")));
    }

    @Test(expected = LimitOfUserPerRolesReachedException.class)
    public void modifyUserToSetRolesIfItMakesExceedMaxUserShouldFail() {
        User user1 = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        User user2 = User.builder()
                .username(totoUsername)
                .email(totoUsername + "@panda.com")
                .firstName("Butter")
                .lastName("Fly")
                .disable()
                .roles(RoleTestUtil.roles("son_b", "composite"))
                .build();

        Role roleA = roleAdministration.getRoleByName("son_a").get();
        Role roleComposite = roleAdministration.getRoleByName("composite").get();

        Integer maxUser = 1;
        Map<String, Integer> limitedRoles = Map.of(roleA.name(), maxUser, roleComposite.name(), 10);

        UserAdministration userAdministrationWithRoleLimitation = KeycloakAdministration.fromConfig(CloakroomConfigWithAdmin.fromTypeSafeConf(REALM), limitedRoles).getUserAdministration();
        userAdministrationWithRoleLimitation.addUser(user1);
        UserWithId createdUser = userAdministrationWithRoleLimitation.addUser(user2);
        userAdministrationWithRoleLimitation.modifyUser(createdUser.id(), UserModifier.setRoles(roleA, roleComposite));
    }

    @Test
    public void modifyUserToSetRolesInParrallelShouldNeverResultInLimitationRolesViolation() throws ExecutionException, InterruptedException {
        User user1 = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .disable()
                .roles(RoleTestUtil.roles("son_b"))
                .build();

        User user2 = User.builder()
                .username(totoUsername)
                .email(totoUsername + "@panda.com")
                .firstName("Butter")
                .lastName("Fly")
                .disable()
                .roles(RoleTestUtil.roles("son_b", "composite"))
                .build();

        Role roleA = RoleTestUtil.role("son_a");

        Integer maxUser = 1;
        Map<String, Integer> rolesLimitation = Map.of(roleA.name(), maxUser);

        UserAdministration userAdministrationWithRoleLimitation = KeycloakAdministration.fromConfig(CloakroomConfigWithAdmin.fromTypeSafeConf(REALM), rolesLimitation).getUserAdministration();
        UserWithId createdUserOne = userAdministrationWithRoleLimitation.addUser(user1);
        UserWithId createdUserTwo = userAdministrationWithRoleLimitation.addUser(user2);

        CompletableFuture<UserWithId> resultOne = CompletableFuture.supplyAsync(() -> userAdministrationWithRoleLimitation.modifyUser(createdUserOne.id(), UserModifier.setRoles(roleA)));
        CompletableFuture<UserWithId> resultTwo = CompletableFuture.supplyAsync(() -> userAdministrationWithRoleLimitation.modifyUser(createdUserTwo.id(), UserModifier.setRoles(roleA)));

        boolean oneSuccess = resultOne.handle((ok, exc) -> exc == null).get();
        boolean twoSucess = resultTwo.handle((ok, exc) -> exc == null).get();

        assertTrue(!oneSuccess || !twoSucess);
        Set<String> expectedListUsersWithRoles;
        if (!oneSuccess && !twoSucess) {
            expectedListUsersWithRoles = Set.of();
        } else if(oneSuccess) {
            expectedListUsersWithRoles = Set.of(user1.username());
        } else {
            expectedListUsersWithRoles = Set.of(user2.username());
        }
        assertEquals(
                roleAdministration.listUsersWithRole(roleA, 0, 100, new User.SortByUsername(true))
                    .users()
                    .stream()
                    .map(User::username)
                    .collect(Collectors.toSet()),
                expectedListUsersWithRoles);
    }

    @Test
    public void createUserInParrallelShouldNeverResultInLimitationRolesViolation() throws ExecutionException, InterruptedException {
        User user1 = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        User user2 = User.builder()
                .username(totoUsername)
                .email(totoUsername + "@panda.com")
                .firstName("Butter")
                .lastName("Fly")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        Role role = roleAdministration.getRoleByName("son_a").get();
        Integer maxUser = 1;
        Map<String, Integer> limitedRoles = Map.of(role.name(), maxUser);
        UserAdministration userAdministrationWithRoleLimitation = KeycloakAdministration.fromConfig(CloakroomConfigWithAdmin.fromTypeSafeConf(REALM), limitedRoles).getUserAdministration();
        CompletableFuture<UserWithId> resultOne = CompletableFuture.supplyAsync(() -> userAdministrationWithRoleLimitation.addUser(user1));
        CompletableFuture<UserWithId> resultTwo = CompletableFuture.supplyAsync(() -> userAdministrationWithRoleLimitation.addUser(user2));

        boolean oneSuccess = resultOne.handle((ok, exc) -> exc == null).get();
        boolean twoSucess = resultTwo.handle((ok, exc) -> exc == null).get();

        assertTrue(!oneSuccess || !twoSucess);
        Set<String> expectedListUsersWithRoles;
        if (!oneSuccess && !twoSucess) {
            expectedListUsersWithRoles = Set.of();
        } else if(oneSuccess) {
            expectedListUsersWithRoles = Set.of(user1.username());
        } else {
            expectedListUsersWithRoles = Set.of(user2.username());
        }
        assertEquals(
                roleAdministration.listUsersWithRole(role, 0, 100, new User.SortByUsername(true))
                    .users()
                    .stream()
                    .map(User::username)
                    .collect(Collectors.toSet()),
                expectedListUsersWithRoles);
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void addUserTwiceWithSameUsernameShouldFail() {
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        userAdministrationWithoutRoleLimitation.addUser(user);
        userAdministrationWithoutRoleLimitation.addUser(user);
    }

    @Test(expected = EmailAlreadyUsedException.class)
    public void addUserWithAnAlreadyExistingEmailShouldFailWithCorrectError() {
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        User otherUser = User.builder()
                .username(titiUsername)
                .email(testUsername + "@panda.com")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        userAdministrationWithoutRoleLimitation.addUser(user);
        userAdministrationWithoutRoleLimitation.addUser(otherUser);
    }

    @Test(expected = EmailAlreadyUsedException.class)
    public void modifyUserShouldFailIfTryingToUseAlreadyExistingEmail() {
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        User otherUser = User.builder()
                .username(titiUsername)
                .email(titiUsername + "@panda.com")
                .disable()
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .build();

        userAdministrationWithoutRoleLimitation.addUser(user);
        UserId titiId = userAdministrationWithoutRoleLimitation.addUser(otherUser).id();
        userAdministrationWithoutRoleLimitation.modifyUser(titiId, UserModifier.email(user.email().get()));
    }

    @Test
    public void activateAndDisableUser() {
        User.Builder builder = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a", "son_b"))
                .disable();

        User user = builder
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        userAdministrationWithoutRoleLimitation.modifyUser(createdUser, UserModifier.activated());

        assertEquals(Optional.of(builder.enable().build()),
                userAdministrationWithoutRoleLimitation.getUserByUsername(user.username()).map(UserWithId::dropId));

        userAdministrationWithoutRoleLimitation.modifyUser(createdUser, UserModifier.disable());
        assertEquals(Optional.of(builder.disable().build()),
                userAdministrationWithoutRoleLimitation.getUserByUsername(user.username()).map(UserWithId::dropId));
    }


    @Test
    public void addRoles() {
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a"))
                .disable()
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        createdUser = userAdministrationWithoutRoleLimitation.modifyUser(createdUser, UserModifier.addRoles(RoleTestUtil.roles("son_a", "son_b")));
        assertEquals(createdUser.roles(), RoleTestUtil.roles("son_a", "son_b"));
    }

    @Test
    public void createEnableUserWithRequiredActionShouldSendEmail() {
        String email = testUsername + "@panda.com";
        User user = User.builder()
                .username(testUsername)
                .email(email)
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a"))
                .enable()
                .requiredActions(UserAction.VERIFY_EMAIL, UserAction.UPDATE_PASSWORD)
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        List<Mail> mails = smtpChecker.getMailsTo(email);
        assertEquals(mails.size(), 1);
        Mail mail = mails.get(0);
        checkMailForAction(mail, user.requiredActions());
    }

    @Test
    public void addUserShouldNotSentEmailIfToldNotTo() {
        String email = testUsername + "@panda.com";
        User user = User.builder()
                .username(testUsername)
                .email(email)
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a"))
                .enable()
                .requiredActions(UserAction.VERIFY_EMAIL, UserAction.UPDATE_PASSWORD)
                .build();

        userAdministrationWithoutRoleLimitation.addUser(user, false);
        List<Mail> mails = smtpChecker.getMailsTo(email);
        assertEquals(mails.size(), 0);
    }

    @Test
    public void createUserShouldAllowToSetConfigureTOTP() {
        User user = User.builder()
                .username(testUsername)
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a"))
                .enable()
                .requiredActions(UserAction.CONFIGURE_TOTP)
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        assertEquals(createdUser.requiredActions(), Set.of(UserAction.CONFIGURE_TOTP));
    }

    @Test
    public void createDisableUserWithRequiredActionShouldNotSendEmail() {
        String email = testUsername + "@panda.com";
        User user = User.builder()
                .username(testUsername)
                .email(email)
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a"))
                .disable()
                .requiredActions(UserAction.VERIFY_EMAIL, UserAction.UPDATE_PASSWORD)
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        List<Mail> mails = smtpChecker.getMailsTo(email);
        assertEquals(mails.size(), 0);
    }

    @Test
    public void activateUserWithRequiredActionShouldSendEmail() {
        String email = testUsername + "@panda.com";
        User user = User.builder()
                .username(testUsername)
                .email(email)
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a"))
                .disable()
                .requiredActions(UserAction.VERIFY_EMAIL, UserAction.UPDATE_PASSWORD)
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        userAdministrationWithoutRoleLimitation.modifyUser(createdUser, UserModifier.activated());
        List<Mail> mails = smtpChecker.getMailsTo(email);
        assertEquals(mails.size(), 1);
        Mail mail = mails.get(0);
        checkMailForAction(mail, user.requiredActions());
    }

    @Test
    public void modifyUserShouldNotSentEmailIfToldNotTo() {
        String email = testUsername + "@panda.com";
        User user = User.builder()
                .username(testUsername)
                .email(email)
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a"))
                .disable()
                .requiredActions(UserAction.VERIFY_EMAIL, UserAction.UPDATE_PASSWORD)
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        userAdministrationWithoutRoleLimitation.modifyUser(createdUser.id(), UserModifier.activated(), false);
        List<Mail> mails = smtpChecker.getMailsTo(email);
        assertEquals(mails.size(), 0);
    }

    private void checkMailForAction(Mail mail, Set<UserAction> actions) {
        String text = mail.getText().toLowerCase();
        for(UserAction action: actions) {
            String actionName = action.name().replace("_", " ").toLowerCase();
            assertTrue(String.format("\"%s\" does not contain %s", text, actionName), text.contains(actionName));
        }
    }

    @Test
    public void removeUser() {
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a", "son_b", "nobody_as_it"))
                .disable()
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        createdUser = userAdministrationWithoutRoleLimitation.modifyUser(createdUser, UserModifier.removeRoles(RoleTestUtil.roles("son_a", "nobody_as_it")));
        assertEquals(createdUser.roles(), RoleTestUtil.roles("son_b"));
    }

    @Test
    public void removeAndAddRolesAtTheSameTimeUser() {
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a", "nobody_as_it"))
                .disable()
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        createdUser = userAdministrationWithoutRoleLimitation.modifyUser(createdUser,
                UserModifier.
                        removeRoles(RoleTestUtil.roles("son_a")).
                        withAddedRoles(RoleTestUtil.roles("son_b")));
        assertEquals(createdUser.roles(), RoleTestUtil.roles("son_b", "nobody_as_it"));
    }

    @Test
    public void setRoles() {
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a", "son_b", "nobody_as_it"))
                .disable()
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        createdUser = userAdministrationWithoutRoleLimitation.modifyUser(createdUser, UserModifier.setRoles(RoleTestUtil.roles("son_a")));
        assertEquals(createdUser.roles(), RoleTestUtil.roles("son_a"));
    }
    
    @Test
    public void userRoleShouldNotExpandComposite() {
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(Set.of(new Role("composite")))
                .disable()
                .build();

        userAdministrationWithoutRoleLimitation.addUser(user);

        Optional<UserWithId> createdUser = userAdministrationWithoutRoleLimitation.getUserByUsername(user.username());
        assertEquals(createdUser.get().roles(), RoleTestUtil.roles("composite"));
        assertEquals(userAdministrationWithoutRoleLimitation.userRoles(createdUser.get()), RoleTestUtil.roles("composite"));
    }

    @Test
    public void effectiveUserRolesShouldExpandCompositeRoles() {
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(Set.of(new Role("composite")))
                .disable()
                .build();

        userAdministrationWithoutRoleLimitation.addUser(user);

        Optional<UserWithId> createdUser = userAdministrationWithoutRoleLimitation.getUserByUsername(user.username());
        assertEquals(userAdministrationWithoutRoleLimitation.effectiveUserRoles(createdUser.get()), RoleTestUtil.roles("composite", "son_a", "son_b"));
    }

    @Test
    public void removeAndAddGroupsAtTheSameTimeUser() {
        Set<Group> initialGroups = GroupTestUtil.groups("group1");
        Set<Group> groupsToAdd = GroupTestUtil.groups("group2", "group3");
        Set<Group> groupsToRemove = GroupTestUtil.groups("group1");

        User user = User.builder()
            .username(testUsername)
            .email(testUsername + "@panda.com")
            .firstName("Jup")
            .lastName("Yter")
            .groups(initialGroups)
            .disable()
            .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        createdUser = userAdministrationWithoutRoleLimitation.modifyUser(createdUser,
            UserModifier.
                removeGroups(groupsToRemove).
                withAddedGroups(groupsToAdd));
        assertEquals(createdUser.groups(), groupsToAdd);
    }

    @Test
    public void setGroups() {
        Set<Group> initialGroups = GroupTestUtil.groups("group1");
        Set<Group> groupsToSet = GroupTestUtil.groups("group2", "group3");
        User user = User.builder()
            .username(testUsername)
            .email(testUsername + "@panda.com")
            .firstName("Jup")
            .lastName("Yter")
            .groups(initialGroups)
            .disable()
            .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        createdUser = userAdministrationWithoutRoleLimitation.modifyUser(createdUser, UserModifier.setGroups(groupsToSet));
        assertEquals(createdUser.groups(), groupsToSet);
    }
    
    @Test
    public void listAllUsers() {
        List<UserWithId> users = userAdministrationWithoutRoleLimitation.listUsers(0, 100).users();
        List<UserWithId> expected = List.of(
                userAdministrationWithoutRoleLimitation.getUserByUsername(anotherUsername).get(),
                userAdministrationWithoutRoleLimitation.getUserByUsername(cloakroomAdmin).get(),
                userAdministrationWithoutRoleLimitation.getUserByUsername(username).get(),
                userAdministrationWithoutRoleLimitation.getUserByUsername(usernameWithoutAnyRoles).get()
        );
        assertEquals(users, expected);
    }

    @Test
    public void listOnlyVisibleUsers() {
        User user = User.builder()
                .username(invisibleUser)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(Set.of(new Role("composite")))
                .disable()
                .build();

        userAdministrationWithoutRoleLimitation.addUser(user);

        List<UserWithId> users = userAdministrationWithoutRoleLimitation.listUsers(0, 100).users();
        List<UserWithId> expected = List.of(
            userAdministrationWithoutRoleLimitation.getUserByUsername(anotherUsername).get(),
            userAdministrationWithoutRoleLimitation.getUserByUsername(cloakroomAdmin).get(),
            userAdministrationWithoutRoleLimitation.getUserByUsername(username).get(),
            userAdministrationWithoutRoleLimitation.getUserByUsername(usernameWithoutAnyRoles).get()
        );
        assertEquals(users, expected);
        assertTrue(userAdministrationWithoutRoleLimitation.getUserByUsername(invisibleUser).isPresent());
    }

    @Test
    public void listUsersWithPagination() {
        User user = User.builder()
                .username(invisibleUser)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(Set.of(new Role("composite")))
                .disable()
                .build();

        userAdministrationWithoutRoleLimitation.addUser(user);

        PaginatedUsers users = userAdministrationWithoutRoleLimitation.listUsers(1, 3);

        assertEquals(3, users.users().size());
        assertEquals(4, users.totalCount());
        assertFalse(users.users().stream().anyMatch(u -> u.username().equals(invisibleUser))); //if the filtering works, the notvisibleUser should not appear. But we should still receive 3 users
    }

    @Test
    public void listUsersReverseOrder() {
        List<UserWithId> users = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByUsername(false)).users();
        List<String> expected = List.of(
            usernameWithoutAnyRoles,
            username,
            cloakroomAdmin,
            anotherUsername
        );
        assertEquals(users.stream().map(User::username).collect(Collectors.toList()), expected);
    }

    @Test
    public void listUsersOrderByEmail() {
        // usertest: test@safety-data.com
        // another.usertest: another.test@safety-data.com
        // userwithoutanyrole: userwithoutanyrole@safety-data.com
        // cloakroom.admin: admin@safety-data.com
        List<UserWithId> users = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByEmail(true)).users();
        List<String> expected = List.of(
            cloakroomAdmin,
            anotherUsername,
            username,
            usernameWithoutAnyRoles
        );
        assertEquals(users.stream().map(User::username).collect(Collectors.toList()), expected);

        List<UserWithId> reverseUsers = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByEmail(false)).users();
        List<String> reverseExpected = new ArrayList(expected);
        Collections.reverse(reverseExpected);
        assertEquals(reverseUsers.stream().map(User::username).collect(Collectors.toList()), reverseExpected);
    }

    @Test
    public void listUsersOrderByCreateDate() {
        List<UserWithId> users = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByCreateDate(true)).users();

        List<ZonedDateTime> userCreateDates = users.stream().map(u -> u.createDate().get()).collect(Collectors.toList());
        List<ZonedDateTime> createDateAscending = userCreateDates.stream().sorted().collect(Collectors.toList());

        assertEquals(userCreateDates, createDateAscending);

        List<UserWithId> reverseUsers = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByCreateDate(false)).users();
        List<String> reverseExpected = new ArrayList(createDateAscending);
        Collections.reverse(reverseExpected);
        assertEquals(reverseUsers.stream().map(u -> u.createDate().get()).collect(Collectors.toList()), reverseExpected);
    }

    @Test
    public void listUsersOrderByAttribute() {
        //        "username": "usertest",
        //        "attributes" : {
        //            "fruit" : [ "banana" ],
        //            "date": [ "2000-01-01T00:00Z" ],
        //            "number": [ "42" ]
        //        }
        //        "username": "another.usertest",
        //        "attributes" : {
        //            "fruit" : [ "apple" ],
        //            "date": [ "2003-01-01T00:00Z" ],
        //            "number": [ "43" ]
        //        }
        //        "username": "userwithoutanyrole",
        //        "clientRoles": {},
        //        "attributes" : {
        //            "fruit" : [ "orange" ],
        //            "date": [ "1999-01-01T00:00Z" ],
        //            "number": [ "44" ]
        //        }
        List<UserWithId> usersByFruit = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByAttribute("fruit", SortUtils.AttributeType.string, true)).users();
        List<String> usersByFruitExpected = List.of(cloakroomAdmin, anotherUsername, username, usernameWithoutAnyRoles);
        assertEquals(usersByFruit.stream().map(User::username).collect(Collectors.toList()), usersByFruitExpected);

        List<UserWithId> usersByFruitReverse = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByAttribute("fruit", SortUtils.AttributeType.string, false)).users();
        List<String> usersByFruitReverseExpected = new ArrayList(usersByFruitExpected);
        Collections.reverse(usersByFruitReverseExpected);
        assertEquals(usersByFruitReverse.stream().map(User::username).collect(Collectors.toList()), usersByFruitReverseExpected);

        List<UserWithId> usersByFruitMissingValuesLast = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByAttribute("fruit", SortUtils.AttributeType.string, true, false)).users();
        List<String> usersByFruitExpectedMissingValuesLast = List.of(anotherUsername, username, usernameWithoutAnyRoles, cloakroomAdmin);
        assertEquals(usersByFruitMissingValuesLast.stream().map(User::username).collect(Collectors.toList()), usersByFruitExpectedMissingValuesLast);

        List<UserWithId> usersByDate = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByAttribute("date", SortUtils.AttributeType.dateTime, true)).users();
        List<String> usersByDateExpected = List.of(cloakroomAdmin, usernameWithoutAnyRoles, username, anotherUsername);
        assertEquals(usersByDate.stream().map(User::username).collect(Collectors.toList()), usersByDateExpected);

        List<UserWithId> usersByDateReverse = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByAttribute("date", SortUtils.AttributeType.dateTime, false)).users();
        List<String> usersByDateReverseExpected = new ArrayList(usersByDateExpected);
        Collections.reverse(usersByDateReverseExpected);
        assertEquals(usersByDateReverse.stream().map(User::username).collect(Collectors.toList()), usersByDateReverseExpected);

        List<UserWithId> usersByDateMissingValuesLast = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByAttribute("date", SortUtils.AttributeType.dateTime, true, false)).users();
        List<String> usersByDateExpectedMissingValuesLast = List.of(usernameWithoutAnyRoles, username, anotherUsername, cloakroomAdmin);
        assertEquals(usersByDateMissingValuesLast.stream().map(User::username).collect(Collectors.toList()), usersByDateExpectedMissingValuesLast);

        List<UserWithId> usersByNumber = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByAttribute("number", SortUtils.AttributeType.integer, true)).users();
        List<String> usersByNumberExpected = List.of(cloakroomAdmin, username, anotherUsername, usernameWithoutAnyRoles);
        assertEquals(usersByNumber.stream().map(User::username).collect(Collectors.toList()), usersByNumberExpected);

        List<UserWithId> usersByNumberReverse = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByAttribute("number", SortUtils.AttributeType.integer, false)).users();
        List<String> usersByNumberReverseExpected = new ArrayList(usersByNumberExpected);
        Collections.reverse(usersByNumberReverseExpected);
        assertEquals(usersByNumberReverse.stream().map(User::username).collect(Collectors.toList()), usersByNumberReverseExpected);

        List<UserWithId> usersByNumberMissingValuesLast = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByAttribute("number", SortUtils.AttributeType.integer, true, false)).users();
        List<String> usersByNumberExpectedMissingValuesLast = List.of(username, anotherUsername, usernameWithoutAnyRoles, cloakroomAdmin);
        assertEquals(usersByNumberMissingValuesLast.stream().map(User::username).collect(Collectors.toList()), usersByNumberExpectedMissingValuesLast);
    }

    @Test
    public void listUsersWithUsernameFilter() {
        PaginatedUsers users = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByUsername(true),
            Optional.of(new User.Filter(User.FilterField.username, Pattern.compile(".*usertest.*"))));

        List<String> expected = List.of(anotherUsername, username);
        assertEquals(users.users().stream().map(User::username).collect(Collectors.toList()), expected);
        assertEquals(2, users.totalCount());
    }

    @Test
    public void listUsersWithEmailFilter() {
        // usertest: test@safety-data.com
        // another.usertest: another.test@safety-data.com
        // userwithoutanyrole: userwithoutanyrole@safety-data.com
        // cloakroom.admin: admin@safety-data.com
        PaginatedUsers users = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByUsername(true),
            Optional.of(new User.Filter(User.FilterField.email, Pattern.compile(".*test@safety-data.com"))));

        List<String> expected = List.of(anotherUsername, username);
        assertEquals(users.users().stream().map(User::username).collect(Collectors.toList()), expected);
        assertEquals(2, users.totalCount());
    }

    @Test
    public void listUsersWithRequiredRole() {
        PaginatedUsers users = userAdministrationWithoutRoleLimitation.listUsers(0, 100, new User.SortByUsername(true),
            Optional.empty(), Optional.of(new Role("composite")));

        List<String> expected = List.of(anotherUsername, username);
        assertEquals(users.users().stream().map(User::username).collect(Collectors.toList()), expected);
        assertEquals(2, users.totalCount());
    }
    
    @Test
    public void countAllUsers() {
        int count = userAdministrationWithoutRoleLimitation.userCount();
        // 3 users defined in docker-compose/realm.json
        assertEquals(4, count);
    }

    @Test
    public void countIgnoreNotVisibleUsers() {
        User user = User.builder()
                .username(invisibleUser)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(Set.of(new Role("composite")))
                .disable()
                .build();

        userAdministrationWithoutRoleLimitation.addUser(user);

        int count = userAdministrationWithoutRoleLimitation.userCount();

        assertEquals(4, count);
    }

    @Test
    public void setFirstName() {
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a", "son_b", "nobody_as_it"))
                .disable()
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        createdUser = userAdministrationWithoutRoleLimitation.modifyUser(createdUser, UserModifier.firstName("new_first_name"));
        assertEquals(createdUser.firstName(), Optional.of("new_first_name"));

    }

    @Test
    public void setLastName() {
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a", "son_b", "nobody_as_it"))
                .disable()
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        createdUser = userAdministrationWithoutRoleLimitation.modifyUser(createdUser, UserModifier.lastName("new_first_name"));
        assertEquals(createdUser.lastName(), Optional.of("new_first_name"));
    }

    @Test
    public void setEmail() {
        User user = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a", "son_b", "nobody_as_it"))
                .disable()
                .build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        createdUser = userAdministrationWithoutRoleLimitation.modifyUser(createdUser, UserModifier.email("new@email.com"));
        assertEquals(createdUser.email(), Optional.of("new@email.com"));
    }

    @Test
    public void setMultipleStuffAtOnce() {
        User.Builder builder = User.builder()
                .username(testUsername)
                .email(testUsername + "@panda.com")
                .firstName("Jup")
                .lastName("Yter")
                .roles(RoleTestUtil.roles("son_a", "son_b", "nobody_as_it"))
                .disable();

        User user = builder.build();

        UserWithId createdUser = userAdministrationWithoutRoleLimitation.addUser(user);
        createdUser = userAdministrationWithoutRoleLimitation.modifyUser(createdUser, UserModifier
            .email("new@email.com")
            .activate()
            .withFirstName("Celine")
            .withLastName("Dion")
            .withAttributes(Map.of("description", List.of("I'm happy")))
            .withSettedRoles(RoleTestUtil.roles("composite"))
            .withRequiredActions(UserAction.UPDATE_PASSWORD));

        User expectedUser = builder
            .enable()
            .email("new@email.com")
            .firstName("Celine")
            .lastName("Dion")
            .attributes(Map.of("description", List.of("I'm happy")))
            .roles(RoleTestUtil.roles("composite"))
            .requiredActions(UserAction.UPDATE_PASSWORD)
            .build();

        assertEquals(createdUser.dropId(), expectedUser);

        List<Mail> mails = smtpChecker.getMailsTo("new@email.com");
        assertEquals(mails.size(), 1);
        Mail mail = mails.get(0);
        checkMailForAction(mail, createdUser.requiredActions());

        UserWithId retrievedUser = userAdministrationWithoutRoleLimitation.getUserById(createdUser.id());
        assertEquals(retrievedUser.dropId(), expectedUser);
    }

    @Test
    public void getUserByUsernameWhenNoUser() {
        assertEquals(userAdministrationWithoutRoleLimitation.getUserByUsername("HoChiMinh"), Optional.empty());
    }

    @Test
    public void addAndGetUserWithGroups() {
        Set<Group> groups = GroupTestUtil.groups("group1", "group3");
        
        User user = User.builder()
            .username(testUsername)
            .email(testUsername + "@panda.com")
            .firstName("Jup")
            .lastName("Yter")
            .disable()
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .groups(groups)
            .build();

        UserWithId returnedUser = userAdministrationWithoutRoleLimitation.addUser(user);

        Optional<UserWithId> createdUser = userAdministrationWithoutRoleLimitation.getUserByUsername(user.username());
        assertEquals(createdUser, Optional.of(returnedUser));
        assertEquals(groups, createdUser.get().groups());
        assertEquals(returnedUser.dropId(), user);
    }

    @Test(expected = GroupNotFoundException.class)
    public void addUserWithUnknownGroup() {
        Set<Group> groups = GroupTestUtil.groups("unknown");

        User user = User.builder()
            .username(testUsername)
            .email(testUsername + "@panda.com")
            .firstName("Jup")
            .lastName("Yter")
            .disable()
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .groups(groups)
            .build();

        UserWithId returnedUser = userAdministrationWithoutRoleLimitation.addUser(user);
    }

    @Test
    public void modifyUserGroupsWithSet() {
        Set<Group> groups = GroupTestUtil.groups("group1", "group3");

        User user = User.builder()
            .username(testUsername)
            .email(testUsername + "@panda.com")
            .firstName("Jup")
            .lastName("Yter")
            .disable()
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .groups(groups)
            .build();

        UserWithId returnedUser = userAdministrationWithoutRoleLimitation.addUser(user);

        Set<Group> newGroups = GroupTestUtil.groups("group2", "group3");
        UserModifier userModifier = UserModifier.setGroups(newGroups);
        UserWithId modifiedUser = userAdministrationWithoutRoleLimitation.modifyUser(returnedUser, userModifier);

        Optional<UserWithId> fetchedUser = userAdministrationWithoutRoleLimitation.getUserByUsername(user.username());
        assertEquals(fetchedUser, Optional.of(modifiedUser));
        assertEquals(newGroups, fetchedUser.get().groups());
    }

    @Test
    public void modifyUserGroupsWithAdd() {
        Set<Group> groups = GroupTestUtil.groups("group1");

        User user = User.builder()
            .username(testUsername)
            .email(testUsername + "@panda.com")
            .firstName("Jup")
            .lastName("Yter")
            .disable()
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .groups(groups)
            .build();

        UserWithId returnedUser = userAdministrationWithoutRoleLimitation.addUser(user);

        Set<Group> newGroups = GroupTestUtil.groups("group2", "group3");
        UserModifier userModifier = UserModifier.addGroups(newGroups);
        UserWithId modifiedUser = userAdministrationWithoutRoleLimitation.modifyUser(returnedUser, userModifier);

        Set<Group> allGroups = new HashSet<>(groups);
        allGroups.addAll(newGroups);
        
        Optional<UserWithId> fetchedUser = userAdministrationWithoutRoleLimitation.getUserByUsername(user.username());
        assertEquals(fetchedUser, Optional.of(modifiedUser));
        assertEquals(allGroups, fetchedUser.get().groups());
    }

    @Test
    public void modifyUserGroupsWithRemove() {
        Set<Group> groups = GroupTestUtil.groups("group1", "group2");

        User user = User.builder()
            .username(testUsername)
            .email(testUsername + "@panda.com")
            .firstName("Jup")
            .lastName("Yter")
            .disable()
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .groups(groups)
            .build();

        UserWithId returnedUser = userAdministrationWithoutRoleLimitation.addUser(user);

        Set<Group> newGroups = GroupTestUtil.groups("group2", "group3");
        UserModifier userModifier = UserModifier.removeGroups(newGroups);
        UserWithId modifiedUser = userAdministrationWithoutRoleLimitation.modifyUser(returnedUser, userModifier);

        Set<Group> allGroups = new HashSet<>(groups);
        allGroups.removeAll(newGroups);

        Optional<UserWithId> fetchedUser = userAdministrationWithoutRoleLimitation.getUserByUsername(user.username());
        assertEquals(fetchedUser, Optional.of(modifiedUser));
        assertEquals(allGroups, fetchedUser.get().groups());
    }

    @Test(expected = GroupNotFoundException.class)
    public void modifyUserWithUnknownGroup() {
        Set<Group> groups = GroupTestUtil.groups("unknown");

        User user = User.builder()
            .username(testUsername)
            .email(testUsername + "@panda.com")
            .firstName("Jup")
            .lastName("Yter")
            .disable()
            .roles(RoleTestUtil.roles("son_a", "son_b"))
            .build();
        
        UserWithId returnedUser = userAdministrationWithoutRoleLimitation.addUser(user);
        
        UserModifier userModifier = UserModifier.addGroups(groups);
        userAdministrationWithoutRoleLimitation.modifyUser(returnedUser, userModifier);
    }
}