package com.safetydata.cloakroom.publickeys;

import com.safetydata.cloakroom.TokenTestUtil;

import java.math.BigInteger;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;

class KeyTestUtil {


    static KeyData generateKeyData(String id, RSAPublicKey publicKey) {
        KeyData keyData = new KeyData();
        keyData.kid = id;
        keyData.n = b64EncodedBigInteger(publicKey.getModulus());
        keyData.e = b64EncodedBigInteger(publicKey.getPublicExponent());

        return keyData;
    }

    static KeyData generateKeyData(String id) {
        return generateKeyData(id, TokenTestUtil.generatePublicKey());
    }


    private static String b64EncodedBigInteger(BigInteger bigInteger) {
        Base64.Encoder urlEncoder = Base64.getUrlEncoder();
        return urlEncoder.encodeToString(bigInteger.toByteArray());
    }
}
