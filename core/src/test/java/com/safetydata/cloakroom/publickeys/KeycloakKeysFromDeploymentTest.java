package com.safetydata.cloakroom.publickeys;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.safetydata.cloakroom.TokenTestUtil;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class KeycloakKeysFromDeploymentTest {

    @Test
    public void keysShouldFailWithCouldNotFetchKeycloakClientPublicKeyIfHtpFetcherFailAsynchroniously() {
        KeycloakKeys keycloakKeys = new KeycloakKeysFromDeployment(
                TokenTestUtil.getDumpDeployment(),
                (url -> CompletableFuture.supplyAsync(() -> {
                    {
                        throw new RuntimeException("Http Error");
                    }
                }))
        );

        try {
            keycloakKeys.keys().join();
        } catch (CompletionException e) {
            assertTrue(e.getCause() instanceof CouldNotFetchKeycloakClientPublicKey);
        }
    }

    @Test
    public void shouldReturnKeysReturnedByTheServer() {
        Keys keys = new Keys();

        RSAPublicKey pk1 = TokenTestUtil.generatePublicKey();
        KeyData k1 = KeyTestUtil.generateKeyData("1", pk1);

        RSAPublicKey pk2 = TokenTestUtil.generatePublicKey();
        KeyData k2 = KeyTestUtil.generateKeyData("2", pk2);

        keys.keys = List.of(k1, k2);

        ObjectMapper objectMapper = new ObjectMapper();

        KeycloakKeys keycloakKeys = new KeycloakKeysFromDeployment(
                TokenTestUtil.getDumpDeployment(),
                (url -> CompletableFuture.supplyAsync(() -> {
                    try {
                        String jsonString = objectMapper.writeValueAsString(keys);
                        return new ByteArrayInputStream(jsonString.getBytes());
                    } catch (JsonProcessingException e) {
                        // should not happen
                        throw new RuntimeException(e);
                    }
                }))
        );

        Map<String, PublicKey> mapKeys = keycloakKeys.keys().join();
        assertEquals(mapKeys, keys.publicKeysById());
    }
}