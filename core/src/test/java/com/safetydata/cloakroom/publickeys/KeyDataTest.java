package com.safetydata.cloakroom.publickeys;

import com.safetydata.cloakroom.TokenTestUtil;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;

import static org.junit.Assert.assertEquals;

public class KeyDataTest {

    @Test
    public void publicKeysShouldParseValidRSAKey() throws NoSuchAlgorithmException {
        RSAPublicKey publicKey = TokenTestUtil.generatePublicKey();
        KeyData keyData = KeyTestUtil.generateKeyData("id", publicKey);
        assertEquals(publicKey, keyData.publicKeys());
    }

    @Test(expected = CouldNotFetchKeycloakClientPublicKey.class)
    public void publicKeysShouldRaiseCouldNotFetchKeycloakClientPublicKeyIfKeyIsInvalid() throws NoSuchAlgorithmException {
        RSAPublicKey publicKey = TokenTestUtil.generatePublicKey();
        KeyData keyData = KeyTestUtil.generateKeyData("id", publicKey);
        keyData.n = "42";
        assertEquals(publicKey, keyData.publicKeys());
    }
}