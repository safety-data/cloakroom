package com.safetydata.cloakroom.publickeys;

import org.junit.Test;

import java.io.InputStream;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

import static org.junit.Assert.assertTrue;

public class HttpGetImplTest {

    private static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    @Test
    public void get() {
        HttpGet httpGet = new HttpGetImpl();
        InputStream is = httpGet.get("http://perdu.com").join();
        String body = convertStreamToString(is);

        assertTrue(body.contains("Perdu"));
    }

    @Test(expected = CompletionException.class)
    public void getShouldFailOn404() {
        HttpGet httpGet = new HttpGetImpl();
        InputStream is = httpGet.get("http://perdu.com/papaouT").join();
    }
}