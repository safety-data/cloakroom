package com.safetydata.cloakroom.publickeys;

import com.safetydata.cloakroom.TokenTestUtil;
import org.junit.Test;

import java.security.interfaces.RSAPublicKey;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class KeysTest {

    @Test
    public void publicKeysById() {
        Keys keys = new Keys();

        RSAPublicKey pk1 = TokenTestUtil.generatePublicKey();
        KeyData k1 = KeyTestUtil.generateKeyData("1", pk1);

        RSAPublicKey pk2 = TokenTestUtil.generatePublicKey();
        KeyData k2 = KeyTestUtil.generateKeyData("2", pk2);

        keys.keys = List.of(k1, k2);

        assertEquals(keys.publicKeysById(),
                Map.of("1", pk1, "2", pk2));
    }

    @Test(expected = CouldNotFetchKeycloakClientPublicKey.class)
    public void shouldRaiseCouldNotFetchKeycloakClientPublicKeyIfTwiceKeysWithSameId() {
        Keys keys = new Keys();
        KeyData k1 = KeyTestUtil.generateKeyData("1");
        KeyData k2 = KeyTestUtil.generateKeyData("1");
        keys.keys = List.of(
                KeyTestUtil.generateKeyData("1"),
                KeyTestUtil.generateKeyData("1")
        );

        keys.publicKeysById();
    }
}