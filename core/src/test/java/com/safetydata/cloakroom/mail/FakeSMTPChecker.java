package com.safetydata.cloakroom.mail;

import com.typesafe.config.ConfigFactory;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;

import java.util.ArrayList;
import java.util.List;

public class FakeSMTPChecker {
    private final Client client;
    private final WebTarget base;

    public FakeSMTPChecker(String apiUrl) {
        this.client = ClientBuilder.newClient();
        this.base = client.target(apiUrl);
    }

    public static FakeSMTPChecker fromTypesafeConf() {
        return new FakeSMTPChecker(ConfigFactory.load().getString("cloakroom.test.fake-smtp-api-url"));
    }

    public void clearAllMail() {
        base.path("api/emails").request().delete();
    }

    public List<Mail> getMailsTo(String to) {
        return (List<Mail>) base.path("api/emails").queryParam(to, to).request(MediaType.APPLICATION_JSON_TYPE).get(new GenericType<ArrayList<Mail>>() {});
    }
}
