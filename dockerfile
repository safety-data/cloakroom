FROM hseeberger/scala-sbt:eclipse-temurin-17.0.2_1.6.2_2.13.8
COPY . /cloakroom
WORKDIR /cloakroom
ENTRYPOINT ["sbt"]
