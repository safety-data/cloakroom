COMPOSE_PROJECT ?= "CLOAKROOM_TEST"

.PHONY: test start-dep stop-dep
.ONESHELL: test start-dep stop-dep
.SHELLFLAGS = -e -c # Needed to make task fail when not last command of fail when using .ONESHELL

init-dev-env: clean-docker-compose  start-dep-with-ports-binding
	@ cd "${CURDIR}"
	@sleep 1


test: start-dep-with-ports-binding
	@ cd "${CURDIR}"
	sbt "+ test"

start-dep-with-ports-binding:
	@ cd "${CURDIR}"
	docker-compose -p $(COMPOSE_PROJECT) -f docker-compose/deps.yml build
	docker-compose -p $(COMPOSE_PROJECT) -f docker-compose/deps.yml -f docker-compose/bind-ports.yml up -d

start-dep-without-ports-binding:
	@ cd "${CURDIR}"
	docker-compose -p $(COMPOSE_PROJECT) -f docker-compose/deps.yml build
	docker-compose -p $(COMPOSE_PROJECT) -f docker-compose/deps.yml up -d

stop-dep:
	@ cd "${CURDIR}"
	docker-compose -p $(COMPOSE_PROJECT) -f docker-compose/deps.yml -f docker-compose/bind-ports.yml down

erase-dep-state: stop-dep
	@ cd "${CURDIR}"
	docker-compose -p $(COMPOSE_PROJECT) -f docker-compose/deps.yml -f docker-compose/bind-ports.yml rm

test-ci: erase-dep-state start-dep-without-ports-binding
	@ cd "${CURDIR}"
	docker-compose -p $(COMPOSE_PROJECT) -f docker-compose/runner.yml build
	sleep 20
	docker-compose -p $(COMPOSE_PROJECT) -f docker-compose/runner.yml run -T sbt "+ test"

deploy-ci: erase-dep-state start-dep-without-ports-binding
	@ cd "${CURDIR}"
	docker-compose -p $(COMPOSE_PROJECT) -f docker-compose/runner.yml build
	docker-compose -p $(COMPOSE_PROJECT) -f docker-compose/runner.yml run -T sbt "+ publish"

simulate-ci:
	docker run -v /var/run/docker.sock:/var/run/docker.sock \
		-v $(CURDIR):/app:ro \
    -w /app \
    tiangolo/docker-with-compose \
		make test-ci

clean-docker-compose:
	@ cd "${CURDIR}"
	docker-compose -p $(COMPOSE_PROJECT) -f docker-compose/runner.yml -f docker-compose/deps.yml -f docker-compose/bind-ports.yml down --remove-orphans --rmi local
